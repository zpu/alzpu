--
--  ALZPU 
-- 
--  Copyright 2008 Alvaro Lopes <alvieboy@alvie.com>
-- 
--  Version: 1.0
-- 
--  The FreeBSD license
--  
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following conditions
--  are met:
--  
--  1. Redistributions of source code must retain the above copyright
--     notice, this list of conditions and the following disclaimer.
--  2. Redistributions in binary form must reproduce the above
--     copyright notice, this list of conditions and the following
--     disclaimer in the documentation and/or other materials
--     provided with the distribution.
--  
--  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY
--  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
--  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
--  PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
--  ZPU PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
--  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
--  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
--  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
--  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
--  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
--  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
--  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--  
-- */
library ieee;  
use ieee.std_logic_1164.all;  
use ieee.std_logic_unsigned.all;

package tech_virtex2 is

component virtex2_dp_ram is
   generic (
      ABITS: integer := 11;
      DBITS: integer := 32;
      DWORDBITS: integer := 2
   );
  	port (a_clk : in std_logic;
        	a_we  : in std_logic;
        	a_a   : in std_logic_vector(ABITS-1 downto DWORDBITS);
        	a_di  : in std_logic_vector(DBITS-1 downto 0);  -- data input
        	a_do  : out std_logic_vector(DBITS-1 downto 0); --data output
          a_en  : in std_logic_vector((DBITS/8)-1 downto 0);
          b_clk : in std_logic;
        	b_we  : in std_logic;
          b_en  : in std_logic_vector((DBITS/8)-1 downto 0);
        	b_a   : in std_logic_vector(ABITS-1 downto DWORDBITS);
          b_di  : in std_logic_vector(DBITS-1 downto 0);  -- data input
        	b_do  : out std_logic_vector(DBITS-1 downto 0) --data output
    );
end component;

end;


library ieee;  
use ieee.std_logic_1164.all;  
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
library work;
use work.txt_util.all;
Library UNISIM;
use UNISIM.vcomponents.all;


entity virtex2_dp_ram is    
   generic (
      ABITS: integer := 11;
      DBITS: integer := 32;
      DWORDBITS: integer := 2
   );
  	port (a_clk : in std_logic;
        	a_we  : in std_logic;
        	a_a   : in std_logic_vector(ABITS-1 downto DWORDBITS);
        	a_di  : in std_logic_vector(DBITS-1 downto 0);  -- data input
        	a_do  : out std_logic_vector(DBITS-1 downto 0); --data output
          a_en  : in std_logic_vector((DBITS/8)-1 downto 0);
          b_clk : in std_logic;
        	b_we  : in std_logic;
          b_en  : in std_logic_vector((DBITS/8)-1 downto 0);
        	b_a   : in std_logic_vector(ABITS-1 downto DWORDBITS);
        	b_di  : in std_logic_vector(DBITS-1 downto 0);  -- data input
        	b_do  : out std_logic_vector(DBITS-1 downto 0)
    );

end virtex2_dp_ram;

architecture behave of virtex2_dp_ram is

begin

w9: if ABITS=11 generate

ram0: RAMB16_S36_S36
  generic map (
    WRITE_MODE_A => "WRITE_FIRST",
    WRITE_MODE_B => "WRITE_FIRST"
--    SIM_COLLISION_CHECK => "NONE"
  )
  port map (
      DOA => a_do,      -- Port A 32-bit Data Output
      DOB => b_do,      -- Port B 32-bit Data Output
      DOPA => open,    -- Port A 4-bit Parity Output
      DOPB => open,    -- Port B 4-bit Parity Output
      ADDRA => a_a,  -- Port A 9-bit Address Input
      ADDRB => b_a,  -- Port B 9-bit Address Input
      CLKA => a_clk,    -- Port A Clock
      CLKB => b_clk,    -- Port B Clock
      DIA => a_di,      -- Port A 32-bit Data Input
      DIB => b_di,      -- Port B 32-bit Data Input
      DIPA => (others=>'0'),    -- Port A 4-bit parity Input
      DIPB => (others=>'0'),    -- Port-B 4-bit parity Input
      ENA => a_en(0),      -- Port A RAM Enable Input
      ENB => b_en(0),      -- PortB RAM Enable Input
      SSRA => '0',    -- Port A Synchronous Set/Reset Input
      SSRB => '0',    -- Port B Synchronous Set/Reset Input
      WEA => a_we,      -- Port A Write Enable Input
      WEB => b_we       -- Port B Write Enable Input
  );

end generate;

w10: if ABITS=12 generate

rams: for n in 0 to 1 generate

ram0: RAMB16_S18_S36
  generic map (
    WRITE_MODE_A => "WRITE_FIRST",
    WRITE_MODE_B => "WRITE_FIRST"
--    SIM_COLLISION_CHECK => "NONE"
  )
  port map (
      DOA => a_do(16*(n+1) -1 downto 16*n),      -- Port A 32-bit Data Output
      DOB => b_do(16*(n+1) -1 downto 16*n),      -- Port B 32-bit Data Output
      DOPA => open,    -- Port A 4-bit Parity Output
      DOPB => open,    -- Port B 4-bit Parity Output
      ADDRA => a_a(ABITS-1 downto DWORDBITS),  -- Port A 9-bit Address Input
      ADDRB => b_a(ABITS-1 downto DWORDBITS),  -- Port B 9-bit Address Input
      CLKA => a_clk,    -- Port A Clock
      CLKB => b_clk,    -- Port B Clock
      DIA => a_di(16*(n+1) -1 downto 16*n),      -- Port A 32-bit Data Input
      DIB => b_di(16*(n+1) -1 downto 16*n),      -- Port B 32-bit Data Input
      DIPA => (others=>'0'),    -- Port A 4-bit parity Input
      DIPB => (others=>'0'),    -- Port-B 4-bit parity Input
      ENA => a_en(n),      -- Port A RAM Enable Input
      ENB => b_en(n),      -- PortB RAM Enable Input
      SSRA => '0',    -- Port A Synchronous Set/Reset Input
      SSRB => '0',    -- Port B Synchronous Set/Reset Input
      WEA => a_we,      -- Port A Write Enable Input
      WEB => b_we       -- Port B Write Enable Input
  );

end generate;

end generate;

w11: if ABITS=13 generate

rams: for n in 0 to 3 generate

ram0: RAMB16_S9_S9
  generic map (
    WRITE_MODE_A => "WRITE_FIRST",
    WRITE_MODE_B => "WRITE_FIRST"
--    SIM_COLLISION_CHECK => "NONE"
  )
  port map (
      DOA => a_do(8*(n+1) -1 downto 8*n),      -- Port A 32-bit Data Output
      DOB => b_do(8*(n+1) -1 downto 8*n),      -- Port B 32-bit Data Output
      DOPA => open,    -- Port A 4-bit Parity Output
      DOPB => open,    -- Port B 4-bit Parity Output
      ADDRA => a_a,  -- Port A 9-bit Address Input
      ADDRB => b_a,  -- Port B 9-bit Address Input
      CLKA => a_clk,    -- Port A Clock
      CLKB => b_clk,    -- Port B Clock
      DIA => a_di(8*(n+1) -1 downto 8*n),      -- Port A 32-bit Data Input
      DIB => b_di(8*(n+1) -1 downto 8*n),      -- Port B 32-bit Data Input
      DIPA => (others=>'0'),    -- Port A 4-bit parity Input
      DIPB => (others=>'0'),    -- Port-B 4-bit parity Input
      ENA => a_en(n),      -- Port A RAM Enable Input
      ENB => b_en(n),      -- PortB RAM Enable Input
      SSRA => '0',    -- Port A Synchronous Set/Reset Input
      SSRB => '0',    -- Port B Synchronous Set/Reset Input
      WEA => a_we,      -- Port A Write Enable Input
      WEB => b_we       -- Port B Write Enable Input
  );

end generate;

end generate;

w12: if ABITS=14 generate

rams: for n in 0 to 7 generate

ram0: RAMB16_S4_S4
  generic map (
    WRITE_MODE_A => "WRITE_FIRST",
    WRITE_MODE_B => "WRITE_FIRST",
    SIM_COLLISION_CHECK => "NONE"
  )
  port map (
      DOA => a_do(4*(n+1) -1 downto 4*n),      -- Port A 32-bit Data Output
      DOB => b_do(4*(n+1) -1 downto 4*n),      -- Port B 32-bit Data Output
      ADDRA => a_a(ABITS-1 downto DWORDBITS),  -- Port A 9-bit Address Input
      ADDRB => b_a(ABITS-1 downto DWORDBITS),  -- Port B 9-bit Address Input
      CLKA => a_clk,    -- Port A Clock
      CLKB => b_clk,    -- Port B Clock
      DIA => a_di(4*(n+1) -1 downto 4*n),      -- Port A 32-bit Data Input
      DIB => b_di(4*(n+1) -1 downto 4*n),      -- Port B 32-bit Data Input
      ENA => a_en(n/2),      -- Port A RAM Enable Input
      ENB => b_en(n/2),      -- PortB RAM Enable Input
      SSRA => '0',    -- Port A Synchronous Set/Reset Input
      SSRB => '0',    -- Port B Synchronous Set/Reset Input
      WEA => a_we,      -- Port A Write Enable Input
      WEB => b_we       -- Port B Write Enable Input
  );

end generate;

end generate;

err: if ABITS/=11 and ABITS/=12 and ABITS/=13 and ABITS/=14 generate

errproc: process
begin
  report "ERROR: no implementation for RAM of address size " & str(ABITS) & " bits, word size " & str(DBITS) severity failure;
end process;

end generate;

end behave;
