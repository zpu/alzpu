library ieee;
use ieee.std_logic_1164.all;
library work;
use work.alzpupkg.all;
use work.alzpu_slaveselect.all;
use work.alzpu_config.all;

entity benchmark is
end benchmark;

architecture behave of benchmark is

  constant period : time := 20 ns;

  signal w_clk : std_logic := '0';
  signal w_rst : std_logic := '0';
  signal w_sleep: std_logic := '0';

  signal txd: std_logic;

  component alzpu_system is
  port (
    clk:  in std_logic;
    rst:  in std_logic;
    txd:  out std_logic;
    sleep: in std_logic
  );
  end component;

begin

  core : alzpu_system
    port map(
      clk   => w_clk,
      rst   => w_rst,
      txd => txd,
      sleep => w_sleep
  );

  w_clk <= not w_clk after period/2;

   stimuli : process
   begin
      w_rst   <= '0';
      wait for 1 ns;
      w_rst   <= '1';
      wait for 40 ns;
      w_rst   <= '1';
      wait for 80 ns;
      w_rst   <= '0';
      wait for 400 ns;
     -- w_sleep <= '1';
      wait for 300 ns;
--      w_sleep <= '0';
      wait for 9000 ns; --1635
  --    w_sleep <= '1';
      wait for 62 ns;
     -- w_sleep <='0';
      wait;
   end process stimuli;

end behave;



