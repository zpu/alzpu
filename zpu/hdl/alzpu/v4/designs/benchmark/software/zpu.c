
static volatile int *debugport = (int*)0x0EFEFEFE;

extern char __end__;
static char *start_brk = &__end__;

extern int _cpu_config;

int _use_syscall;


extern int _syscall(int *foo, int ID, ...);

extern int main(int argc, char **argv);

static char *args[]={"dummy.exe"};
unsigned int _memreg[4];

extern void _init(void);
void _initIO(void);

extern unsigned char __ram_start,__data_start,__data_end;
extern unsigned char ___jcr_start,___jcr_begin,___jcr_end;
extern unsigned char __bss_start__, __bss_end__;

const unsigned int ZPU_ID=1;
static volatile int *UART;
static volatile int *TIMER;
volatile int *MHZ;

void __copy_data(void)
{
	unsigned int *cptr;
	cptr = (unsigned int*)&__ram_start;
	unsigned int *dptr;
	dptr = (unsigned int*)&__data_start;

	do {
		*dptr = *cptr;
		cptr++,dptr++;
	} while (dptr<(unsigned int*)(&__data_end));

	cptr = (unsigned int*)&___jcr_start;
	dptr = (unsigned int*)&___jcr_begin;


	while (dptr<(unsigned int*)(&___jcr_end)) {
		*dptr = *cptr;
		cptr++,dptr++;
	}
}


/*
 * Wait indefinitely for input byte
 */

int inbyte()
{
	int val;
	for (;;)
	{
		val=UART[1];
		if ((val&0x100)!=0)
		{
			return val&0xff;
		}
	}
}



/* 
 * Output one character to the serial port 
 * 
 * 
 */
void outbyte(int c)
{
	/* Wait for space in FIFO */
	while ((UART[0]&0x100)==0);
	UART[0]=c;
}

static const int mhz=50;

void _initIO(void)
{
	UART=(volatile int *) 0x00008000;
	TIMER=(volatile int *)0x0000C000;
	MHZ=(volatile int *)&mhz;
}

void _premain()
{
	int t;
	__copy_data();
	_initIO();
	_init();
	t=main(1, args);
	exit(t);
	for (;;);
}


void *sbrk(int inc)
{
	start_brk+=inc;
	return start_brk;
}
