-- ZPU / ALZPU
--
-- Copyright 2004-2008 oharboe - �yvind Harboe - oyvind.harboe@zylin.com
-- Copyright 2008-2009 alvieboy - �lvaro Lopes - alvieboy@alvie.com
-- 
-- The FreeBSD license
-- 
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above
--    copyright notice, this list of conditions and the following
--    disclaimer in the documentation and/or other materials
--    provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE ZPU PROJECT ``AS IS'' AND ANY
-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
-- PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
-- ZPU PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
-- INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
-- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
-- STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
-- ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
-- The views and conclusions contained in the software and documentation
-- are those of the authors and should not be interpreted as representing
-- official policies, either expressed or implied, of the ZPU Project.


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package alzpu_config is

  type zpu_tech_type is ( none, virtex2 );

	constant wordPower			    : integer := 5;
	-- during simulation, set this to '0' to get matching trace.txt 
	constant	DontCareValue		  : std_logic := '0';
	-- Clock frequency in MHz.
	constant	ZPU_Frequency		  : integer := 50;

	-- This is the msb address bit. bytes=2^(maxAddrBitIncIO+1)
	constant maxAddrBitIncIO		: integer := 15;
  constant maxAddrBit		      : integer := maxAddrBitIncIO-1;

  --constant 	maxAddrBit		    : integer := 15;

  constant  maxPCBit          : integer := 13; -- Set to maxAddrBit if PC allowed for RAM

  -- Make sure you put enough stack bits here.
  -- Stack size = wordSize ** stackSizeBits
  constant stackSizeBits      : integer := 7;

	-- start byte address of stack. 
	-- point to top of RAM - 2*words
  constant 	spStart				    : unsigned(stackSizeBits-1+2 downto 2) := to_unsigned( 2**(stackSizeBits) - 2, stackSizeBits+1-1 );

  -- Number of RAM address bits.
  constant alzpu_ram_num_bits: positive := 14;

  -- Number of ROM address bits.
  constant alzpu_rom_num_bits: positive := 14;

	-- Generate trace output or not.
  constant alzpu_tracer_enabled: boolean := true;

  -- UART definitions
  constant uart_baud_rate:  integer := 38600;
  constant brgen_count:     integer := (ZPU_Frequency * 1000000) / uart_baud_rate;

  -- Use external validator file. Do not enable unless you know what you're doing.
  constant alzpu_validator_enabled: boolean := false;

  constant zpu_tech: zpu_tech_type := virtex2;

  constant alzpu_debug_ram_accesses: boolean := false;

  -- Emulated instruction implementation

  constant implement_loadb: boolean := true;
  constant implement_neqbranch: boolean := true;
  constant implement_eq: boolean := true;
  constant implement_ulessthanorequal: boolean := true;
  constant implement_ulessthan: boolean := true;
  constant implement_lessthanorequal: boolean := true;
  constant implement_lessthan: boolean := true;
  constant implement_sub: boolean := true;
  constant implement_mult: boolean := true;

  -- ***WARNING WARNING***
  -- STOREB implementation does not work for IO.

  constant implement_storeb: boolean := true;

  --constant implement_call: boolean := false;
  --constant implement_mult: boolean := false;
  --constant implement_pushspadd: boolean := false;
  --constant implement_callpcrel: boolean := false;
  --constant implement_poppcrel: boolean := false;

  -- SHR is broken. don't use.
  constant implement_lshiftright: boolean := false;

  -- Multiplier delay, in clock cycles
  constant multiplier_delay: integer := 5;

  -- Improved hazard detection - but reduces maxFreq, and uses a few luts. Useful if you can easily meet timing
  -- for your design, and you don't have size constraints.
  constant improved_hazard_prevention: boolean := true;
  --constant no_stack_forwarding: boolean := true;

end alzpu_config;
