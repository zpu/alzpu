#!/bin/sh

mkdir -p xst/projnav.tmp
echo work > alzpu_system.lso
xst -ifn synthesize.xst || exit -1
ngdbuild -intstyle ise -dd _ngo -p xc3s500e-4-fg320 -uc alzpu_system.ucf alzpu_system || exit -1
map alzpu_system || exit -1
par -pl high -rl high alzpu_system -w alzpu_system_routed || exit -1
bitgen -w alzpu_system_routed
