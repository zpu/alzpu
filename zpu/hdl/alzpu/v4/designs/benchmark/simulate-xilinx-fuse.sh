#!/bin/sh

TEST_MODULE=benchmark
PROJECT=alzpu
PROJDIR=$(pwd)
TIME=50000000

SIGS=sim/xilinx-fuse/signals.txt

rm -f ${TEST_MODULE}_isim_beh.exe

fuse -d SIM_COMPILE -d SIMULATION \
-intstyle ise -incremental -lib unisims_ver -lib xilinxcorelib_ver \
-o ${TEST_MODULE}_isim_beh.exe -prj sim/xilinx-fuse/${TEST_MODULE}.prj \
-top ${TEST_MODULE} -top glbl || exit -1

perl ../../../utils/scripts/gensignals.pl $SIGS > isim.cmd || exit -1

cat >> isim.cmd <<EOM
ntrace start
run ${TIME} ns;
quit; 
EOM
test -f ./${TEST_MODULE}_isim_beh.exe && ./${TEST_MODULE}_isim_beh.exe -tclbatch isim.cmd
