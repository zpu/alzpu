-- ZPU/ALZPU
--
-- Copyright 2004-2008 oharboe - �yvind Harboe - oyvind.harboe@zylin.com
-- Copyright 2008 alvieboy - �lvaro Lopes - alvieboy@alvie.com
-- 
-- The FreeBSD license
-- 
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above
--    copyright notice, this list of conditions and the following
--    disclaimer in the documentation and/or other materials
--    provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE ZPU PROJECT ``AS IS'' AND ANY
-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
-- PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
-- ZPU PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
-- INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
-- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
-- STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
-- ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
-- The views and conclusions contained in the software and documentation
-- are those of the authors and should not be interpreted as representing
-- official policies, either expressed or implied, of the ZPU Project.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.alzpupkg.all;
use work.alzpu_slaveselect.all;
use work.alzpu_config.all;

entity alzpu_system is
  port (
    clk:  in std_logic;
    rst:  in std_logic;
    -- UART
    txd:  out std_logic;
    sleep: in std_logic
  );
end alzpu_system;

architecture behave of alzpu_system is

  component alzpu is
  port (
    clk:        in std_logic;
    rst:        in std_logic;
    interrupt:  in std_logic;
    break:      out std_logic;
    sleep:      in std_logic;

    io_o_addr:        out std_logic_vector(maxAddrBit downto minAddrBit);
    io_o_wr_data:     out cpuword_type;
    io_o_clk:         out std_logic;
    io_o_rst:         out std_logic;
    io_i_busy:        in std_logic;
    io_i_rd_data:     in cpuword_type;
    io_o_en:          out std_logic;
    io_o_wr_en:       out std_logic;

    rom_o_addra:      out std_logic_vector(alzpu_rom_num_bits-1 downto 0);
    rom_o_addrb:      out std_logic_vector(alzpu_rom_num_bits-1 downto 2);
    rom_i_doa:        in std_logic_vector(7 downto 0);
    rom_i_dob:        in cpuword_type;
    rom_o_ena:        out std_logic;
    rom_o_enb:        out std_logic;

    ram_o_addra:      out std_logic_vector(alzpu_ram_num_bits-1 downto 2);
    ram_o_addrb:      out std_logic_vector(alzpu_ram_num_bits-1 downto 2);
    ram_i_doa:        in cpuword_type;
    ram_o_dib:        out cpuword_type;
    ram_o_ena:        out std_logic_vector(3 downto 0);
    ram_o_enb:        out std_logic_vector(3 downto 0)
  );
  end component;

  component alzpu_rom is
    port (
      clka:       in std_logic;
      clkb:       in std_logic;
      rst:        in std_logic;
      addra:      in std_logic_vector(alzpu_rom_num_bits-1 downto 0);
      addrb:      in std_logic_vector(alzpu_rom_num_bits-1 downto 2);
      ena:        in std_logic;
      enb:        in std_logic;
      wra:        in std_logic;
      wrb:        in std_logic;
      doa:        out std_logic_vector(7 downto 0);
      dob:        out cpuword_type;
      dia:        in std_logic_vector(7 downto 0);
      dib:        in cpuword_type
  );
  end component;

  component alzpu_ram is
    port (
      clka:       in std_logic;
      clkb:       in std_logic;
      rst:        in std_logic;
      addra:      in std_logic_vector(alzpu_ram_num_bits-1 downto 2);
      addrb:      in std_logic_vector(alzpu_ram_num_bits-1 downto 2);
      doa:        out cpuword_type;
      dob:        out cpuword_type;
      dia:        in cpuword_type;
      dib:        in cpuword_type;
      ena:        in std_logic_vector(3 downto 0);
      enb:        in std_logic_vector(3 downto 0);
      wra:        in std_logic;
      wrb:        in std_logic
    );
  end component;

  component timer is
   generic(
      MHZ: integer range 0 to 500 := 50
   );
   port(
      slave_in : in zpu_slave_in_type;
      slave_out : out zpu_slave_out_type
   );
  end component;

  component uart is
   generic(
      clk_mhz: integer := 50;
      baud_rate: integer := 38600;
      oversample: integer := 16
   );
   port(
      slave_in:   in zpu_slave_in_type;
      slave_out:  out zpu_slave_out_type;
      tx_d:       out std_logic;
      rx_d:       in  std_logic;
      intr:       out std_logic
   );
  end component;

  signal iosi: zpu_slave_in_type;
  signal ioso: zpu_slave_out_type;

  signal io_o_addr:        std_logic_vector(maxAddrBit downto minAddrBit);
  signal io_o_wr_data:     cpuword_type;
  signal io_o_clk:         std_logic;
  signal io_o_rst:         std_logic;
  signal io_i_busy:        std_logic;
  signal io_i_rd_data:     cpuword_type;
  signal io_o_en:          std_logic;
  signal io_o_wr_en:       std_logic;


  signal timer_si: zpu_slave_in_type;
  signal timer_so: zpu_slave_out_type;
  signal uart_si: zpu_slave_in_type;
  signal uart_so: zpu_slave_out_type;
  signal dummy_so: zpu_slave_out_type;

  signal rom_o_addra:      std_logic_vector(alzpu_rom_num_bits-1 downto 0);
  signal rom_o_addrb:      std_logic_vector(alzpu_rom_num_bits-1 downto 2);
  signal rom_i_doa:        std_logic_vector(7 downto 0);
  signal rom_i_dob:        cpuword_type;
  signal rom_o_ena:        std_logic;
  signal rom_o_enb:        std_logic;

  signal ram_o_addra:      std_logic_vector(alzpu_ram_num_bits-1 downto 2);
  signal ram_o_addrb:      std_logic_vector(alzpu_ram_num_bits-1 downto 2);
  signal ram_i_doa:        cpuword_type;
  signal ram_o_dib:        cpuword_type;
  signal ram_o_ena:        std_logic_vector(3 downto 0);
  signal ram_o_enb:        std_logic_vector(3 downto 0);

begin

  dummy_slave: alzpu_slave_unconnected
    port map (
      slave_out => dummy_so
  );

  ioss: alzpu_slaveselect4
    generic map (
      address_bits => alzpu_rom_num_bits+1,
      slave0_address_size => alzpu_ram_num_bits,
      slave1_address_size => alzpu_ram_num_bits,
      slave2_address_size => alzpu_ram_num_bits,
      slave3_address_size => alzpu_ram_num_bits
    )
    port map (
      master_in => iosi,
      master_out => ioso,

      slave_in_0 => uart_si,    -- 0x8000 00
      slave_out_0  => uart_so,
      slave_in_1 =>  open,     -- 0xA000 01
      slave_out_1  => dummy_so,
      slave_in_2 => timer_si,   -- 0xC000 10
      slave_out_2 => timer_so,
      slave_in_3 => open,       -- 0xE000 11
      slave_out_3 => dummy_so
  );

  mytimer: timer
    generic map (
        MHZ => ZPU_Frequency
    )
    port map (
      slave_in => timer_si,
      slave_out => timer_so
    );

  myuart: uart
    generic map(
      clk_mhz => ZPU_Frequency,
      baud_rate => uart_baud_rate
    )
    port map (
      slave_in => uart_si,
      slave_out => uart_so,
      tx_d => txd,
      rx_d => '1',
      intr => open
    );

  core : alzpu
    port map(
      clk           => clk,
      rst           => rst,
      io_o_addr     => io_o_addr,
      io_o_wr_data  => io_o_wr_data,
      io_o_clk      => io_o_clk,
      io_o_rst      => io_o_rst,
      io_i_busy     => io_i_busy,
      io_i_rd_data  => io_i_rd_data,
      io_o_en       => io_o_en,
      io_o_wr_en    => io_o_wr_en,
      interrupt     => '0',
      break         => open,
      sleep         => sleep,

      rom_o_addra   => rom_o_addra,
      rom_o_addrb   => rom_o_addrb,
      rom_i_doa     => rom_i_doa,
      rom_i_dob     => rom_i_dob,
      rom_o_ena     => rom_o_ena,
      rom_o_enb     => rom_o_enb,

      ram_o_addra   => ram_o_addra,
      ram_o_addrb   => ram_o_addrb,
      ram_i_doa     => ram_i_doa,
      ram_o_dib     => ram_o_dib,
      ram_o_ena     => ram_o_ena,
      ram_o_enb     => ram_o_enb
  );

  rom: alzpu_rom
    port map (
      clka    => clk,
      clkb    => clk,
      rst     => rst,
      addra   => rom_o_addra,
      addrb   => rom_o_addrb,
      doa     => rom_i_doa,
      dob     => rom_i_dob,
      dia     => (others => DontCareValue),
      dib     => (others => DontCareValue),
      ena     => rom_o_ena,
      enb     => rom_o_enb,
      wra     => '0',
      wrb     => '0'
  );

  ram: alzpu_ram
    port map (
      clka    => clk,
      clkb    => clk,
      rst     => rst,
      addra   => ram_o_addra,
      addrb   => ram_o_addrb,
      doa     => ram_i_doa,
      dob     => open,
      dia     => (others =>DontCareValue),
      dib     => ram_o_dib,
      ena     => ram_o_ena,
      enb     => ram_o_enb,
      wra     => '0',
      wrb     => '1'
    );

  iosi.clk <= clk;
  iosi.rst <= rst;
  iosi.dati <= std_logic_vector(io_o_wr_data);
  iosi.addr(maxAddrBit downto minAddrBit) <= io_o_addr;
  iosi.rd_en <= io_o_en and not io_o_wr_en;
  iosi.wr_en <= io_o_en and io_o_wr_en;
  io_i_rd_data <= unsigned(ioso.dato);
  io_i_busy <= ioso.busy;

end behave;



