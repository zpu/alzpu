------------------------------------------------------------------------------
----                                                                      ----
----  64 bits clock counter                                               ----
----                                                                      ----
----  http://www.opencores.org/                                           ----
----                                                                      ----
----  Description:                                                        ----
----  This is a peripheral used by the PHI I/O layout. It just counts the ----
----  elapsed number of clocks.                                           ----
----                                                                      ----
----  To Do:                                                              ----
----  -                                                                   ----
----                                                                      ----
----  Author:                                                             ----
----    - �yvind Harboe, oyvind.harboe zylin.com                          ----
----    - Salvador E. Tropea, salvador inti.gob.ar                        ----
----                                                                      ----
------------------------------------------------------------------------------
----                                                                      ----
---- Copyright (c) 2008 �yvind Harboe <oyvind.harboe zylin.com>           ----
---- Copyright (c) 2008 Salvador E. Tropea <salvador inti.gob.ar>         ----
---- Copyright (c) 2008 Instituto Nacional de Tecnolog�a Industrial       ----
----                                                                      ----
---- Distributed under the BSD license                                    ----
----                                                                      ----
------------------------------------------------------------------------------
----                                                                      ----
---- Design unit:      Timer(Behave) (Entity and architecture)            ----
---- File name:        timer.vhdl                                         ----
---- Note:             None                                               ----
---- Limitations:      None known                                         ----
---- Errors:           None known                                         ----
---- Library:          zpu                                                ----
---- Dependencies:     IEEE.std_logic_1164                                ----
----                   IEEE.numeric_std                                   ----
----                   zpu.zpupkg                                         ----
---- Target FPGA:      Spartan 3 (XC3S1500-4-FG456)                       ----
---- Language:         VHDL                                               ----
---- Wishbone:         No                                                 ----
---- Synthesis tools:  Xilinx Release 9.2.03i - xst J.39                  ----
---- Simulation tools: GHDL [Sokcho edition] (0.2x)                       ----
---- Text editor:      SETEdit 0.5.x                                      ----
----                                                                      ----
------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library work;
use work.alzpupkg.all;
use work.txt_util.all;

entity Timer is
   generic (
    MHZ: integer range 0 to 500 := 50
   );
   port(
      slave_in : in zpu_slave_in_type;
      slave_out : out zpu_slave_out_type
   );
end entity Timer;

architecture Behave of Timer is
   signal sample   : std_logic;
   signal reset    : std_logic;
   
   signal cnt      : unsigned(31 downto 0);
   signal i_cnt    : integer range 0 to MHZ-1;
   signal cnt_smp  : unsigned(31 downto 0);

begin
   slave_out.busy <= '0';

   process (slave_in.clk, slave_in.rst)
   begin
    if rising_edge(slave_in.clk) then
      if slave_in.rst='1' then
        i_cnt <= MHZ-1;
      else
        if i_cnt=0 then
          i_cnt <= MHZ-1;
        else
          i_cnt <= i_cnt - 1;
        end if;
      end if;
    end if;
   end process;

   process (slave_in.clk, slave_in.rst)
   begin
      if rising_edge(slave_in.clk) then
         if slave_in.rst='1' then
            cnt <= (others => '0');
            cnt_smp <= (others => '0');
         else

            if slave_in.wr_en='1' then
              if slave_in.dati(0)='1' then
                cnt_smp <= (others=>'0');
                cnt <= (others=>'0');
              end if;
              if slave_in.dati(1)='1' then
                cnt_smp <= cnt;
              end if;
            else
              if (i_cnt=0) then
                cnt <= cnt + 1;
              end if;
            end if;

            slave_out.data_valid <= '0';

            if slave_in.rd_en='1' then
              report "SAMP: Address " & hstr(slave_in.addr) severity note;
              slave_out.dato <= std_logic_vector(cnt_smp);
              --slave_out.dato <= (others => '0');
              slave_out.data_valid <= '1';  
            end if;

         end if; -- else reset_i='1'
      end if; -- rising_edge(clk_i)
   end process;

  
end Behave; -- Entity: Timer
 
