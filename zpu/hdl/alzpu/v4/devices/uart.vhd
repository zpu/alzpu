--
-- UART Controller
--
-- Copyright 2008,2009 �lvaro Lopes <alvieboy@alvie.com>
--
-- The FreeBSD license
-- 
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above
--    copyright notice, this list of conditions and the following
--    disclaimer in the documentation and/or other materials
--    provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY
-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
-- PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
-- ZPU PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
-- INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
-- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
-- STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
-- ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.alzpupkg.all;
use work.alzpu_config.all;

entity uart is
   generic(
      clk_mhz: integer := 50;
      baud_rate: integer := 38600;
      oversample: integer := 16
   );
   port(
      slave_in:   in zpu_slave_in_type;
      slave_out:  out zpu_slave_out_type;
      tx_d:       out std_logic;
      rx_d:       in  std_logic;
      intr:       out std_logic
   );
end entity uart;


architecture behave of uart is

component uart_tx is
  port (
     clk_i    : in  std_logic;  -- Clock signal
     reset_i  : in  std_logic;  -- Reset input
     enable_i : in  std_logic;  -- Enable input
     load_i   : in  std_logic;  -- Load input
     txd_o    : out std_logic;  -- RS-232 data output
     busy_o   : out std_logic;  -- Tx Busy
     datai_i  : in  std_logic_vector(7 downto 0)); -- Byte to transmit
end component uart_tx;

component uart_rx is
  port (
     clk_i:       in std_logic;
     reset_i:     in std_logic;
     rx_clken_i:  in std_logic;
     rxd_i:       in std_logic;
     clr_ready_i: in std_logic;
     data_ready_o:out std_logic;
     data_o:      out std_logic_vector(7 downto 0)
  );
end component uart_rx;

component brgen is
  generic(
     count : integer range 0 to 65535);-- count revolution
  port (
     clk_i   : in  std_logic;  -- clock
     reset_i : in  std_logic;  -- reset input
     ce_i    : in  std_logic;  -- chip enable
     o_o     : out std_logic); -- output
end component brgen;

signal clken: std_logic;
signal tx_clken: std_logic;
signal tx_data_en: std_logic;
signal tx_busy: std_logic;
signal tx_dly_clk: integer;-- range 0 to oversample-1;

signal rx_data: std_logic_vector(7 downto 0);
signal rx_data_ready: std_logic;

signal txd_int: std_logic;
signal rxread:  std_logic;

constant brgen_count:     integer := (clk_mhz * 1000000) / (baud_rate*oversample);
                                                                        
begin

mybrgen: brgen
  generic map (
    count => brgen_count
  )
  port map (
    clk_i   => slave_in.clk,
    reset_i => slave_in.rst,
    ce_i    => '1',
    o_o     => clken
  );

ospdiv: if oversample /= 1 generate
  process(slave_in.clk)
  begin
    if rising_edge(slave_in.clk) then
      if slave_in.rst='1' then
        tx_clken<='0';
        tx_dly_clk <= oversample - 1;
      else
        tx_clken<='0';
        if clken='1' then
          if tx_dly_clk=0 then
            tx_dly_clk <= oversample - 1;
            tx_clken <= '1';
          else
            tx_dly_clk <= tx_dly_clk - 1;
          end if;
        end if;
      end if;
    end if;
  end process;
end generate;

tx_d <= txd_int;

tx: uart_tx
  port map (
     clk_i    => slave_in.clk,
     reset_i    => slave_in.rst,
     enable_i => tx_clken,
     load_i   => tx_data_en,
     txd_o    => txd_int,
     busy_o   => tx_busy,
     datai_i  => slave_in.dati(7 downto 0)
  );

rx: uart_rx
  port map (
     clk_i    => slave_in.clk,
     reset_i  => slave_in.rst,
     rx_clken_i => clken,
     rxd_i    => rx_d,
     clr_ready_i   => rxread,
     data_ready_o   => rx_data_ready,
     data_o  => rx_data
  );

slave_out.busy <= '0';
tx_data_en <= slave_in.wr_en;

process(slave_in.addr)
begin
  slave_out.dato <= (others=>DontCareValue);
  rxread <= '0';

  case slave_in.addr(2) is
    when '0' =>
      slave_out.dato(8) <= not tx_busy;
      slave_out.dato(0) <= rx_data_ready;
    when '1' =>
      slave_out.dato(7 downto 0) <= rx_data;
      rxread <= slave_in.rd_en;
    when others =>
  end case;
end process;

slave_out.data_valid <= '1';

end behave; 
 
