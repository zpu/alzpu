--
-- UART Controller RX unit
--
-- Copyright 2008,2009 �lvaro Lopes <alvieboy@alvie.com>
--
-- The FreeBSD license
-- 
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above
--    copyright notice, this list of conditions and the following
--    disclaimer in the documentation and/or other materials
--    provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY
-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
-- PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
-- ZPU PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
-- INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
-- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
-- STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
-- ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.alzpupkg.all;
use work.alzpu_config.all;

entity uart_rx is
  generic (
    oversample: integer := 16
  );
  port (
     clk_i:       in std_logic;
     reset_i:     in std_logic;
     rx_clken_i:  in std_logic;
     rxd_i:       in std_logic;
     clr_ready_i: in std_logic;
     data_ready_o:out std_logic;
     data_o:      out std_logic_vector(7 downto 0)
  );
end entity uart_rx;

architecture behave of uart_rx is

type rxstate_type is (
  idle,
  start,
  receive,
  stop
);

signal count_in: integer range 0 to oversample + oversample/2;
signal state: rxstate_type;
signal bits: integer range 0 to 8;
signal shiftbits: std_logic_vector(9 downto 0); -- 10 bits.

signal debug_sample_bit: std_logic;

begin

process(clk_i)
begin
  if rising_edge(clk_i) then
    if reset_i='1' then
      state <= idle;
      debug_sample_bit <= '0';
      data_ready_o <= '0';
      count_in <= oversample + oversample/2 - 2;
    else
      if clr_ready_i='1' then
        data_ready_o <= '0';
      end if;

      if rx_clken_i='1' then

        debug_sample_bit <= '0';

        case state is
          when idle =>
            if rxd_i='0' then
              shiftbits<=(others=>'0');
              shiftbits(7) <= '1';
              count_in <= oversample + oversample/2 - 2;
              state <= start;
            end if;
          when start =>
            -- Do some debouncing/glitch avoidance here
            if count_in = 0 then
              state <= receive;
              shiftbits(7) <= rxd_i;
              shiftbits(6 downto 0) <= shiftbits(7 downto 1);

              debug_sample_bit <= '1';
              bits <= 0;
              count_in <= oversample - 1;
            else
              count_in <= count_in - 1;
            end if;
          when receive =>
            if count_in = 0 then
              debug_sample_bit <= '1';
              shiftbits(7) <= rxd_i;
              shiftbits(6 downto 0) <= shiftbits(7 downto 1);
              count_in <= oversample - 1;
              if shiftbits(0)='1' then
                state <= stop;
              end if;
            else
              count_in <= count_in -1;
            end if;
            
          when stop =>
            if count_in = 0 then
              if rxd_i='1' then
                data_o <= shiftbits(7 downto 0);
                data_ready_o <= '1';
                state <= idle;
              end if;
            else
              count_in <= count_in - 1;
            end if;
        end case;
      end if;
    end if;
  end if;
end process;


end behave;

