--
-- SPI flash read-only controller, IO device mode
--
-- Copyright 2008,2009 �lvaro Lopes <alvieboy@alvie.com>
--
-- Version: 1.0
--
-- The FreeBSD license
-- 
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above
--    copyright notice, this list of conditions and the following
--    disclaimer in the documentation and/or other materials
--    provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY
-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
-- PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
-- ZPU PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
-- INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
-- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
-- STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
-- ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--
-- Changelog:
--
-- 1.1: First version, imported from old controller.
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.alzpupkg.all;
use work.txt_util.all;

entity spi is
  generic (
    INIT_CLOCK_CYCLE_WAIT : integer := 2;   -- Clock cycles to wait before init
    DESELECT_CYCLES : integer := 3;         -- Clock cycles to wait after deselection - should give 100ns at least
    SPI_REGISTER_SIZE: integer := 40;
    SPI_ADDRESS_SIZE: integer := 24
  );
  port (
      slave_in:   in zpu_slave_in_type;
      slave_out:  out zpu_slave_out_type;
      SPI_MOSI:   out std_logic;
      SPI_MISO:   in std_logic;
      SPI_CLK:    out std_logic;
      SPI_SELN:   out std_logic
  );
end entity spi;

architecture behave of spi is

type spi_state_type is (
  SPI_STATE_WAIT,
  SPI_STATE_IDLE,
  SPI_STATE_WACK,
  SPI_STATE_SEND,
  SPI_STATE_BREAD,
  SPI_STATE_READ,
  SPI_STATE_WDES
);

-- SPI commands
constant SPI_CMD_READ_FAST: std_logic_vector( 7downto 0) := "00001011";

-- Shift register to hold command to be sent to SPI
signal spi_shift_register_out: std_logic_vector(SPI_REGISTER_SIZE-1 downto 0);

signal spi_reg_count: integer;
signal spi_read_count: std_logic_vector(7 downto 0);
--signal spi_data: std_logic_vector(7 downto 0);
signal spi_data: std_logic_vector(31 downto 0);
signal data_valid_window: std_logic_vector(4 downto 0);
--signal next_address: std_logic_vector(SPI_REGISTER_SIZE-1 downto 0);


signal dsel_dly: integer;
signal spi_init_count: integer;

signal spi_start_count: std_logic;
signal spi_enable_clock: std_logic;   -- Enable SPI clock
signal spi_state: spi_state_type;    -- SPI state machine
signal seq_read: std_logic;  -- Sequential read in progress
signal clk_inv: std_logic;
signal done: std_logic;
--signal adr: std_logic_vector(SPI_REGISTER_SIZE-1 downto 0);
signal adr: std_logic_vector(31 downto 0);
signal start_read: std_logic;

begin

-- SPI clock generation
clk_inv <= not slave_in.clk;
SPI_CLK <= clk_inv when spi_enable_clock='1' else '0';
--SPI_CLK <= slave_in.clk when spi_enable_clock='1' else '0';

slave_out.busy <= '0';

process(slave_in.clk)
begin
  if rising_edge(slave_in.clk) then
   if slave_in.rst='1' then
      spi_enable_clock  <= '0';
      spi_state         <= SPI_STATE_WAIT;
      spi_init_count    <= INIT_CLOCK_CYCLE_WAIT;
      SPI_SELN          <= '1';
      done              <= '1';
      spi_start_count   <= '0';
    else
      case spi_state is

        when SPI_STATE_WAIT =>
          if spi_init_count=0 then
            spi_state <= SPI_STATE_IDLE;
          else
            spi_init_count <= spi_init_count - 1;
          end if;

        when SPI_STATE_IDLE =>
          if start_read='1' then
            done <= '0';

            spi_shift_register_out <=  SPI_CMD_READ_FAST & adr(SPI_ADDRESS_SIZE-1 downto 2) & "00";
            spi_enable_clock <= '1';

            if seq_read='1' then
              spi_state <= SPI_STATE_BREAD;
            else
              SPI_SELN <= '1';
              spi_reg_count <= SPI_REGISTER_SIZE;
              dsel_dly <= DESELECT_CYCLES;
              spi_state <= SPI_STATE_WDES;
            end if;
          end if;
--        when SPI_STATE_WACK =>
--            ack <= '0';
--            spi_state <= SPI_STATE_IDLE;

        when SPI_STATE_SEND =>
          SPI_SELN <= '0';

          if spi_reg_count=0 then
            spi_state <= SPI_STATE_BREAD;
          else
            SPI_MOSI <= spi_shift_register_out(SPI_REGISTER_SIZE-1);
            spi_shift_register_out <= spi_shift_register_out(SPI_REGISTER_SIZE-2 downto 0) & '0';
            spi_reg_count <= spi_reg_count - 1;
          end if;

        when SPI_STATE_BREAD =>
          spi_start_count <= '1';
          spi_state <= SPI_STATE_READ;

        when SPI_STATE_READ =>
          spi_start_count <= '0';

          -- Stop clock a bit earlier
          if data_valid_window(3)='1' and spi_read_count(1)='1' then
            spi_enable_clock <= '0';
          end if;

            if spi_read_count(0)='1' then
              -- Better a full shifter here?

              --dat_o <= dat_o(SPI_ADDRESS_SIZE-1 downto 0) & spi_data;

              if data_valid_window(3)='1' then
                done <= '1'; -- done
                spi_state <= SPI_STATE_IDLE;--WACK;
              end if;
            end if;

        when SPI_STATE_WDES =>
          if dsel_dly=0 then
            spi_state <= SPI_STATE_SEND;
          else
            dsel_dly <= dsel_dly -1;
          end if;
        when others =>
          report "invalid SPI state" severity failure;
      end case;
   end if;
  end if;
end process;


-- Control process

process(slave_in.clk)
begin
  if rising_edge(slave_in.clk) then
    if slave_in.rst='1' then
      --next_address      <= (others => '1');
      adr <= (others => '0');
      start_read <= '0';
      seq_read <= '0';
      slave_out.data_valid <= '0';
    else
      start_read <= '0';
      slave_out.data_valid <= '0';
      if slave_in.rd_en='1' then
        report "READ ACCESS " & hstr(slave_in.addr) severity note;
        case slave_in.addr(2) is
          when '0' =>
            slave_out.dato <= (others => '0');
            slave_out.dato(0) <= done;
          when '1' =>
            slave_out.dato <= spi_data;
          when others =>
        end case;
        slave_out.data_valid <= '1';
      elsif slave_in.wr_en='1' then
        report "WRITE ACCESS " & hstr(slave_in.dati) severity note;
        adr <= (others => '0');
        adr(SPI_ADDRESS_SIZE-1 downto 2) <= slave_in.dati(SPI_ADDRESS_SIZE-1 downto 2);
        start_read <= '1';
        seq_read <= slave_in.dati(wordSize-1); -- MSB

        -- we provide this. seq_read = adr[SPI_ADDRESS_SIZE-1:2] == next_address[SPI_ADDRESS_SIZE-1:2];
            -- Latch address (24 bit wordsize)

      end if;

    end if;
  end if;
end process;


process(slave_in.clk)
begin
  if rising_edge(slave_in.clk) then
   if spi_start_count='1' then
    spi_read_count <= "01000000";
    data_valid_window <= "00001";
   else
    if spi_read_count(0)='1' then
      data_valid_window <= data_valid_window(3 downto 0) & '0';
    end if;

    spi_read_count <= spi_read_count(0) & spi_read_count(7 downto 1);
   end if;
  end if;
end process;

-- SPI data shift register

process(slave_in.clk)
begin
  if falling_edge(slave_in.clk) then
    if spi_enable_clock='1' then
      spi_data <= spi_data(30 downto 0) & SPI_MISO;
    end if;
  end if;
end process;

end architecture behave;
