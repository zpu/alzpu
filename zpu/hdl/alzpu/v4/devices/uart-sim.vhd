
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;

library work;
use work.alzpupkg.all;
use work.txt_util.all;

entity uart is
   generic(
      clk_mhz: integer := 50;
      baud_rate: integer := 38600;
      oversample: integer := 16
   );
   port(
      slave_in:   in zpu_slave_in_type;
      slave_out:  out zpu_slave_out_type;
      tx_d:       out std_logic;
      rx_d:       in  std_logic;
      intr:       out std_logic
   );
end entity uart;


architecture behave of uart is

-- synthesis translate_off
  file 		lf:   TEXT open write_mode is "uart.txt";
-- synthesis translate_on

begin

process(slave_in.clk, slave_in.rst)
begin
  if rising_edge(slave_in.clk) then
    if slave_in.rst='1' then
      slave_out.data_valid <= '0';
    else
      slave_out.data_valid <= '0';
      if slave_in.rd_en='1' then
        slave_out.data_valid <= '1';
        slave_out.dato <= (others=>'0');
        slave_out.dato(8) <= '1';
      end if;
      if slave_in.wr_en='1' then
        report "UART: Transmitting data " & hstr(slave_in.dati(7 downto 0)) severity note;
        print( lf, character'val( to_integer(unsigned(slave_in.dati( 7 downto 0 ) )  mod 256 ) ) );
      end if;
    end if;
  
  end if;
end process;

end behave; 
 
