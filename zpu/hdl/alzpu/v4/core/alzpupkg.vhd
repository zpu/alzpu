-- ZPU/ALZPU
--
-- Copyright 2004-2008 oharboe - �yvind Harboe - oyvind.harboe@zylin.com
-- Copyright 2008 alvieboy - �lvaro Lopes - alvieboy@alvie.com
-- 
-- The FreeBSD license
-- 
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above
--    copyright notice, this list of conditions and the following
--    disclaimer in the documentation and/or other materials
--    provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE ZPU PROJECT ``AS IS'' AND ANY
-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
-- PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
-- ZPU PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
-- INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
-- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
-- STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
-- ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
-- The views and conclusions contained in the software and documentation
-- are those of the authors and should not be interpreted as representing
-- official policies, either expressed or implied, of the ZPU Project.

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

library work;
use work.alzpu_config.all;
use work.txt_util.all;

package alzpupkg is

	-- This bit is set for read/writes to IO
	-- FIX!!! eventually this should be set to wordSize-1 so as to
	-- to make the address of IO independent of amount of memory
	-- reserved for CPU. Requires trivial tweaks in toolchain/runtime
	-- libraries.
	
	constant byteBits			: integer := wordPower-3; -- # of bits in a word that addresses bytes
	constant ioBit				: integer := maxAddrBit+1;
	constant wordSize			: integer := 2**wordPower;
	constant wordBytes		: integer := wordSize/8;
	constant minAddrBit		: integer := byteBits;
	-- configurable internal stack size. Probably going to be 16 after toolchain is done
--	constant	stack_bits	: integer := 5;
--	constant	stack_size	: integer := 2**stack_bits;

  type memif_in_type is record
    address: std_logic_vector(maxAddrBitIncIO-1 downto 0);
    write_data: std_logic_vector( wordSize-1 downto 0 );
    write_mask: std_logic_vector( wordBytes-1 downto 0);
    write_enable: std_logic;
    read_enable: std_logic;
  end record;

  type memif_out_type is record
    read_data: std_logic_vector( wordSize-1 downto 0 );
    busy: std_logic;
  end record;
	
	-- opcode decode constants
	constant	OpCode_Im		: std_logic_vector(7 downto 7) := "1";
	constant	OpCode_StoreSP	: std_logic_vector(7 downto 5) := "010";
	constant	OpCode_LoadSP	: std_logic_vector(7 downto 5) := "011";
	constant	OpCode_Emulate	: std_logic_vector(7 downto 5) := "001";
	constant	OpCode_AddSP	: std_logic_vector(7 downto 4) := "0001";
	constant	OpCode_Short	: std_logic_vector(7 downto 4) := "0000";
	
	constant	OpCode_Break	: std_logic_vector(3 downto 0) := "0000";
	constant	OpCode_NA4      : std_logic_vector(3 downto 0) := "0001";
	constant	OpCode_PushSP	: std_logic_vector(3 downto 0) := "0010";
	constant	OpCode_NA3		: std_logic_vector(3 downto 0) := "0011";
	
	constant	OpCode_PopPC	: std_logic_vector(3 downto 0) := "0100";
	constant	OpCode_Add		: std_logic_vector(3 downto 0) := "0101";
	constant	OpCode_And		: std_logic_vector(3 downto 0) := "0110";
	constant	OpCode_Or		: std_logic_vector(3 downto 0) := "0111";
	
	constant	OpCode_Load		: std_logic_vector(3 downto 0) := "1000";
	constant	OpCode_Not		: std_logic_vector(3 downto 0) := "1001";
	constant	OpCode_Flip		: std_logic_vector(3 downto 0) := "1010";
	constant	OpCode_Nop		: std_logic_vector(3 downto 0) := "1011";
	
	constant	OpCode_Store	: std_logic_vector(3 downto 0) := "1100";
	constant	OpCode_PopSP	: std_logic_vector(3 downto 0) := "1101";
	constant	OpCode_NA2		: std_logic_vector(3 downto 0) := "1110";
	constant	OpCode_NA		: std_logic_vector(3 downto 0) := "1111";
	
	constant	OpCode_Lessthan				: std_logic_vector(5 downto 0) := std_logic_vector(to_unsigned(36, 6));
	constant	OpCode_Lessthanorequal		: std_logic_vector(5 downto 0) := std_logic_vector(to_unsigned(37, 6));
	constant	OpCode_Ulessthan			: std_logic_vector(5 downto 0) := std_logic_vector(to_unsigned(38, 6));
	constant	OpCode_Ulessthanorequal		: std_logic_vector(5 downto 0) := std_logic_vector(to_unsigned(39, 6));

	constant	OpCode_Swap					: std_logic_vector(5 downto 0) := std_logic_vector(to_unsigned(40, 6));
	constant	OpCode_Mult					: std_logic_vector(5 downto 0) := std_logic_vector(to_unsigned(41, 6));
	
	constant	OpCode_Lshiftright			: std_logic_vector(5 downto 0) := std_logic_vector(to_unsigned(42, 6));
	constant	OpCode_Ashiftleft			: std_logic_vector(5 downto 0) := std_logic_vector(to_unsigned(43, 6));
	constant	OpCode_Ashiftright			: std_logic_vector(5 downto 0) := std_logic_vector(to_unsigned(44, 6));
	constant	OpCode_Call					: std_logic_vector(5 downto 0) := std_logic_vector(to_unsigned(45, 6));

	constant	OpCode_Eq					: std_logic_vector(5 downto 0) := std_logic_vector(to_unsigned(46, 6));
	constant	OpCode_Neq					: std_logic_vector(5 downto 0) := std_logic_vector(to_unsigned(47, 6));

	constant	OpCode_Sub					: std_logic_vector(5 downto 0) := std_logic_vector(to_unsigned(49, 6));
	constant	OpCode_Loadb				: std_logic_vector(5 downto 0) := std_logic_vector(to_unsigned(51, 6));
	constant	OpCode_Storeb				: std_logic_vector(5 downto 0) := std_logic_vector(to_unsigned(52, 6));

	constant	OpCode_Eqbranch				: std_logic_vector(5 downto 0) := std_logic_vector(to_unsigned(55, 6));
	constant	OpCode_Neqbranch			: std_logic_vector(5 downto 0) := std_logic_vector(to_unsigned(56, 6));
	constant	OpCode_Poppcrel				: std_logic_vector(5 downto 0) := std_logic_vector(to_unsigned(57, 6));

	constant	OpCode_Pushspadd			: std_logic_vector(5 downto 0) := std_logic_vector(to_unsigned(61, 6));
	constant	OpCode_Mult16x16			: std_logic_vector(5 downto 0) := std_logic_vector(to_unsigned(62, 6));
	constant	OpCode_Callpcrel			: std_logic_vector(5 downto 0) := std_logic_vector(to_unsigned(63, 6));
	


	constant OpCode_Size		: integer := 8;

type InsnType is
(
Insn_AddTop,
Insn_Dup,
Insn_DupStackB,
Insn_Pop,
Insn_Popdown,
Insn_Add,
Insn_Or,
Insn_And,
Insn_Store,
Insn_AddSP,
Insn_Shift,
Insn_Nop,
Insn_Im,
Insn_LoadSP,
Insn_StoreSP,
Insn_Emulate,
Insn_Load,
Insn_PushPC,
Insn_PushSP,
Insn_PopPC,
Insn_Not,
Insn_Flip,
Insn_PopSP,
Insn_Break,
Insn_StoreSP2,
Insn_StoreSP3,
Insn_LoadB,
Insn_Eq,
Insn_Ulessthan,
Insn_Ulessthanorequal,
Insn_Lessthan,
Insn_Lessthanorequal,
Insn_Neqbranch,
Insn_StoreB,
Insn_Sub,
Insn_Mult,
Insn_Lshiftright,
Insn_unknown
);

type zpu_slave_in_type is record
  clk   : std_logic;
  rst   : std_logic;
  rd_en : std_logic;
  wr_en : std_logic;
  addr  : std_logic_vector(maxAddrBitIncIO downto 0);
  dati  : std_logic_vector(wordSize-1 downto 0);
end record;

type zpu_slave_out_type is record
  dato  : std_logic_vector(wordSize-1 downto 0);
  busy  : std_logic;
  data_valid: std_logic;
end record;

subtype sp_type is unsigned(stackSizeBits+1 downto minAddrBit);
subtype pc_type is unsigned(maxPCBit downto 0);
subtype memaddr_type is unsigned(maxAddrBitIncIO downto minAddrBit);
subtype cpuword_type is unsigned(wordSize-1 downto 0);

type memory_target_type is (
  TARGET_ROM,
  TARGET_RAM,
  TARGET_IO
);

function sp_to_memaddr(sp: sp_type) return memaddr_type;
--function pc_to_memaddr(pc: pc_type) return memaddr_type;

function sp_to_cpuword(sp: sp_type) return cpuword_type;
function cpuword_to_sp(word: cpuword_type) return sp_type;

function pc_to_cpuword(pc: pc_type) return cpuword_type;
function cpuword_to_pc(word: cpuword_type) return pc_type;
function str(b: InsnType; o: unsigned(4 downto 0) ) return string;
function memaddr_target(addr: memaddr_type) return memory_target_type;
function str(ti: TIME) return STRING;

end alzpupkg;


package body alzpupkg is

function sp_to_memaddr(sp: sp_type) return memaddr_type is
  variable maddr: memaddr_type;
begin
  maddr := (others => '0');
  maddr(stackSizeBits-1+minAddrBit downto minAddrBit) := sp;

  maddr(maxAddrBit downto stackSizeBits+minAddrBit) := (others => '1');
  return maddr;
end sp_to_memaddr;

function memaddr_target(addr: memaddr_type) return memory_target_type is
  variable t: memory_target_type;
begin
  if addr(maxAddrBitIncIO)='1' then
    t:=TARGET_IO;
  else
    if addr(maxAddrBit)='1' then
      t:=TARGET_RAM;
    else
      t:=TARGET_ROM;
    end if;
  end if;
  return t;
end memaddr_target;


function sp_to_cpuword(sp: sp_type) return cpuword_type is
  variable w: unsigned(wordSize-1 downto 0);
begin
  w := (others => '0');
  w(stackSizeBits-1+minAddrBit downto minAddrBit) := sp;
  w(maxAddrBit downto stackSizeBits+minAddrBit) := (others =>'1');
  return w;
end sp_to_cpuword;

function pc_to_cpuword(pc: pc_type) return cpuword_type is
  variable w: unsigned(wordSize-1 downto 0);
begin
  w := (others => '0');
  w(maxPCBit downto 0) := pc;
  return w;
end pc_to_cpuword;

function cpuword_to_sp(word: cpuword_type) return sp_type is
  variable s: sp_type;
begin
  s := word(stackSizeBits+1 downto minAddrBit);
  return s;
end cpuword_to_sp;

function cpuword_to_pc(word: cpuword_type) return pc_type is
  variable s: pc_type;
begin
  s := word(maxPCBit downto 0);
  return s;
end cpuword_to_pc;

  function str(b: InsnType; o: unsigned(4 downto 0) ) return string is
    variable insnname: string(1 to 9) := " UNKNOWN ";
  begin
    case b is
      when Insn_AddTop => insnname:=             "ADDTOP   ";
      when Insn_Dup => insnname :=               "DUP      ";
      when Insn_DupStackB => insnname :=         "DUPSTACKB";
      when Insn_Pop => insnname :=               "POP      ";
      when Insn_Popdown => insnname :=           "POPDOWN  ";
      when Insn_Add => insnname :=               "ADD      ";
      when Insn_Or => insnname :=                "OR       ";
      when Insn_And => insnname :=               "AND      ";
      when Insn_Store => insnname :=             "STORE    ";
      when Insn_AddSP => insnname :=             "ADDSP__" & hstr(std_logic_vector(o));
      when Insn_Shift => insnname :=             "SHIFT    ";
      when Insn_Nop => insnname :=               "NOP      ";
      when Insn_Im => insnname :=                "IM       ";
      when Insn_LoadSP => insnname :=            "LOADSP_" & hstr(std_logic_vector(o));
      when Insn_StoreSP => insnname :=           "STORESP" & hstr(std_logic_vector(o));
      when Insn_StoreSP2 => insnname :=          "STORESP02";
      when Insn_StoreSP3 => insnname :=          "STORESP03";
      when Insn_Emulate => insnname :=           "EMULATE  ";
      when Insn_Load => insnname :=              "LOAD     ";
      when Insn_PushPC => insnname :=            "PUSHPC   ";
      when Insn_PushSP => insnname :=            "PUSHSP   ";
      when Insn_PopPC => insnname :=             "POPPC    ";
      -- when Insn_PopPCRel => insnname:=           "POPPCREL ";
      when Insn_Not => insnname :=               "NOT      ";
      when Insn_Flip => insnname :=              "FLIP     ";
      when Insn_PopSP => insnname :=             "POPSP    ";
      when Insn_Neqbranch => insnname :=         "NEQBRANCH";
      when Insn_Eq => insnname :=                "EQ       ";
      when Insn_Loadb => insnname :=             "LOADB    ";
      when Insn_Mult => insnname :=              "MULT     ";
      when Insn_Lessthan => insnname :=          "LESSTHAN ";
      when Insn_Lessthanorequal => insnname :=   "LESSOREQ ";
      when Insn_Ulessthanorequal => insnname :=  "ULESSOREQ";
      when Insn_Ulessthan => insnname :=         "ULESS    ";
      -- when Insn_Pushspadd => insnname :=         "PUSHSPADD";
      -- when Insn_Call => insnname :=              "CALL     ";
      -- when Insn_Callpcrel => insnname :=         "CALLPCREL";
      when Insn_Sub => insnname :=               "SUB      ";
      when Insn_Break => insnname :=             "! BREAK !";
      when Insn_Storeb => insnname :=            "STOREB   ";
      when Insn_Lshiftright => insnname :=       "LSHTRIGHT";
      when others => insnname :=                  "UNKNOWN  ";
    end case;
    return insnname;
  end str;

function str(ti: TIME) return STRING is
variable result : STRING(14 downto 1) := "              "; -- longest string is "2147483647 min"
variable tmp    : NATURAL;
variable pos    : NATURAL := 1;
variable digit  : NATURAL;
variable resol  : TIME := 10 ns;--TIME'succ(ti) - ti; -- time resolution
variable scale  : NATURAL := 1;
variable unit   : TIME;

begin	
	
	if resol = 100 sec then scale := 100; unit := 1 sec;
	elsif  resol = 10 sec then scale := 10; unit := 1 sec;
	elsif  resol = 1 sec then scale := 1; unit := 1 sec;
	elsif resol = 100 ms then scale := 100; unit := 1 ms;
	elsif  resol = 10 ms then scale := 10; unit := 1 ms;
	elsif  resol = 1 ms then scale := 1; unit := 1 ms;
	elsif resol = 100 us then scale := 100; unit := 1 us;
	elsif  resol = 10 us then scale := 10; unit := 1 us;
	elsif  resol = 1 us then scale := 1; unit := 1 us;	
	elsif resol = 100 ns then scale := 100; unit := 1 ns;
	elsif  resol = 10 ns then scale := 10; unit := 1 ns;
	elsif  resol = 1 ns then scale := 1; unit := 1 ns;		 
	elsif resol = 100 ps then scale := 100; unit := 1 ps;
	elsif  resol = 10 ps then scale := 10; unit := 1 ps;
	elsif  resol = 1 ps then scale := 1; unit := 1 ps;	
	elsif resol = 100 fs then scale := 100; unit := 1 fs;
	elsif  resol = 10 fs then scale := 10; unit := 1 fs;
	elsif  resol = 1 fs then scale := 1; unit := 1 fs;
	else scale := 0; unit := 1 fs;
	end if;
				
	-- Write unit (reversed order)
	if unit = 1 hr then
		result(pos) := 'r';
		pos := pos + 1;
		result(pos) := 'h';
		pos := pos + 1;
		result(pos) := ' ';
		pos := pos + 1;	
	elsif unit = 1 sec then
		result(pos) := 'c';
		pos := pos + 1;
		result(pos) := 'e';
		pos := pos + 1;
		result(pos) := 's';
		pos := pos + 1;
	elsif unit = 1 ms then
		result(pos) := 's';
		pos := pos + 1;
		result(pos) := 'm';
		pos := pos + 1;
		result(pos) := ' ';
		pos := pos + 1;
	elsif unit = 1 us then
		result(pos) := 's';
		pos := pos + 1;
		result(pos) := 'u';
		pos := pos + 1;
		result(pos) := ' ';
		pos := pos + 1;
	elsif unit = 1 ns then
		result(pos) := 's';
		pos := pos + 1;
		result(pos) := 'n';
		pos := pos + 1;
		result(pos) := ' ';
		pos := pos + 1;
	elsif unit = 1 ps then
		result(pos) := 's';
		pos := pos + 1;
		result(pos) := 'p';
		pos := pos + 1;
		result(pos) := ' ';
		pos := pos + 1;
	elsif unit = 1 fs then
		result(pos) := 's';
		pos := pos + 1;
		result(pos) := 'f';
		pos := pos + 1;
		result(pos) := ' ';
		pos := pos + 1;	
	else
		result(pos) := '?';
		pos := pos + 1;
		result(pos) := '?';
		pos := pos + 1;
		result(pos) := ' ';
		pos := pos + 1;	
	end if;

	-- Convert TIME to NATURAL
	tmp := scale * (ti / resol);
	
	loop
	   	digit := tmp MOD 10; -- extract last digit
	    	tmp := tmp / 10;
	    	result(pos) := character'val(character'pos('0') + digit);
	    	pos := pos + 1;
	    	exit when tmp = 0;
	end loop;
	
	-- Return result (put back in right order)
	return result((pos-1) downto 1);
end str;



end alzpupkg;
