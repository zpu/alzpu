----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Carl Haken�s
-- 
-- Create Date:    18:48:15 02/11/2009 
-- Design Name:  
-- Module Name:    SerialShifter - Behavioral 
-- Project Name:   ZPU
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity FastShifter is
   Generic(DATA_WIDTH  : integer:=1);
   
    Port ( clk          : in  STD_LOGIC;    
          reset         : in  STD_LOGIC;    
          aritm         : in  STD_LOGIC;
          data_in       : in  unsigned(31 downto 0);        
          shift_amount  : in  unsigned(4 downto 0);
          valid_in      : in  STD_LOGIC;
          busy          : out  STD_LOGIC;
          data_out      : out  unsigned(31 downto 0));

end FastShifter;

architecture Behavioral of FastShifter is

signal busy_i: std_logic;

type reg_type is record
  lsb         : std_logic;
  working     : std_logic;
  shift_amount : unsigned(4 downto 0);  
  shift_data  : unsigned(31 downto 0);
end record;

signal r,rin : reg_type;

begin

  busy <= '0';--'1' when valid_in='1' or r.working='1' else '0';

  process(r,data_in,shift_amount,valid_in,reset,aritm)
  variable v:reg_type;
  variable done:std_logic;
  begin
    v:=r;
    
    if r.shift_amount = "0000" then
      done:='1';
    else
      done:='0';            
     end if;
    
    if valid_in = '1' and done='1' then
      v.shift_amount:=shift_amount;
      v.shift_data:=data_in;
      v.lsb:=data_in(0);
      done:='1';            
    else        
                
      if done = '0' then
        v.shift_data(31):=r.shift_data(31) and aritm;  
        v.shift_data(30):=r.shift_data(31);
        v.shift_data(29):=r.shift_data(30);
        v.shift_data(28):=r.shift_data(29);
        v.shift_data(27):=r.shift_data(28);
        v.shift_data(26):=r.shift_data(27);
        v.shift_data(25):=r.shift_data(26);
        v.shift_data(24):=r.shift_data(25);
        
        for i in 0 to 30-7 loop      
            if r.shift_amount >= 8  then 
              v.shift_data(30):=r.shift_data(31) and aritm;
              v.shift_data(29):=r.shift_data(31) and aritm;
              v.shift_data(28):=r.shift_data(31) and aritm;
              v.shift_data(27):=r.shift_data(31) and aritm;
              v.shift_data(26):=r.shift_data(31) and aritm;
              v.shift_data(25):=r.shift_data(31) and aritm;
              v.shift_data(24):=r.shift_data(31) and aritm;              
              v.shift_data(i):=r.shift_data(i+8);                    
              v.shift_amount:=r.shift_amount-8;                        
            else                        
              v.shift_data(i):=r.shift_data(i+1);    
              v.shift_amount:=r.shift_amount-1;                
            end if;
        end loop;
      end if;          
    end if;
    
    if reset = '1' then
      v.shift_amount:=(others =>'0');            
    end if;
    
    busy_i<=not done;
    data_out<=r.shift_data;
    
    rin<=v;
  end process;

  process(clk)
  begin
    if rising_edge(clk) then  
      r<=rin;
    end if;
  end process;

end Behavioral;

