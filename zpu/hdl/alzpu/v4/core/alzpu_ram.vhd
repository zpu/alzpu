--
--  ALZPU 
-- 
--  Copyright 2008 Alvaro Lopes <alvieboy@alvie.com>
-- 
--  Version: 1.0
-- 
--  The FreeBSD license
--  
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following conditions
--  are met:
--  
--  1. Redistributions of source code must retain the above copyright
--     notice, this list of conditions and the following disclaimer.
--  2. Redistributions in binary form must reproduce the above
--     copyright notice, this list of conditions and the following
--     disclaimer in the documentation and/or other materials
--     provided with the distribution.
--  
--  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY
--  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
--  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
--  PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
--  ZPU PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
--  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
--  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
--  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
--  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
--  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
--  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
--  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--  
-- */
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.tech.all;
use work.txt_util.all;
use work.alzpupkg.all;
use work.alzpu_config.all;

entity alzpu_ram is
  port (
    clka:       in std_logic;
    clkb:       in std_logic;
    rst:        in std_logic;
    addra:      in std_logic_vector(alzpu_ram_num_bits-1 downto 2);
    addrb:      in std_logic_vector(alzpu_ram_num_bits-1 downto 2);
    doa:        out cpuword_type;
    dob:        out cpuword_type;
    dia:        in cpuword_type;
    dib:        in cpuword_type;
    ena:        in std_logic_vector(3 downto 0);
    enb:        in std_logic_vector(3 downto 0);
    wra:        in std_logic;
    wrb:        in std_logic
  );
end entity alzpu_ram;

architecture behave of alzpu_ram is

signal doa_i: std_logic_vector(wordSize-1 downto 0);
signal dob_i: std_logic_vector(wordSize-1 downto 0);

begin

  u1:  dp_ram
    generic map(
      dbits   => wordSize,
      abits   => alzpu_ram_num_bits
	  )
    port map(
      a_di     => std_logic_vector(dia),
      a_a      => addra,
      a_we     => wra,
      a_do     => doa_i,
      a_clk    => clka,
      a_en     => ena,

      b_di     => std_logic_vector(dib),
      b_a      => addrb,
      b_we     => wrb,
      b_en     => enb,
      b_do     => dob_i,
      b_clk    => clkb
    );

    doa <= unsigned(doa_i);
    dob <= unsigned(dob_i);

end behave;
