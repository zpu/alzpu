-- ZPU/ALZPU
--
-- Copyright 2008 alvieboy - �lvaro Lopes - alvieboy@alvie.com
-- 
-- The FreeBSD license
-- 
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above
--    copyright notice, this list of conditions and the following
--    disclaimer in the documentation and/or other materials
--    provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE ZPU PROJECT ``AS IS'' AND ANY
-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
-- PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
-- ZPU PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
-- INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
-- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
-- STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
-- ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
-- The views and conclusions contained in the software and documentation
-- are those of the authors and should not be interpreted as representing
-- official policies, either expressed or implied, of the ZPU Project.
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;

library work;
use work.alzpu_config.all;
use work.alzpupkg.all;
use work.txt_util.all;


entity alzpu_tracer is
  generic (
    log_file: string  := "alzpu-trace.txt"
  );
  port (
    clk:    in std_logic;
    rst:    in std_logic;
    insn:   in InsnType;
    opcode: in std_logic_vector(7 downto 0);
    stackA: in cpuword_type;
    stackB: in cpuword_type;
    memR:   in cpuword_type;
    memR_en:in std_logic;
    hazard: in std_logic;
    branch: in std_logic;
    sync:   in std_logic;
    pc:     in pc_type;
    sp:     in sp_type;
    exec:   in std_logic
  );
end alzpu_tracer;


architecture behave of alzpu_tracer is


  file 		lf:   TEXT open write_mode is log_file;
  signal  counter: unsigned(31 downto 0);
  signal read_dly: std_logic;
begin
  print(lf,"---CLOCK--      OPCD Insn         ProgramCnt    StackPtr        StackA     StackB  ");

process(clk,rst)
  variable psp: cpuword_type;
  variable psprel: cpuword_type;
  variable relsp: unsigned(maxAddrBitIncIO downto minAddrBit);
  variable tSpOffset : unsigned(4 downto 0);
  variable sprelstr: string(1 to 10) := "----------";
  variable memrstr: string(1 to 10) := "----------";
  variable busystr: string(1 to 10) := "----------";
begin
	psp:= (others => '0');
	psprel:= (others => '0');
  tSpOffset := (others =>'0');
  if clk'event and clk='1' then
    if rst='1' then
      counter <= (others => '0');
      read_dly <= '0';
    else
      if counter=0 then
        report "TRACER starting" severity note;
      end if;

      read_dly <= memR_en;
      counter <= counter + 1;
        tSpOffset(4):=not opcode(4);
        tSpOffset(3 downto 0):=unsigned(opcode(3 downto 0));

        psp := sp_to_cpuword(sp);

        psprel := sp_to_cpuword(sp + tSpOffset);

        if ( insn=Insn_LoadSP or insn=Insn_StoreSP or insn=Insn_StoreSP2 or insn=Insn_StoreSP3 or insn=Insn_AddSP) then
          sprelstr := "0x" & hstr(psprel);
        else
          sprelstr := "----------";
        end if;

        if read_dly='1' then
          memrstr := "0x" & hstr(std_logic_vector(memR));
        else
          memrstr := "----------";
        end if;
        busystr := "----------";

        if branch='1' then
          busystr := " <BRANCH> ";
        else
          if hazard='1' then
            busystr := " <HAZARD> ";
          elsif sync='1' then
            busystr := " <-SYNC-> ";
          end if;
        end if;

      if exec='1' then
        print(lf, "0x" & hstr(counter) & " INSN 0x" &hstr(opcode)& " " & str(insn,tSpOffset) & " PC 0x" & hstr(pc_to_cpuword(pc)) & " SP " & hstr(psp)
              & " DATA 0x" & hstr(std_logic_vector(stackA)) & " 0x" & hstr(std_logic_vector(stackB))  & " SPRel " & sprelstr
              & " memR " & memrstr & " Time:" & str(now) );
      else
        print(lf, "0x" & hstr(counter) & " BUSY     "& busystr &" PC 0x" & hstr(pc_to_cpuword(pc)) & " SP " & hstr(psp)
              & " DATA 0x" & hstr(std_logic_vector(stackA)) & " 0x" & hstr(std_logic_vector(stackB))  & " SPRel " & sprelstr
              & " memR " & memrstr & " Time:" & str(now) );

      end if;
    end if;
  end if;

end process;

end behave;
