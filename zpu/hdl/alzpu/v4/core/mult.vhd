-- ALZPU Multiplier Unit
--
-- Copyright 2009 alvieboy - �lvaro Lopes - alvieboy@alvie.com
-- 
-- The FreeBSD license
-- 
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above
--    copyright notice, this list of conditions and the following
--    disclaimer in the documentation and/or other materials
--    provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE ZPU PROJECT ``AS IS'' AND ANY
-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
-- PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
-- ZPU PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
-- INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
-- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
-- STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
-- ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
-- The views and conclusions contained in the software and documentation
-- are those of the authors and should not be interpreted as representing
-- official policies, either expressed or implied, of the ZPU Project.


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.alzpupkg.all;
use work.alzpu_config.all;
use work.txt_util.all;

entity mult is
  port (
    clk:  in std_logic;
    rst:  in std_logic;
    a:    in cpuword_type;
    b:    in cpuword_type;
    o:    out cpuword_type;
    en:   in std_logic;
    busy: out std_logic
  );
end entity mult;

architecture behave of mult is

type mulstate is (
  IDLE,
  MULT1,
  MULT2,
  MULT3
);

signal state: mulstate;
signal multResult1: unsigned(wordSize*2-1 downto 0);
signal multResult2: unsigned(wordSize*2-1 downto 0);
signal multResult3: unsigned(wordSize*2-1 downto 0);

begin

  busy <= '1' when en='1' and state /= MULT3 else '0';
  o <= multResult3(wordSize-1 downto 0);

  process(clk)
    variable tMultResult : unsigned(wordSize*2-1 downto 0);
  begin
    if rising_edge(clk) then
      tMultResult := a * b;
      multResult3 <= multResult2;
      multResult2 <= multResult1;
      multResult1 <= tMultResult;
    end if;
  end process;


  process(clk)
  begin
    if rising_edge(clk) then
      if rst='1' then
        state <= IDLE;
      else
        case state is
          when IDLE =>
            if en='1' then
              state <= MULT1;
            end if;
          when MULT1 => state <= MULT2;
          when MULT2 => state <= MULT3;
          when MULT3 => state <= IDLE;
          when others =>
            state <= IDLE;
        end case;
      end if;
    end if;
  end process;

end behave;
