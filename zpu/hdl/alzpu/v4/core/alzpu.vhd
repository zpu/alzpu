-- ZPU/ALZPU
--
-- Copyright 2004-2008 oharboe - �yvind Harboe - oyvind.harboe@zylin.com
-- Copyright 2008 alvieboy - �lvaro Lopes - alvieboy@alvie.com
-- 
-- The FreeBSD license
-- 
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above
--    copyright notice, this list of conditions and the following
--    disclaimer in the documentation and/or other materials
--    provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE ZPU PROJECT ``AS IS'' AND ANY
-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
-- PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
-- ZPU PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
-- INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
-- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
-- STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
-- ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
-- The views and conclusions contained in the software and documentation
-- are those of the authors and should not be interpreted as representing
-- official policies, either expressed or implied, of the ZPU Project.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


library work;
use work.alzpu_config.all;
use work.alzpupkg.all;
use work.txt_util.all;

entity alzpu is
  port (
    clk:              in std_logic;
    rst:              in std_logic;
    interrupt:        in std_logic;
    break:            out std_logic;
    sleep:            in std_logic;

    -- IO interface

    io_o_addr:        out std_logic_vector(maxAddrBit downto minAddrBit);
    io_o_wr_data:     out cpuword_type;
    io_o_clk:         out std_logic;
    io_o_rst:         out std_logic;
    io_i_busy:        in std_logic;
    io_i_rd_data:     in cpuword_type;
    io_o_en:          out std_logic;
    io_o_wr_en:       out std_logic;

    -- ROM

    rom_o_addra:      out std_logic_vector(alzpu_rom_num_bits-1 downto 0);
    rom_o_addrb:      out std_logic_vector(alzpu_rom_num_bits-1 downto 2);
    rom_i_doa:        in std_logic_vector(7 downto 0);
    rom_i_dob:        in cpuword_type;
    rom_o_ena:        out std_logic;
    rom_o_enb:        out std_logic;

    -- RAM

    ram_o_addra:      out std_logic_vector(alzpu_ram_num_bits-1 downto 2);
    ram_o_addrb:      out std_logic_vector(alzpu_ram_num_bits-1 downto 2);
    ram_i_doa:        in cpuword_type;
    ram_o_dib:        out cpuword_type;
    ram_o_ena:        out std_logic_vector(3 downto 0);
    ram_o_enb:        out std_logic_vector(3 downto 0)
  );
end entity alzpu;

architecture behave of alzpu is

component alzpu_validator is
  port (
    clk:    in std_logic;
    rst:    in std_logic;
    valid:  in std_logic;
    op:     in std_logic_vector(7 downto 0);
    stackA: in cpuword_type;
    stackB: in cpuword_type;
    pc:     in pc_type;
    sp:     in sp_type
  );
end component;

component alzpu_tracer is
  port (
    clk:    in std_logic;
    rst:    in std_logic;
    insn:   in InsnType;
    stackA: in cpuword_type;
    stackB: in cpuword_type;
    memR:   in cpuword_type;
    memR_en:in std_logic;
    pc:     in pc_type;
    sp:     in sp_type;
    hazard: in std_logic;
    sync:   in std_logic;
    branch: in std_logic;
    opcode: in std_logic_vector(7 downto 0);
    exec:   in std_logic
  );
end component;

component mult is
  port (
    clk:  in std_logic;
    rst:  in std_logic;
    a:    in cpuword_type;
    b:    in cpuword_type;
    o:    out cpuword_type;
    en:   in std_logic;
    busy: out std_logic
  );
end component mult;

component fastshifter is
  generic(
    data_width  : integer:=1
  );
  port (
    clk:          in std_logic;
    reset:        in std_logic;
    aritm:        in std_logic;
    data_in:      in cpuword_type;
    shift_amount: in unsigned(4 downto 0);
    valid_in:     in std_logic;
    busy:         out std_logic;
    data_out:     out cpuword_type
  );
end component fastshifter;

---------------------------------------------------
--- Instruction fetcher signals
---------------------------------------------------

signal ifu_o_mem_address:     std_logic_vector(alzpu_rom_num_bits-1 downto 0); -- Memory address to ROM
signal ifu_o_mem_read_enable: std_logic;                                       -- Read ROM enable
signal ifu_i_mem_data:        std_logic_vector(7 downto 0);   -- output from ROM (opcode)
signal ifu_i_freeze:          std_logic;                      -- Freeze IFU
signal ifu_i_do_branch:       std_logic;                      -- Perform a branch
signal ifu_i_branch_address:  pc_type;                        -- Branch address
signal ifu_o_flush:           std_logic;                      -- Flush all other stages
signal ifu_o_opcode:          std_logic_vector(7 downto 0);   -- Opcode output from ROM
signal ifu_o_opcode_valid:    std_logic;                      -- Opcode valid - used for tracer
signal ifu_o_pc:              pc_type;                        -- PC in sync with opcode
signal ifu_o_pc_inc:          pc_type;                        -- PC+1 in sync with opcode
signal ifu_data:              std_logic_vector(7 downto 0);
signal ifu_pc_int:            pc_type;                        -- PC itself
signal ifu_pc_fetch:          pc_type;                        -- Fetch PC.
signal ifu_pc_next:           pc_type;                        -- Next PC (pc+1)
signal ifu_opcode_valid_int:  std_logic;
signal ifu_freeze_int:        std_logic;
-- Fast EMUL
signal ifu_is_emulate:        std_logic;                      -- "decoded" emulate opcode.
signal ifu_emulate_address:   pc_type;                        -- Address where to branch for emulate

---------------------------------------------------
--- Decoder signals
---------------------------------------------------

signal dec_i_freeze:          std_logic;
signal dec_o_flush_ex:        std_logic;
signal dec_o_busy:            std_logic;
signal dec_o_insn:            InsnType;
signal dec_o_opcode:          std_logic_vector(7 downto 0);
signal dec_o_spOffset:        unsigned(4 downto 0);
signal dec_o_pc:              pc_type;
signal dec_o_pc_inc:          pc_type;
signal dec_o_idim_flag:       std_logic;
signal dec_o_sp:              sp_type;
signal dec_o_memR:            cpuword_type;
signal dec_o_mem_wren:        std_logic;
signal dec_o_spAddOffset:     sp_type;
signal dec_spOffset:          unsigned(4 downto 0);
signal dec_dSpAddOffset:      sp_type;
signal dec_opcode_valid_int:  std_logic;
signal dec_mem_read_enable:   std_logic;
signal dec_mem_address:       memaddr_type;
signal dec_insn_writes_memory:std_logic;
signal dec_insn_reads_memory: std_logic;
signal dec_idim_flag_int:     std_logic;
signal dec_idim_flag_q:       std_logic;
signal dec_idim_flag_dly_q:   std_logic;
signal dec_dSp:               sp_type;
signal dec_dIncSp:            sp_type;
signal dec_dDecSp:            sp_type;
signal dec_dIncIncSp:         sp_type;
signal dec_next_sp:           sp_type;
signal dec_do_freeze:         std_logic;
signal dec_hazard:            std_logic;
signal dec_hazard_q:          std_logic;
signal dec_flush_q:           std_logic;

attribute INIT: std_logic;
attribute INIT of dec_flush_q: signal is '1';

signal dec_decoded_opcode:    InsnType;
signal dec_need_stacka:             std_logic;
signal dec_nextsp_need_stacka:      std_logic;
signal dec_memaddr_need_stacka:     std_logic;
signal dec_stallfreeze_for_stacka:  std_logic;
signal dec_do_change_sp:            std_logic;
signal dec_insn_is_loadsp:          std_logic;
signal dec_insn_is_dpop_hazard:     std_logic;
signal dec_insn_was_dpop_hazard_q:  std_logic;
signal dec_insn_is_dualpop:         std_logic;
signal dec_insn_is_syncb_hazard:    std_logic;
signal dec_insn_was_syncb_hazard_q: std_logic;
signal dec_insn_is_storesp:         std_logic;
signal dec_insn_is_resync_b:        std_logic;
signal dec_insn_was_storesp_q:      std_logic;
signal dec_mem_target_q:            memory_target_type := TARGET_RAM;
signal dec_mem_target:              memory_target_type;
signal dec_insn_need_syncb:         std_logic;
signal dec_insn_need_syncb_q:       std_logic;
signal dec_insn_need_syncb_dly_q:   std_logic;
signal dec_hazard_comp:             std_logic;
signal dec_o_interrupt:             std_logic;

---------------------------------------------------
--- Execution unit signals
---------------------------------------------------

signal ex_o_stall:            std_logic;
signal ex_i_freeze:           std_logic;
signal ex_o_break:            std_logic;
signal ex_o_stackTopReady:    std_logic;
signal ex_o_executing:        std_logic;
signal ex_i_interrupt:        std_logic;
signal ex_stackA:             cpuword_type;
signal ex_stackB:             cpuword_type;
signal ex_set_stackA:         cpuword_type;
signal ex_set_stackB:         cpuword_type;
signal ex_stackA_ready:       std_logic;
signal ex_stackA_changes:     std_logic;
signal ex_stackB_changes:     std_logic;
signal ex_mem_write_int:      cpuword_type;
signal ex_stall_int:          std_logic;
signal ex_inInterrupt:        std_logic;
signal ex_in_mem_busy:        std_logic;
signal ex_mem_write_address:  memaddr_type;
signal ex_do_write:           std_logic;
signal ex_do_mult:            std_logic;
signal ex_do_shift:           std_logic;
signal ex_multresult:         cpuword_type;
signal ex_shiftresult:        cpuword_type;
signal ex_mult_busy:          std_logic;
signal ex_shift_busy:         std_logic;
signal ex_shift_aritm:        std_logic;
signal ex_stall_int_q:        std_logic;
signal trace_valid_insn:      std_logic;
signal ex_insn_branch:        std_logic;
-------------------------
--- IO signals ----------
-------------------------

signal grant_a:           boolean;
signal grant_b:           boolean;
signal io_A_busy:         std_logic;
signal io_B_busy:         std_logic;
signal io_A_rden:         std_logic;
signal io_B_wren:         std_logic;

--------------------------
--- RAM signals ----------
--------------------------

signal do_interrupt:      std_logic;
signal in_interrupt_q:    std_logic;
signal reset_interrupt:   std_logic;

begin

rom_o_addra <= ifu_o_mem_address;
rom_o_addrb <= std_logic_vector(ex_stackA(alzpu_rom_num_bits-1 downto 2)); -- Only accessed through LOAD
ifu_i_mem_data <= rom_i_doa;
rom_o_ena <= ifu_o_mem_read_enable;

ifu_i_freeze <= ex_o_stall or dec_o_busy or ex_o_break or sleep;
ex_i_freeze <= dec_o_flush_ex or ex_o_break or sleep;
dec_i_freeze <= ex_o_stall or ex_o_break or sleep;

---------------------------------------------------
--- Instruction fetcher unit
---------------------------------------------------

  memaddressgen: process(ifu_pc_fetch)
  begin
    ifu_o_mem_address <= (others =>'0');
    ifu_o_mem_address(maxPCbit downto 0) <= std_logic_vector(ifu_pc_fetch);
  end process;

  ifu_o_mem_read_enable <= not ifu_i_freeze;
  ifu_data <= ifu_i_mem_data(7 downto 0);
  ifu_pc_next <= ifu_pc_int+1;
  ifu_o_opcode <= ifu_data;

  genpcfetch: process(ifu_is_emulate, ifu_pc_next, ifu_i_branch_address, ifu_emulate_address, ifu_i_do_branch)
  begin
    if ifu_i_do_branch='0' then
      if ifu_is_emulate='1' then
        ifu_pc_fetch <= ifu_emulate_address;
      else
        ifu_pc_fetch <= ifu_pc_next;
      end if;
    else
      ifu_pc_fetch <= ifu_i_branch_address;
    end if;
  end process;
  
  ifu_o_flush <= ifu_i_do_branch;
  ifu_o_opcode_valid <= ifu_opcode_valid_int when ifu_i_do_branch='0' else '0';
  ifu_freeze_int <= ifu_i_freeze and ifu_opcode_valid_int; -- Requested to freeze and insn in decoder is valid
  ifu_o_pc <= ifu_pc_int;
  ifu_o_pc_inc <= ifu_pc_next;

  pcintgen: process( clk, rst )
  begin
    if rising_edge(clk) then
      if rst='1' then
        ifu_pc_int <= (others=>'1');
        ifu_opcode_valid_int <= '0';
      else
        ifu_opcode_valid_int <= '1';
        if ifu_freeze_int='0' then
          ifu_pc_int <= ifu_pc_fetch;
        end if;
      end if;
    end if;
  end process;

  isemul: process(ifu_data,dec_decoded_opcode)
    variable opcode: std_logic_vector(5 downto 0);
  begin
    ifu_emulate_address <= (others=>'0');
    ifu_emulate_address(9 downto 5) <= unsigned(ifu_data(4 downto 0));

    if dec_decoded_opcode = Insn_Emulate then
      ifu_is_emulate <= '1';
    else
      ifu_is_emulate <= '0';
    end if;
  end process;

---------------------------------------------------
------------------- Decode unit -------------------
---------------------------------------------------

  insndec: process (ifu_o_opcode)
  begin
  if ifu_o_opcode(7 downto 7) = "1" then 
    dec_decoded_opcode <= Insn_im;
  elsif ifu_o_opcode(6 downto 5) = "10" then 
    if ifu_o_opcode(4 downto 0) = "10000" then 
      dec_decoded_opcode <= Insn_pop;
    elsif ifu_o_opcode(4 downto 0) = "10001" then 
      dec_decoded_opcode <= Insn_popdown;
    elsif ifu_o_opcode(4 downto 0) = "10010" then
      dec_decoded_opcode <= Insn_storesp2;
    elsif ifu_o_opcode(4 downto 0) = "10011" then
      dec_decoded_opcode <= Insn_storesp3;
    else
      dec_decoded_opcode <= Insn_storesp;
    end if;
  elsif ifu_o_opcode(6 downto 5) = "11" then 
    if ifu_o_opcode(4 downto 0) = "10000" then 
      dec_decoded_opcode <= Insn_dup;
    elsif ifu_o_opcode(4 downto 0) = "10001" then 
      dec_decoded_opcode <= Insn_dupstackb;
    else
      dec_decoded_opcode <= Insn_loadsp;
    end if;
  elsif ifu_o_opcode(6 downto 4) = "001" then 
    if ifu_o_opcode(3 downto 0) = "0000" then 
      dec_decoded_opcode <= Insn_shift;
    elsif ifu_o_opcode(3 downto 0) = "0001" then 
      dec_decoded_opcode <= Insn_addtop;
    else
      dec_decoded_opcode <= Insn_addsp;
    end if;
  elsif ifu_o_opcode(6 downto 5) = "01" then
    if ifu_o_opcode(5 downto 0) = OpCode_Neqbranch and implement_neqbranch then
      dec_decoded_opcode <= Insn_Neqbranch;
    elsif ifu_o_opcode(5 downto 0) = OpCode_Loadb and implement_loadb then
      dec_decoded_opcode <= Insn_Loadb;
    elsif ifu_o_opcode(5 downto 0) = OpCode_Eq and implement_eq then
      dec_decoded_opcode <= Insn_Eq;
    elsif ifu_o_opcode(5 downto 0) = OpCode_Lessthan and implement_lessthan then
      dec_decoded_opcode <= Insn_Lessthan;
    elsif ifu_o_opcode(5 downto 0) = OpCode_Lessthanorequal and implement_lessthanorequal then
      dec_decoded_opcode <= Insn_Lessthanorequal;
    elsif ifu_o_opcode(5 downto 0) = OpCode_Ulessthanorequal and implement_ulessthanorequal then
      dec_decoded_opcode <= Insn_Ulessthanorequal;
    elsif ifu_o_opcode(5 downto 0) = OpCode_Ulessthan and implement_ulessthan then
      dec_decoded_opcode <= Insn_Ulessthan;
    elsif ifu_o_opcode(5 downto 0) = OpCode_Storeb and implement_storeb then
      dec_decoded_opcode <= Insn_StoreB;
    elsif ifu_o_opcode(5 downto 0) = OpCode_Sub and implement_sub then
      dec_decoded_opcode <= Insn_Sub;
    elsif ifu_o_opcode(5 downto 0) = OpCode_Mult and implement_mult then
      dec_decoded_opcode <= Insn_Mult;
    elsif ifu_o_opcode(5 downto 0) = OpCode_Lshiftright and implement_lshiftright then
      dec_decoded_opcode <= Insn_Lshiftright;
    else
      dec_decoded_opcode <= Insn_emulate;
    end if;
  elsif ifu_o_opcode(3 downto 0) = "0000" then
    dec_decoded_opcode <= Insn_break;
  elsif ifu_o_opcode(3 downto 0) = "0010" then
    dec_decoded_opcode <= Insn_pushsp;
  elsif ifu_o_opcode(3 downto 0) = "0100" then
    dec_decoded_opcode <= Insn_poppc;
  elsif ifu_o_opcode(3 downto 0) = "0101" then
    dec_decoded_opcode <= Insn_add;
  elsif ifu_o_opcode(3 downto 0) = "0110" then
    dec_decoded_opcode <= Insn_and;
  elsif ifu_o_opcode(3 downto 0) = "0111" then
    dec_decoded_opcode <= Insn_or;
  elsif ifu_o_opcode(3 downto 0) = "1000" then
    dec_decoded_opcode <= Insn_load;
  elsif ifu_o_opcode(3 downto 0) = "1001" then
    dec_decoded_opcode <= Insn_not;
  elsif ifu_o_opcode(3 downto 0) = "1010" then
    dec_decoded_opcode <= Insn_flip;
  elsif ifu_o_opcode(3 downto 0) = "1011" then
    dec_decoded_opcode <= Insn_nop;
  elsif ifu_o_opcode(3 downto 0) = "1100" then
    dec_decoded_opcode <= Insn_store;
  elsif ifu_o_opcode(3 downto 0) = "1101" then
    dec_decoded_opcode <= Insn_popsp;
  else
    dec_decoded_opcode <= Insn_break;
  end if;
  end process;

  process (dec_mem_target_q,rom_i_dob,ram_i_doa,io_i_rd_data)
  begin
    case dec_mem_target_q is
      when TARGET_ROM =>
        dec_o_memR <= unsigned(rom_i_dob);
      when TARGET_RAM =>
        dec_o_memR <= unsigned(ram_i_doa);
      when TARGET_IO =>
        dec_o_memR <= unsigned(io_i_rd_data);
      when others =>
        dec_o_memR <= (others => DontCareValue);
    end case;
  end process;

  -- Address selection
  process (dec_mem_address,dec_mem_read_enable, dec_stallfreeze_for_stacka)
    variable i: memory_target_type;
  begin
    rom_o_enb <= DontCareValue;
    ram_o_ena <= (others=>DontCareValue);
    io_A_rden <= DontCareValue;

    i:=memaddr_target(dec_mem_address);

    case (i) is
      when TARGET_ROM =>
        rom_o_enb <= dec_mem_read_enable;
        ram_o_ena <= (others=>'0');
        io_A_rden <= '0';
      when TARGET_RAM =>
        ram_o_ena <= (others=>dec_mem_read_enable);
        rom_o_enb <= '0';
        io_A_rden <= '0';
      when TARGET_IO =>
        io_A_rden <= dec_mem_read_enable and not dec_stallfreeze_for_stacka;
        rom_o_enb <= '0';
        ram_o_ena <= (others=>'0');
      when others =>
    end case;

    dec_mem_target <= i;
  end process;


  process(clk)
  begin
    if rising_edge(clk) then
      if rst='1' then
        dec_mem_target_q <= TARGET_RAM;
      else
        dec_mem_target_q <= dec_mem_target;
      end if;
    end if;
  end process;

  dec_dIncSp <= dec_dSp + 1;
  dec_dIncIncSp <= dec_dSp + 2;
  dec_dDecSp <= dec_dSp - 1;


  genflushq:process(clk)
    begin
      if rising_edge(clk) then
        if rst='1' then
        dec_flush_q <= '1';
      else
        dec_flush_q <= ifu_o_flush;
      end if;
    end if;
  end process;


  nextspgen: process ( dec_decoded_opcode, ex_stackA, dec_dSp,
            dec_idim_flag_q, dec_dIncIncSp, dec_dIncSp, dec_dDecSp,
            do_interrupt)
  begin
    dec_idim_flag_int <= '0';
    dec_nextsp_need_stacka <= '0';

    dec_next_sp <= (others => DontCareValue);
    dec_do_change_sp <= '1';

    if do_interrupt='1' then
      dec_next_sp <= dec_dDecSp;
    else
    case dec_decoded_opcode is

      when Insn_Im =>
        dec_idim_flag_int <= '1';
        dec_do_change_sp <= not dec_idim_flag_q;
        dec_next_sp <= dec_dDecSp;

      when Insn_LoadSp
          |Insn_Dup
          |Insn_DupStackB
          |Insn_PushSP
          |Insn_Emulate =>
        dec_next_sp <= dec_dDecSp;

      when Insn_Store
          |Insn_Neqbranch
          |Insn_StoreB =>
        dec_next_sp <= dec_dIncIncSp;

      when Insn_PopDown
          |Insn_PopPC
          |Insn_StoreSP
          |Insn_StoreSP2
          |Insn_StoreSP3
          |Insn_Add
          |Insn_And
          |Insn_Mult
          |Insn_Or
          |Insn_Pop
          |Insn_Eq
          |Insn_Sub
          |Insn_Lshiftright
          |Insn_Ulessthan
          |Insn_Ulessthanorequal
          |Insn_Lessthan
          |Insn_Lessthanorequal =>
        dec_next_sp <= dec_dIncSp;

      when Insn_PopSP =>
        dec_nextsp_need_stacka <= '1';
        dec_next_sp <= cpuword_to_sp(ex_stackA);

      when Insn_break =>
        dec_nextsp_need_stacka <= DontCareValue;
        dec_idim_flag_int <= DontCareValue;
        dec_do_change_sp <= DontCareValue;

      when others =>
        dec_do_change_sp <= '0';
    end case;
    end if;
  end process;

  dec_need_stacka <= dec_memaddr_need_stacka or dec_nextsp_need_stacka; 

  dec_stallfreeze_for_stacka <= dec_need_stacka and not ex_o_StackTopReady;

  dec_dSpAddOffset <= dec_dSp + dec_SpOffset;

  -- Read enable signal

  readengen: process (dec_decoded_opcode, dec_insn_need_syncb_q)
  begin
    dec_insn_reads_memory<='1';
    if dec_insn_need_syncb_q='0' then
      case dec_decoded_opcode  is
        when Insn_LoadSP
            |Insn_AddSP
            |Insn_Store
            |Insn_PopDown
            |Insn_PopPC
            |Insn_StoreSP
            |Insn_StoreSP2
            |Insn_StoreSP3
            |Insn_Add
            |Insn_And
            |Insn_Or
            |Insn_Pop
            |Insn_Neqbranch
            |Insn_Eq
            |Insn_Sub
            |Insn_Mult
            |Insn_Lshiftright
            |Insn_Lessthan
            |Insn_Lessthanorequal
            |Insn_Ulessthan
            |Insn_Ulessthanorequal
            |Insn_StoreB
            |Insn_PopSP
            |Insn_Load
            |Insn_LoadB =>
        when others =>
          dec_insn_reads_memory <= '0';
      end case;
    end if;
  end process;

  readaddrgen: process ( ifu_o_opcode, ex_stackA, dec_dSp,
            dec_idim_flag_q, dec_dSp, dec_dIncIncSp, dec_dIncSp,
            dec_dDecSp, dec_decoded_opcode,dec_insn_need_syncb_q,
            dec_dSpAddOffset)
    variable tSpOffset : unsigned(4 downto 0);
    variable stackA_vec: std_logic_vector(wordSize-1 downto 0);
    variable next_mem_address: memaddr_type;
  begin

    tSpOffset(4):=not ifu_o_opcode(4);
    tSpOffset(3 downto 0):=unsigned(ifu_o_opcode(3 downto 0));

    dec_memaddr_need_stacka <= '0';
    dec_insn_is_resync_b <= '0';

    if dec_insn_need_syncb_q = '1' then
      next_mem_address := sp_to_memaddr(dec_dIncSp);
      --dec_insn_reads_memory <= '1';
    else
      case dec_decoded_opcode  is
        when Insn_LoadSP
            |Insn_AddSP =>
          next_mem_address := sp_to_memaddr(dec_dSpAddOffset);
          --dec_insn_reads_memory <= '1';

        when Insn_Store
            |Insn_PopDown
            |Insn_PopPC
            |Insn_StoreSP
            |Insn_StoreSP2
            |Insn_StoreSP3
            |Insn_Add
            |Insn_And
            |Insn_Or
            |Insn_Pop
            |Insn_Neqbranch
            |Insn_Eq
            |Insn_Sub
            |Insn_Mult
            |Insn_Lshiftright
            |Insn_Lessthan
            |Insn_Lessthanorequal
            |Insn_Ulessthan
            |Insn_Ulessthanorequal
            |Insn_StoreB =>
          next_mem_address := sp_to_memaddr(dec_dIncIncSp);
          --dec_insn_reads_memory <= '1';
          dec_insn_is_resync_b <= '1';

        when Insn_PopSP
            |Insn_Load =>
          dec_memaddr_need_stacka <= '1';
          next_mem_address := ex_stackA(maxAddrBitIncIO downto minAddrBit);
          --dec_insn_reads_memory <= '1';

        when Insn_LoadB =>
          dec_memaddr_need_stacka <= '1';
          next_mem_address := ex_stackA(maxAddrBitIncIO downto minAddrBit);
          --dec_insn_reads_memory <= '1';

        when others =>
          next_mem_address := (others => DontCareValue);
          --dec_insn_reads_memory <= '0';
      end case;
    end if;

    dec_spOffset <= tSpOffset;
    dec_mem_address <= next_mem_address;

  end process;

  genwritesignal:process (dec_decoded_opcode, dec_idim_flag_q, do_interrupt)
  begin
    if do_interrupt='1' then
      dec_insn_writes_memory <= '1';
    else
    case dec_decoded_opcode is
      when Insn_Im
          |Insn_LoadSP
          |Insn_Dup
          |Insn_DupStackB
          |Insn_Emulate
          |Insn_PushSP
          |Insn_Store
          |Insn_StoreSP
          |Insn_StoreSP2
          |Insn_StoreSP3
          |Insn_StoreB  =>
        dec_insn_writes_memory <= '1';
      when others =>
        dec_insn_writes_memory <= '0';
    end case;
    end if;
  end process;


  genisstoreisloadsp:process (dec_decoded_opcode)
  begin
    dec_insn_is_storesp <= '0';
    dec_insn_is_loadsp <= '0';
    dec_insn_is_syncb_hazard <= '0';
    dec_insn_need_syncb <= '0';
    dec_insn_is_dualpop <= '0';
    dec_insn_is_dpop_hazard <= '0';

    case dec_decoded_opcode is
      when Insn_LoadSP
          |Insn_PopDown =>
        dec_insn_is_loadsp <= '1';

      when Insn_Store
          |Insn_Neqbranch =>
        dec_insn_need_syncb <= '1';
        dec_insn_is_dualpop <= '1';

      when Insn_StoreSP =>
        dec_insn_is_storesp <= '1';

      when Insn_StoreSP2 =>
        dec_insn_is_storesp <= '1';
        dec_insn_is_syncb_hazard <= '1';

      when Insn_StoreSP3 =>
        dec_insn_is_storesp <= '1';
        dec_insn_is_dpop_hazard <= '1';
        dec_insn_is_syncb_hazard <= '1';

      when Insn_PopSP
          |Insn_StoreB =>
        dec_insn_need_syncb <= '1';

      when Insn_break =>
        dec_insn_is_storesp <= DontCareValue;
        dec_insn_is_loadsp <= DontCareValue;
        dec_insn_is_syncb_hazard <= DontCareValue;
        dec_insn_need_syncb <= DontCareValue;
        dec_insn_is_dualpop <= DontCareValue;
        dec_insn_is_dpop_hazard <= DontCareValue;

      when others =>
    end case;
  end process;

  dec_mem_read_enable <= dec_insn_reads_memory;

  ram_o_addra(alzpu_ram_num_bits-1 downto minAddrBit) <= std_logic_vector(dec_mem_address(alzpu_ram_num_bits-1 downto minAddrBit));

  -- Don't put busy on if we're branching, because that kills newbranch operation
  dec_o_busy <= '1' when dec_hazard='1' or (dec_do_freeze='1' and ifu_i_do_branch='0') or dec_stallfreeze_for_stacka='1' else '0';
                                
  do_interrupt <= '1' when interrupt='1'  and ex_insn_branch='0' --and dec_o_busy='0'
    and in_interrupt_q='0' and dec_o_idim_flag='0' and dec_insn_need_syncb_q='0' and dec_o_flush_ex='0' else '0';

  dec_o_idim_flag <= dec_idim_flag_dly_q; -- 2 clocks later.

  dechazcompgen: if improved_hazard_prevention=true generate
    -- I suspect a bug somewhere, but I have to check for 2 offsets here.
    dec_hazard_comp <= '1' when dec_o_spOffset = dec_spOffset+1 or dec_o_spOffset = dec_spOffset+2 else '0';
  end generate;

  decnohazcompgen: if improved_hazard_prevention=false generate
    dec_hazard_comp <= '1';
  end generate;
  
  hazgeb: process(dec_insn_is_loadsp, dec_insn_was_storesp_q, dec_insn_is_resync_b, dec_insn_was_syncb_hazard_q,
                  dec_insn_is_dpop_hazard, dec_insn_was_dpop_hazard_q,
                  dec_insn_is_dualpop,dec_hazard_comp)
  begin
    dec_hazard<='0';
    if dec_insn_is_loadsp='1' and dec_insn_was_storesp_q='1' then
      if dec_hazard_comp='1' then
        dec_hazard <= '1';
      end if;
    elsif dec_insn_is_resync_b='1' and dec_insn_was_syncb_hazard_q='1' then
      dec_hazard <= '1';
    elsif dec_insn_is_dualpop='1' and dec_insn_was_dpop_hazard_q='1' then
      dec_hazard <= '1';
    end if;
  end process;

  dec_o_flush_ex <= dec_flush_q;

  spoffq: process(clk)
  begin
    if rising_edge(clk) then
      if dec_i_freeze='0' then
        dec_o_spOffset(4) <=not ifu_o_opcode(4);
        dec_o_spOffset(3 downto 0) <=unsigned(ifu_o_opcode(3 downto 0));
      end if;
    end if;
  end process;

  dec_do_freeze <= dec_insn_need_syncb_q and not dec_insn_need_syncb_dly_q;

  process(clk)
  begin
    if clk'event and clk='1' then
      if rst='1' then

        dec_opcode_valid_int <= '0';
        dec_idim_flag_q <= '0';
        dec_idim_flag_dly_q <= '0';
        dec_dSp <= sp_type(spStart);
        dec_o_mem_wren <= '0';
        dec_o_insn <= Insn_Nop;
        dec_o_interrupt <= '0';
        dec_insn_was_storesp_q <= '0';
        dec_insn_was_syncb_hazard_q <= '0';
        dec_hazard_q <= '0';
      else

        dec_hazard_q <= '0';
        dec_o_mem_wren <= '0';
        dec_insn_was_storesp_q <= '0'; --reset this ff. otherwise we'll loop on hazard
        dec_insn_was_dpop_hazard_q <= '0'; --reset this ff. otherwise we'll loop on hazard
        dec_insn_was_syncb_hazard_q <= '0';

        if dec_i_freeze='0'  then
        
          dec_o_pc <= ifu_o_pc;
          dec_o_pc_inc <= ifu_o_pc_inc;
          dec_o_opcode <= ifu_o_opcode;
          dec_o_insn <= dec_decoded_opcode;
          dec_o_interrupt <= do_interrupt;

          if dec_hazard='1' or dec_stallfreeze_for_stacka='1' then
            dec_opcode_valid_int <= '0';
            dec_hazard_q <= '1';
          else

            dec_insn_need_syncb_q <= dec_insn_need_syncb;
            dec_insn_need_syncb_dly_q <= dec_insn_need_syncb_q;

            if dec_insn_need_syncb_q='1' then
              dec_o_mem_wren <= '0';
            else
              dec_o_mem_wren <= dec_insn_writes_memory;
            end if;

            if dec_do_change_sp='1' and (ifu_o_flush='0') and dec_insn_need_syncb_q='0' then
              dec_dSp <= dec_next_sp;
            end if;

            dec_o_sp <= dec_dSp;
            dec_o_spAddOffset <= dec_dSpAddOffset;

            if ifu_o_flush='0' and dec_insn_need_syncb_q='0' then
              dec_idim_flag_q <= dec_idim_flag_int;
              dec_idim_flag_dly_q <= dec_idim_flag_q;
            end if;

            dec_opcode_valid_int <= ifu_o_opcode_valid;
            dec_insn_was_storesp_q <= dec_insn_is_storesp;
            dec_insn_was_syncb_hazard_q <= dec_insn_is_syncb_hazard;
            dec_insn_was_dpop_hazard_q <= dec_insn_is_dpop_hazard;

          end if; -- hazard
        end if;
      end if;
    end if; -- clk'event
  end process;

  intrcontrol: process(clk)
  begin
    if rising_edge(clk) then
      if rst='1' then
        in_interrupt_q <='0';
      else
        if do_interrupt='1' and in_interrupt_q='0' then
          report "Jumping to interrupt";
          in_interrupt_q<='1';
        end if;
        if interrupt='0' and in_interrupt_q='1' then
          if dec_i_freeze='0' and dec_decoded_opcode = insn_poppc then
            report "Leaving interrupt";
            in_interrupt_q <= '0';
          end if;
        end if;

      end if;
    end if;
  end process;

  intrdly: process(clk)
  begin
    if rising_edge(clk) then
      if rst='1' then
        ex_i_interrupt <= '0';
      else
        ex_i_interrupt <= do_interrupt;--dec_o_interrupt;
      end if;
    end if;
  end process;

---------------------------------------------------
------------------- Execution unit ----------------
---------------------------------------------------

  ex_o_stall <= ex_in_mem_busy or ex_stall_int ;
  ex_in_mem_busy <= io_i_busy;
  break <= ex_o_break;

  ram_o_addrb(alzpu_ram_num_bits-1 downto minAddrBit) <= std_logic_vector(ex_mem_write_address(alzpu_ram_num_bits-1 downto minAddrBit));
  ram_o_dib <= ex_mem_write_int;

  process (ex_mem_write_address, ex_do_write, dec_o_opcode,dec_o_insn,ex_stackA)
    variable i: memory_target_type;
  begin
    i := memaddr_target(ex_mem_write_address);
    ram_o_enb <= (others=>'0');
    io_B_wren <= '0';
    case i is
      when TARGET_ROM =>
      when TARGET_RAM =>
        if dec_o_insn=Insn_StoreB then
          case ex_stackA(1 downto 0) is
            when "00" =>  ram_o_enb <= "1000";
            when "01" =>  ram_o_enb <= "0100";
            when "10" =>  ram_o_enb <= "0010";
            when "11" =>  ram_o_enb <= "0001";
            when others =>
          end case;
        else
          ram_o_enb <= (others=>ex_do_write);
        end if;
      when TARGET_IO =>
        io_B_wren <= ex_do_write;
    end case;
  end process;

  ex_o_executing <='1' when  ex_stall_int_q='0' and dec_o_flush_ex='0' and dec_insn_need_syncb_dly_q='0' and sleep='0' else '0'; 
  ex_stackA_ready <= not ex_stackA_changes;
  ex_o_stackTopReady <= ex_stackA_ready;

  ex_stall_int <= ex_mult_busy or ex_shift_busy;  -- or io_B_busy

  writeaddrgen: process ( dec_o_insn, ex_stackB, ex_set_stackB, ex_stackA, dec_o_Sp, dec_o_spAddOffset,
                        ex_i_interrupt)
  begin
    if ex_i_interrupt='1' then
        ex_mem_write_address <= sp_to_memaddr(dec_o_Sp);
        ex_mem_write_int <= ex_set_stackB;
    else
    case dec_o_insn is
      when Insn_Im
          |Insn_LoadSP
          |Insn_Dup
          |Insn_DupStackB
          |Insn_Emulate
          |Insn_PushSP =>
        ex_mem_write_address <= sp_to_memaddr(dec_o_Sp);
        ex_mem_write_int <= ex_set_stackB;

      when Insn_Store =>
        ex_mem_write_address <= ex_stackA(maxAddrBitIncIO downto minAddrBit);
        ex_mem_write_int <= ex_stackB;

      when Insn_StoreB =>
        ex_mem_write_address <= ex_stackA(maxAddrBitIncIO downto minAddrBit);
        ex_mem_write_int <= (others=>DontCareValue);
        ex_mem_write_int(((wordBytes-1-to_integer(ex_stackA(byteBits-1 downto 0)))*8+7) downto (wordBytes-1-to_integer(ex_stackA(byteBits-1 downto 0)))*8) <= ex_stackB(7 downto 0);

      when Insn_StoreSP
          |Insn_StoreSP2
          |Insn_StoreSP3  =>
        ex_mem_write_address <= sp_to_memaddr(dec_o_spAddOffset);
        ex_mem_write_int <= ex_stackA;

      when others =>
        ex_mem_write_address <= (others => DontCareValue);
        ex_mem_write_int <= (others => DontCareValue);
    end case;
    end if;
  end process;



  writegen: process(dec_o_mem_wren, ex_stall_int,dec_o_flush_ex,dec_o_idim_flag, dec_o_insn,ex_i_interrupt)
  begin
    if ex_stall_int='0' and dec_o_flush_ex='0' then
      if ex_i_interrupt = '1' then
        ex_do_write <= dec_o_mem_wren;
      else
      case dec_o_insn is
        when Insn_Im =>
          ex_do_write <= not dec_o_idim_flag;
        when others =>
          ex_do_write <= dec_o_mem_wren;
      end case;
      end if;
    else
      ex_do_write <= '0';
    end if;
  end process;

  stackbgen: process(dec_o_insn, dec_o_memR, ex_stackA, dec_o_idim_flag,dec_insn_need_syncb_dly_q,sleep,ex_i_interrupt)
  begin
    ex_stackB_changes <= not sleep;

    if ex_i_interrupt='1' then
      ex_set_stackB <= ex_stackA;
    else

    if dec_insn_need_syncb_dly_q='1' then
      ex_set_stackB <= dec_o_memR;
    else
      case dec_o_insn is
        when Insn_Im =>
          ex_set_stackB <= ex_stackA;
          ex_stackB_changes <= not dec_o_idim_flag;

        when Insn_LoadSP
            |Insn_Dup
            |Insn_DupStackB
            |Insn_Emulate
            |Insn_StoreSP2
            |Insn_PushSP =>
          ex_set_stackB <= ex_stackA;

        when Insn_StoreSP3
            |Insn_PopDown
            |Insn_PopPC
            |Insn_StoreSP
            |Insn_Add
            |Insn_And
            |Insn_Or
            |Insn_Pop
            |Insn_Eq
            |Insn_Sub
            |Insn_Lessthan
            |Insn_Lessthanorequal
            |Insn_Ulessthan
            |Insn_Mult
            |Insn_Lshiftright
            |Insn_Ulessthanorequal =>
          ex_set_stackB <= dec_o_memR;

        when others =>
          ex_set_stackB <= (others => DontCareValue);
          ex_stackB_changes <= '0';
      end case;
    end if;
    end if;
  end process;

  stackgenq: process(clk)
  begin
    if rising_edge(clk) then
      if rst='1' then
        ex_stackA <= (others =>'0');
        ex_stackB <= (others =>'0');
        ex_o_break <= '0';
        ex_stall_int_q <= '0';
      else
        ex_stall_int_q <= ex_stall_int;

        if ex_stackA_changes='1' then
          ex_stackA <= ex_set_stackA;
        end if;

        if sleep='0' then
          if dec_o_flush_ex='0' then
            if dec_o_insn = Insn_break then
              report "BREAK instruction" severity failure;
              ex_o_break <= '1';
            end if;
          end if;
        if dec_o_flush_ex='0' or dec_insn_need_syncb_dly_q='1' then
          if ex_stall_int_q='0' and dec_hazard_q='0' then
            if ex_stackB_changes='1' then
              --if ram_o_enb='1' then --dec_mem_read_enable='1' then
              --report "Instruction " & str(dec_o_insn, dec_o_spOffset) & ": set stackB to " & hstr(std_logic_vector(ex_set_stackB)) &
              --" written to position " & hstr(std_logic_vector(ex_mem_write_address)) severity note;
              --end if;
              ex_stackB <= ex_set_stackB;
            end if;
          end if;
        end if;
      end if;
      end if;
    end if;
  end process;

  aluen: process(dec_o_insn, dec_hazard_q, dec_insn_need_syncb_dly_q, dec_o_flush_ex)
  begin
    ex_stackA_changes <= not (dec_hazard_q or dec_insn_need_syncb_dly_q or dec_o_flush_ex); 
    case dec_o_insn is
      when Insn_Im
          |Insn_Add
          |Insn_AddSP
          |Insn_AddTop
          |Insn_Or
          |Insn_And
          |Insn_Not
          |Insn_Shift
          |Insn_Flip
          |Insn_Emulate
          |Insn_DupStackB
          |Insn_PopPC
          |Insn_Pop
          |Insn_StoreSP
          |Insn_StoreSP2
          |Insn_StoreSP3
          |Insn_Load
          |Insn_StoreB
          |Insn_Neqbranch
          |Insn_LoadSP
          |Insn_Store
          |Insn_PopSP
          |Insn_Sub
          |Insn_PushSP
          |Insn_Eq
          |Insn_Loadb
          |Insn_Lessthan
          |Insn_Lessthanorequal
          |Insn_Ulessthan
          |Insn_Ulessthanorequal
          |Insn_Mult
          |Insn_Lshiftright =>
      when others =>
        ex_stackA_changes <= '0';
    end case;
  end process;

  alu: process(dec_o_insn, ex_stackA, ex_stackB, dec_o_memR, dec_o_opcode,
    dec_o_pc_inc, dec_o_sp, dec_o_idim_flag,dec_hazard_q,dec_insn_need_syncb_dly_q,dec_o_flush_ex,
    ex_multResult, ex_i_interrupt,dec_o_pc,ex_shiftresult)
  begin

    ex_do_mult <= '0';
    ex_do_shift <= '0';
    if ex_i_interrupt='1' then
      ex_set_stackA <= pc_to_cpuword(dec_o_pc);
    else
    case dec_o_insn is
      when Insn_Im =>
        if dec_o_idim_flag='1' then
          ex_set_stackA(wordSize-1 downto 7) <= ex_stackA(wordSize-8 downto 0);
        else
          for i in wordSize-1 downto 7 loop
            ex_set_stackA(i) <= dec_o_opcode(6);
          end loop;
        end if;
        ex_set_stackA(6 downto 0) <= unsigned(dec_o_opcode(6 downto 0));
      when Insn_Add =>
        ex_set_stackA <= ex_stackA + ex_stackB;
      when Insn_AddSP =>
        ex_set_stackA <= ex_stackA + dec_o_memR;
      when Insn_AddTop =>
        ex_set_stackA <= ex_stackA + ex_stackB;
      when Insn_Or =>
        ex_set_stackA <= ex_stackA or ex_stackB;
      when Insn_And =>
        ex_set_stackA <= ex_stackA and ex_stackB;
      when Insn_Not =>
        ex_set_stackA <= not ex_stackA;
      when Insn_Shift =>
        ex_set_stackA(wordSize-1 downto 1) <= ex_stackA(wordSize-2 downto 0);
        ex_set_stackA(0) <= '0';
      when Insn_Flip =>
        for i in 0 to wordSize-1 loop
          ex_set_stackA(i) <= ex_stackA(wordSize-1-i);
        end loop;
      when Insn_Emulate =>
        ex_set_stackA <= pc_to_cpuword(dec_o_pc_inc);

      when Insn_DupStackB
          |Insn_PopPC
          |Insn_Pop
          |Insn_StoreSP
          |Insn_StoreSP2
          |Insn_StoreSP3 =>
        ex_set_stackA <= ex_stackB;

      when Insn_Load
          |Insn_StoreB
          |Insn_Neqbranch
          |Insn_LoadSP
          |Insn_Store
          |Insn_PopSP =>
        ex_set_stackA <= dec_o_memR;

      when Insn_Sub =>
        ex_set_stackA <= ex_stackB - ex_stackA;
      when Insn_PushSP =>
        ex_set_stackA <= sp_to_cpuword(dec_o_sp);
      when Insn_Eq =>
        ex_set_stackA <= (others=>'0');
        if ex_stackA = ex_stackB then
          ex_set_stackA(0)<='1';
        end if;
      when Insn_Loadb =>
        ex_set_stackA <= (others => '0');
        ex_set_stackA(7 downto 0) <= unsigned( dec_o_memR(((wordBytes-1-to_integer(ex_stackA(byteBits-1 downto 0)))*8+7) downto (wordBytes-1-to_integer(ex_stackA(byteBits-1 downto 0)))*8));
      when Insn_Lessthan =>
        ex_set_stackA <= (others=>'0');
        if signed(ex_stackA) < signed(ex_stackB) then
          ex_set_stackA(0)<='1';
        end if;
      when Insn_Lessthanorequal =>
        ex_set_stackA <= (others=>'0');
        if signed(ex_stackA) <= signed(ex_stackB) then
          ex_set_stackA(0)<='1';
        end if;
      when Insn_Ulessthan =>
        ex_set_stackA <= (others=>'0');
        if ex_stackA < ex_stackB then
          ex_set_stackA(0)<='1';
        end if;
      when Insn_Ulessthanorequal =>
        ex_set_stackA <= (others=>'0');
        if ex_stackA <= ex_stackB then
          ex_set_stackA(0)<='1';
        end if;
      when Insn_Mult =>
        ex_do_mult <= '1';
        ex_set_stackA <= ex_multResult;
      when Insn_Lshiftright =>
        ex_do_shift <= '1';
        ex_set_stackA <= ex_shiftresult;

      when others =>
        ex_set_stackA <= (others=> DontCareValue);
        --ex_stackA_changes <= '0';
    end case;
    end if;
  end process;

  ifu_i_do_branch <= ex_insn_branch or ex_i_interrupt;

  genbranchsig: process (dec_o_insn,dec_flush_q,dec_hazard_q, dec_o_flush_ex,
        dec_insn_need_syncb_q,dec_insn_need_syncb_dly_q, ex_stackB,ex_i_interrupt)
  begin
    ex_insn_branch <= '0';
    case dec_o_insn is
      when Insn_Neqbranch =>
        if dec_hazard_q='0' and dec_insn_need_syncb_dly_q='0' and dec_o_flush_ex='0' then
          if ex_stackB /= 0 then
            ex_insn_branch <= '1';
          end if;
        end if;
      when Insn_PopPC =>
        if dec_hazard_q='0' and dec_insn_need_syncb_dly_q='0' and dec_o_flush_ex='0' then
          ex_insn_branch <= '1';
        end if;
      when others =>
    end case;
  end process;

  genbranchaddress: process (dec_o_insn, dec_o_opcode, ex_stackA, ex_stackB, dec_o_pc, dec_o_pc_inc,ex_i_interrupt)
  begin
    if ex_i_interrupt='1' then
      ifu_i_branch_address <= cpuword_to_pc(x"00000020");
    else
    case dec_o_insn is
      when Insn_PopPC =>
        ifu_i_branch_address <= cpuword_to_pc(ex_stackA);
      when Insn_Neqbranch =>
        ifu_i_branch_address <= dec_o_pc + cpuword_to_pc(ex_stackA);
      when others =>
        ifu_i_branch_address <= (others => DontCareValue);
    end case;
    end if;
  end process;

  mymult: mult
    port map (
      clk   => clk,
      rst   => rst,
      en    => ex_do_mult,
      a     => ex_stackA,
      b     => ex_stackB,
      o     => ex_multResult,
      busy  => ex_mult_busy
    );

  ex_shift_aritm <= '0';

  myshifter: fastshifter
    port map (
      clk       => clk,
      reset     => rst,
      valid_in  => ex_do_shift,
      data_in   => ex_stackB,
      data_out  => ex_shiftresult,
      shift_amount => ex_stackA(4 downto 0),
      aritm     => ex_shift_aritm,
      busy      => ex_shift_busy
    );

  trace_valid_insn <= ex_o_executing and dec_opcode_valid_int;

  trc: if alzpu_tracer_enabled generate

    tracer_inst: alzpu_tracer
      port map (
        clk     => clk,
        rst     => rst,
        opcode  => dec_o_opcode,
        insn    => dec_o_insn,
        pc      => dec_o_pc,
        sp      => dec_o_sp,
        stackA  => ex_stackA,
        stackB  => ex_stackB,
        hazard  => dec_hazard_q,
        branch  => dec_o_flush_ex,
        sync    => dec_insn_need_syncb_dly_q,
        memR    => dec_o_memR,
        memR_en => dec_insn_reads_memory,
        exec    => trace_valid_insn
      );

  end generate;

  valgen: if alzpu_validator_enabled generate
    vali: alzpu_validator
      port map (
        clk => clk,
        rst => rst,
        valid => trace_valid_insn,
        op   => dec_o_opcode,
        stackA => ex_stackA,
        stackB => ex_stackB,
        pc  => dec_o_pc,
        sp => dec_o_sp
    );
  end generate;

--------------------------------------------
--- Arbiter for IO access ------------------
--------------------------------------------

  -- Assume port A is always read (from decoder unit), and port B is always write (from execution unit)
  --
  io_o_clk <= clk;
  io_o_rst <= rst;
  io_A_busy <= '1' when grant_b else io_i_busy;
  io_B_busy <= io_i_busy;
  grant_b <= io_B_wren='1';
  grant_a <= io_A_rden='1' and io_B_wren='0';

  -- Always write stackB.
  io_o_wr_data <= ex_stackB;
  -- And address is always stackA, for loads and stores.
  io_o_addr <= std_logic_vector(ex_stackA(maxAddrBit downto minAddrBit));

  io_o_wr_en <= io_B_wren when grant_b else '0';

  io_o_en <= '1' when io_A_rden='1' or io_B_wren='1' else '0';

end behave;
