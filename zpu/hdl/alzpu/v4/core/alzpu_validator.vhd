-- ALZPU
--
-- Copyright 2004-2008 oharboe - �yvind Harboe - oyvind.harboe@zylin.com
-- Copyright 2008 alvieboy - �lvaro Lopes - alvieboy@alvie.com
-- 
-- The FreeBSD license
-- 
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above
--    copyright notice, this list of conditions and the following
--    disclaimer in the documentation and/or other materials
--    provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE ZPU PROJECT ``AS IS'' AND ANY
-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
-- PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
-- ZPU PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
-- INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
-- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
-- STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
-- ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
-- The views and conclusions contained in the software and documentation
-- are those of the authors and should not be interpreted as representing
-- official policies, either expressed or implied, of the ZPU Project.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;


library work;
use work.alzpu_config.all;
use work.alzpupkg.all;
use work.txt_util.all;

entity alzpu_validator is
port (
  clk: in std_logic;
  rst: in std_logic;
  valid: in std_logic;
  op:          in std_logic_vector(7 downto 0);
  stackA:      in cpuword_type;
  stackB:      in cpuword_type;
  pc:          in pc_type;
  sp:          in sp_type
);
end alzpu_validator;

architecture behave of alzpu_validator is

component zpuvalidatortable is
port ( 
cnt:         in integer;
op:          out std_logic_vector(7 downto 0);
stackA:      out std_logic_vector(wordSize-1 downto 0);
stackB:      out std_logic_vector(wordSize-1 downto 0);
pc:          out std_logic_vector(wordSize-1 downto 0);
sp:          out std_logic_vector(wordSize-1 downto 0)
);
end component;


signal cnt: integer := 0;


signal  i_op:          std_logic_vector(7 downto 0);
signal  i_stackA:      std_logic_vector(wordSize-1 downto 0);
signal  i_stackB:      std_logic_vector(wordSize-1 downto 0);
signal  i_pc:          std_logic_vector(wordSize-1 downto 0);
signal  i_sp:          std_logic_vector(wordSize-1 downto 0);

--  Delay signals

signal  op_q:          std_logic_vector(7 downto 0);
signal  stackA_q:      std_logic_vector(wordSize-1 downto 0);
signal  stackB_q:      std_logic_vector(wordSize-1 downto 0);
signal  pc_q:          pc_type;
signal  sp_q:          sp_type;
signal  valid_q:       std_logic;

begin

vtable: zpuvalidatortable
  port map (
    cnt => cnt,
    op  => i_op,
    stackA => i_stackA,
    stackB => i_stackB,
    pc => i_pc,
    sp => i_sp
  );

process(clk,rst)
begin
  if clk'event and clk='1' then
    if rst='1' then
      op_q     <=  (others=>'0');
      stackA_q <=  (others=>'0');
      stackB_q <=  (others=>'0');
      pc_q     <=  (others=>'0');
      sp_q     <=  (others=>'0');
      valid_q  <=  '0';
    else
      op_q     <= op;
      stackA_q <= std_logic_vector(stackA);
      stackB_q <= std_logic_vector(stackB);
      pc_q     <= pc;
      sp_q     <= sp;
      valid_q  <= valid;
    end if;
  end if;
end process;

process(clk,rst)
variable abort: boolean := false;
begin
  if clk'event and clk='1' then
    if rst='1' then
      cnt <= 0;
      --report "RESET RESET RESET" severity note;
    else
      if valid_q='1' then
        abort := false;
        if (std_logic_vector(sp_to_cpuword(sp_q)) /= i_sp) then
          report "At clock " & str(cnt) & " - INVALID SP! expected " &hstr(i_sp)&", got "&hstr(sp_to_cpuword(sp_q)) severity note;
          abort := true;
        end if;

        if (std_logic_vector(pc_to_cpuword(pc_q)) /= i_pc ) then
          report "At clock " & str(cnt) & " - INVALID PC! expected " &hstr(std_logic_vector(i_pc))&", got "&hstr(std_logic_vector(pc_q)) severity note;
          abort := true;
        end if;

        if (stackA_q /= i_stackA) then
          report "At clock " & str(cnt) & " - INVALID StackA! expected " &hstr(std_logic_vector(i_stackA))&", got "&hstr(std_logic_vector(stackA_q)) severity note;
          abort := true;
        end if;

        if (stackB_q /= i_stackB) then
          report "At clock " & str(cnt) & " - INVALID StackB! expected " &hstr(std_logic_vector(i_stackB))&", got "&hstr(std_logic_vector(stackB_q)) severity note;
          abort := true;
        end if;

        if (op_q /= i_op) then
          report "At clock " & str(cnt) & " - INVALID OPCODE! expected " &hstr(i_op)&", got "&hstr(op_q) severity note;
          abort := true;
        end if;
        if abort then
          report "Aborting program" severity failure;
        end if;
        cnt <= cnt + 1;
        --report str(cnt)  severity note;
      end if;
    end if;
  end if;
end process;

end behave;
