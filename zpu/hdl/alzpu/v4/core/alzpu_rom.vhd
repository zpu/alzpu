--
--  ALZPU 
-- 
--  Copyright 2008 Alvaro Lopes <alvieboy@alvie.com>
-- 
--  Version: 1.0
-- 
--  The FreeBSD license
--  
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following conditions
--  are met:
--  
--  1. Redistributions of source code must retain the above copyright
--     notice, this list of conditions and the following disclaimer.
--  2. Redistributions in binary form must reproduce the above
--     copyright notice, this list of conditions and the following
--     disclaimer in the documentation and/or other materials
--     provided with the distribution.
--  
--  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY
--  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
--  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
--  PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
--  ZPU PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
--  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
--  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
--  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
--  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
--  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
--  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
--  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--  
-- */
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.alzpupkg.all;
use work.alzpu_config.all;

entity alzpu_rom is
  port (
    clka:       in std_logic;
    clkb:       in std_logic;
    rst:        in std_logic;
    addra:      in std_logic_vector(alzpu_rom_num_bits-1 downto 0);
    addrb:      in std_logic_vector(alzpu_rom_num_bits-1 downto 2);
    ena:        in std_logic;
    enb:        in std_logic;
    wra:        in std_logic;
    wrb:        in std_logic;
    doa:        out std_logic_vector(7 downto 0);
    dob:        out cpuword_type;
    dia:        in std_logic_vector(7 downto 0);
    dib:        in cpuword_type
  );
end entity alzpu_rom;

architecture behave of alzpu_rom is

  --signal a_rom_dato : std_logic_vector(7 downto 0);
  --signal b_rom_dato : std_logic_vector(31 downto 0);
  --signal a_request_q: std_logic;
  --signal b_request_q: std_logic;
--  signal doa_i: std_logic_vector downto 0);
  signal dob_i: std_logic_vector(wordSize-1 downto 0);

  component dp_rom_8_32 is
    port (ADDRA: in std_logic_vector(alzpu_rom_num_bits-1 downto 0);
      CLK : in std_logic;
      ENA:   in std_logic;
      WEA: in std_logic; -- to avoid a bug in Xilinx ISE
      DOA: out STD_LOGIC_VECTOR (7 downto 0);
      ADDRB: in std_logic_vector(alzpu_rom_num_bits-1 downto 2);
      DIA: in STD_LOGIC_VECTOR (7 downto 0); -- to avoid a bug in Xilinx ISE
      WEB: in std_logic;
      ENB:   in std_logic;
      DOB: out STD_LOGIC_VECTOR (31 downto 0);
      DIB: in STD_LOGIC_VECTOR (31 downto 0)
    );
  end component dp_rom_8_32;

begin

  my_prom:  dp_rom_8_32
    port map(
    addra  => addra,
    doa  => doa,
    ena    => ena,
    web    => wrb,
    wea    => wra,
    dib  => std_logic_vector(dib),
    dia  => dia,
    clk     => clka,
    addrb  => addrb,
    enb    => enb,
    dob  => dob_i
  );
  dob <= unsigned(dob_i);

end behave;


