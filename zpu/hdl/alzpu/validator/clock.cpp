/*            

 Copyright (C) 2008 Alvaro Lopes <alvieboy at alvie dot com>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 */
#include "clock.h"

clock::clock( unsigned freq ): m_uFreq(freq), m_uCounter(0), m_bExit(false)
{

}
void clock::delayTick(unsigned int delay)
{
	m_uCounter+=delay;
}

void clock::stop()
{
	m_bExit=true;
}

void clock::run()
{
	while (!m_bExit) {
		m_tickSignal.emit(m_uCounter);
		m_uCounter++;
	}
}
