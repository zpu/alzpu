/*            

 Copyright (C) 2008 Alvaro Lopes <alvieboy at alvie dot com>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 */
#include "zpuinterface.h"
#include "zpuexception.h"
#include <cassert>
#include <sstream>
#include <iostream>

std::vector<zpudeventry>::iterator zpuinterface::locateDevice(const dev_address_t &address)
{
	std::vector<zpudeventry>::iterator i;

	for (i=m_vecDevices.begin(); i!=m_vecDevices.end(); i++) {

		if ( i->address > address )
			continue;

		//        printf("WScan %08x, this %08x with mask %08x \n", address, i->address, i->mask);
		if ( (address- i->address) < i->length ) {
			//  		printf("Matches\n");
			return i;
		}
	}
	return m_vecDevices.end();
}

std::vector<zpudeventry>::const_iterator zpuinterface::locateDevice(const dev_address_t &address) const
{
	std::vector<zpudeventry>::const_iterator i;

	for (i=m_vecDevices.begin(); i!=m_vecDevices.end(); i++) {

		if ( i->address > address )
			continue;
		if ( (address- i->address) < i->length ) {
			return i;
		}
	}
	return m_vecDevices.end();
}

void zpuinterface::attachDevice( zpudevice *dev, unsigned base, unsigned length, unsigned mask )
{
	zpudeventry e;
	e.device = dev;
	e.address=base;
	e.length = length;
    e.mask = mask;
	m_vecDevices.push_back(e);
}


dev_value_t zpuinterface::read(const dev_address_t &addr) const
{
	std::ostringstream ss;
	std::vector<zpudeventry>::const_iterator dev = locateDevice(addr);

	if (dev==m_vecDevices.end()) {
		ss<<"READ: Cannot find device for address 0x"<<std::hex<<addr<<std::dec<<std::endl;
		throw ZPUException(ss.str());
	}
	dev_value_t ret = dev->device->read(addr & dev->mask);
   // printf("Reading address 0x%08x = 0x%s\n", addr, ret.asString().c_str());
	return ret;
}

void zpuinterface::write(const dev_address_t & addr, const dev_value_t & value, bool setup)
{
    std::ostringstream ss;
	std::vector<zpudeventry>::iterator dev = locateDevice(addr);

	if (dev==m_vecDevices.end()) {
		ss<<"WRITE: Cannot find device for address 0x"<<std::hex<<addr<<", while writing value 0x"<<value.value()<<std::dec<<std::endl;
        throw ZPUException(ss.str());
	}
	
    //printf("Write address 0x%08x = 0x%sx\n", addr, value.asString().c_str());
	dev->device->write(addr & dev->mask,value,setup);
	return;
}
