/*            

 Copyright (C) 2008 Alvaro Lopes <alvieboy at alvie dot com>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 */
#include "zpu.h"
#include <cassert>
#include "common.h"
#include <iostream>
#include "zpuexception.h"
#include <fstream>

const char *insnName[] = {
	"im         ",
	"storesp    ",
	"loadsp     ",
	"emulate    ",
	"addsp      ",
	"ibreak     ",
	"pushsp     ",
	"poppc      ",
	"add        ",
	"and        ",
	"or         ",
	"load       ",
	"not        ",
	"flip       ",
	"nop        ",
	"store      ",
	"popsp      ",
	"neqbranch  ",
	"ulessthan  ",
	"pushspadd  ",
	"eq         ",
	"callpcrel  ",
	"mult       ",
	"lshiftright",
	"ashiftleft ",
	"sub        ",
	"lessthan   ",
	"loadb      ",
	"ulessoreq  ",
	"lessoreq   ",
	"loadb      ",
	"synca      ",
	"syncb      ",
	"call       ",
	"poppcrel   ",
	"ashiftright",
	(char*)0
};

zpu::zpu(class clock *myclock,unsigned spstart): m_uSPStart(spstart), m_clock(myclock)
{
	m_bMaxInstructionsSet=false;
	m_bShowStats=false;
	reset();
}

void zpu::setShowStats(bool b)
{
    m_bShowStats = b;
}

void zpu::setMaxInstructions(unsigned int max)
{
	m_uMaxInstructions = max;
	m_bMaxInstructionsSet = true;
}

void zpu::setSkipDump(unsigned int max)
{
	m_uSkipDump = max;
	m_bSkipDumpSet = true;
}

void zpu::reset()
{
	m_uSPmax = m_uSPmin = m_uSP = m_uSPStart;
	m_bIdimflag = false;
    m_uPC = 0x0;
}

void zpu::attachDevice(zpudevice *dev, unsigned int addr, size_t size, unsigned mask)
{
    m_Interface.attachDevice(dev,addr,size,mask);
}

dev_value_t zpu::readmem(const dev_address_t &addr) const
{
	return m_Interface.read(addr);
}

/*dev_value_t zpu::readmem_Byte(const dev_address_t &addr) const
{
	dev_value_t v = m_Interface.read(addr);
	v>>=8*(3-(addr&0x3));
	return v;
}
*/




void zpu::writemem(const dev_address_t &addr, const dev_value_t & value)
{
	m_Interface.write(addr,value);
}


dev_value_t zpu::stackA() const {
	return readmem(m_uSP);
}

dev_value_t zpu::stackB() const {
	return readmem(m_uSP+4);
}

void zpu::push(const dev_value_t &val)
{
	m_uSP-=4;
	writemem(m_uSP,val);
}

dev_value_t zpu::pop()
{
	dev_value_t r = readmem(m_uSP);
	m_uSP+=4;
	return r;
}

void zpu::process_im(unsigned char op)
{
	unsigned int v = 0;
	if (m_bIdimflag) {
		v = stackA().value();
		v <<= 7;
		v |= op &0x7f;
		writemem(m_uSP,dev_value_t(v));
		m_uIdimSize++;
	} else {
		m_bIdimflag=true;
		m_uIdimSize=1;
		push(dev_value_t(0));

		if ( op&0x40) {
			v=0xFFFFFF80;
		}
		v |= op &0x7f;
		writemem(m_uSP,dev_value_t(v));
	}
	m_uPC++;
}

void zpu::process_storesp(unsigned char op)
{
   // printf("StoreSP Offset: %08x\n", spOffset<<2);
	dev_value_t v = pop();
	//printf("Popped value: %08x\n",v);
	writemem( m_uSP - 4 + (m_uSPOffset<<2), v);
	m_uPC++;
}

void zpu::process_loadsp(unsigned char op)
{
	//printf("LoadSP Offset: %08x\n", (spOffset<<2));

	dev_value_t v = readmem( m_uSP + (m_uSPOffset<<2) );
//	printf("LOADSP: Value loaded : 0x%08x at address 0x%08x\n",v, (m_uSPOffset<<2) );
	push( v );
	m_uPC++;
}

void zpu::process_emulate(unsigned char op)
{
	push(dev_value_t(m_uPC+1));
	m_uPC=get_bits(op,4,0)<<5;
}

void zpu::process_addsp(unsigned char op)
{
	int offset =  (int)op - 0x10;
	dev_address_t address = m_uSP + offset*4;
	dev_value_t delta=pop();
	push(delta+readmem(address));
	m_uPC++;
}

void zpu::process_ibreak(unsigned char op)
{
    throw ZPUException("BREAK instrucion\n");
}

void zpu::process_pushsp(unsigned char op)
{
	push(dev_value_t(m_uSP));
	m_uPC++;
}

void zpu::process_poppc(unsigned char op)
{
	m_uPC = pop().value();
}

void zpu::process_poppcrel(unsigned char op)
{
	m_uPC = m_uPC + pop().value();
}

void zpu::process_add(unsigned char op)
{
	push( pop() + pop() );
	m_uPC++;
}

void zpu::process_sub(unsigned char op)
{
	dev_value_t a = pop();
	dev_value_t b = pop();
	push( b - a );
	m_uPC++;
}

void zpu::process_and(unsigned char op)
{
	push( pop() & pop() );
	m_uPC++;
}

void zpu::process_or(unsigned char op)
{
	zpu::push( pop() | pop() );
	m_uPC++;
}

void zpu::process_load(unsigned char op)
{
	push( readmem( pop().value() ) );
	m_uPC++;
}

void zpu::process_loadb(unsigned char op)
{
	dev_value_t addr = pop();
	dev_value_t val;

	dev_value_t rmem = readmem( addr.value() & 0xFFFFFFFC );

	val = dev_value_t( (rmem.value() >> (8*(3-(addr.value() & 0x3)))) & 0xFF );
	push( val );
	m_uPC++;
}

void zpu::process_storeb(unsigned char op)
{
	dev_value_t addr = pop();
	dev_value_t val = pop();

	dev_value_t rmem = readmem( addr.value() & 0xFFFFFFFC );

	val = val | dev_value_t( (rmem.value() >> (8*(3-(addr.value() & 0x3)))) & 0xFF );

	writemem(addr.value() & 0xFFFFFFFC, val);
	m_uPC++;
}

void zpu::process_not(unsigned char op)
{
	push( pop() ^ dev_value_t(0xffffffff) );
	m_uPC++;
}

void zpu::process_nop(unsigned char op)
{
    m_uPC++;
}

void zpu::process_store(unsigned char op)
{
	dev_address_t addr = pop().value();
	dev_value_t val = pop();
  //  printf("STORE: Storing val 0x%08x in address 0x%08x\n",val, addr);

	writemem(addr,val);
	m_uPC++;
}

void zpu::process_popsp(unsigned char op)
{
	m_uSP = pop().value();
	m_uPC++;
}

void zpu::process_neqbranch(unsigned char op)
{
	dev_value_t delta = pop();
    dev_value_t val = pop();
	if (val.value()!=0)
		m_uPC += delta.value();
	else
		m_uPC++;
}

void zpu::process_ulessthan(unsigned char op)
{
	dev_value_t a  = pop();
	dev_value_t b  = pop();
	push ( a<b ? dev_value_t(1): dev_value_t(0));
	m_uPC++;
}

void zpu::process_ulessthanorequal(unsigned char op)
{
	dev_value_t a  = pop();
	dev_value_t b  = pop();
	push ( a<=b ? dev_value_t(1): dev_value_t(0));
	m_uPC++;
}

void zpu::process_lessthan(unsigned char op)
{
	int a;
	a = (int)(pop().value());
	int b;

	b = (int)(pop().value());
	push ( a<b ? dev_value_t(1): dev_value_t(0));
	m_uPC++;
}

void zpu::process_lessthanorequal(unsigned char op)
{
	int a;
	a = (int)(pop().value());
	int b;

	b = (int)(pop().value());
	push ( a<=b ? dev_value_t(1): dev_value_t(0));
	m_uPC++;
}

void zpu::process_pushspadd(unsigned char op)
{
	dev_value_t a(m_uSP);
	dev_value_t b = pop() << 2;
	push(a+b);
	m_uPC++;
}

void zpu::process_eq(unsigned char op)
{
	dev_value_t a = pop();
	dev_value_t b = pop();
	push( a==b ? dev_value_t(1): dev_value_t(0));
	m_uPC++;
}

void zpu::process_callpcrel(unsigned char op)
{
	dev_value_t a = pop();
	push( dev_value_t(m_uPC+1 ));
	m_uPC = m_uPC+a.value();
}
void zpu::process_call(unsigned char op)
{
	dev_value_t a = pop();
	push( dev_value_t(m_uPC+1 ));
	m_uPC = a.value();
}

void zpu::process_flip(unsigned char op)
{
	int i;
	dev_value_t a = pop();
	unsigned int b = 0;
	for (i=0;i<32; i++) {
		b<<=1;
		b |= ( a.value() &0x1 );
		a>>=1;
	}
	push(dev_value_t(b));
	m_uPC++;
}
void zpu::process_mult(unsigned char op)
{
	int a = (int)pop().value();
	int b = (int)pop().value();
	push(dev_value_t((unsigned int)a*b));
	m_uPC++;
}

void zpu::process_lshiftright(unsigned char op)
{
	dev_value_t shift;
	dev_value_t valX;
	dev_value_t t;

	shift = pop() & dev_value_t(0x3f);
	valX = pop();
	t = valX >> shift;

	//printf("LSHIFTRIGHT: %08x by %08x, gives %08x\n", valX, shift, t);

	push(t);
	m_uPC++;
}

void zpu::process_ashiftleft(unsigned char op)
{
	dev_value_t shift;
	dev_value_t valX;
	dev_value_t t;
	shift = pop() &0x3f;
	valX = pop();
	t = valX << shift;

	//printf("ASHIFTLEFT: %08x by %08x, gives %08x\n", valX, shift, t);

	push(t);
	m_uPC++;
}
void zpu::process_ashiftright(unsigned char op)
{
	dev_value_t shift;
	dev_value_t valX;
	dev_value_t t;
	shift = pop() &0x3f;
	valX = pop();
	t = valX >> shift;

	//printf("ASHIFTRIGHT: %08x by %08x, gives %08x\n", valX, shift, t);

	push(t);
	m_uPC++;
}

void zpu::setDumpFile(const std::string &name)
{
	m_TracefileStream.open( name.c_str(), std::ios::out );
	if (!m_TracefileStream.is_open()) {
		throw ZPUException("Cannot open trace file for writing");
	}
}

void zpu::setValidateFile( const std::string & file )
{
	m_ValidatefileStream.open( file.c_str(), std::ios::out );
	if (!m_ValidatefileStream.is_open()) {
		throw ZPUException("Cannot open validate file for writing");
	}
    //m_ValidatefileStream.setbuf(m_pchBuffer, sizeof(m_pchBuffer));
}

void zpu::trace(unsigned char op, const char *insn )
{
	if (m_ValidatefileStream.is_open()) {
		validate_entry_t e;

		e.pc = m_uPC;
		e.sp = m_uSP;
		e.op = op;
		e.stacka = stackA();
		e.stackb = stackB();

		m_vecValidateEntries.push_back(e);
		if (m_bSkipDumpSet) {
			if (m_clock->getTicks() < m_uSkipDump)
				return;
		}
	}
	if (m_TracefileStream.is_open()) {
		char buf[128];

		snprintf(buf,128,"0x%08x INSN 0x%02x PC 0x%08x SP 0x%08x SPDATA %s %s %s\n",
				 (unsigned)m_clock->getTicks(), op, m_uPC, m_uSP, stackA().asString().c_str(), stackB().asString().c_str(),insn);
		m_TracefileStream << buf;
	}
}

static std::vector<std::string> split( const std::string &s, const std::string &delim, bool multiple=true)
{
	std::vector<std::string> eArray;

	std::string::size_type pos = 0;

	if ( s.length() == 0)
		return eArray;

	while ( pos<s.length() && delim.find_first_of(s[pos]) != std::string::npos )
		pos++;

	if ( pos==s.length() )
		return eArray;

	do {
		std::string::size_type d;

		d = s.find_first_of(delim,pos);
		//    fprintf(stderr,"D: Found at index %d\n",d);
		if ( d != std::string::npos ) {

			eArray.push_back( s.substr( pos, d-pos ) );

			pos = d+1;
			if (multiple) {
				while ( pos<s.length() && delim.find_first_of(s[pos]) != std::string::npos )
					pos++;
			}
		} else {

			eArray.push_back( s.substr( pos, s.length() ) );
			pos = s.length();
		}
	} while ( pos<s.length());

	return eArray;
}



void zpu::setImplement(const std::string &list)
{
	std::vector<std::string> vecList = split(list,",");
	std::vector<std::string>::const_iterator i;
	for (i=vecList.begin(); i!=vecList.end(); i++) {
		//std::cerr<<"* Set implement of "<<*i<<" to true"<<std::endl;
		m_mapImplement[*i] = true;
	}
}


void zpu::execute(unsigned char op)
{
#define DISPATCH(start,end,match,method) \
	if (get_bits(op,start,end)==bin<match>::value) { trace(op,#method) ; \
	incInstruction(I_##method); \
	return process_##method(op); }

#define DDISPATCH(start,end,match,method) \
	if (get_bits(op,start,end)==match && m_mapImplement[#method]) { trace(op,#method) ; \
	incInstruction(I_##method); \
	return process_##method(op); }

	m_uSPOffset = 0;
	//	spOffset |= op & 0x10 ? 0x10:0x00;
	m_uSPOffset |= get_bits(op,4,0);
	m_uSPOffset ^= 0x10;

	//m_tickSignal.emit(m_uCounter);

	DISPATCH(7,7,1,im);

	if (m_bIdimflag) {
		// Save stats from IM size
		if ( m_imsizestats.find(m_uIdimSize) == m_imsizestats.end() ) {
			m_imsizestats[m_uIdimSize] = 1;
		} else {
			m_imsizestats[m_uIdimSize]++;
		}
	}
	m_bIdimflag = false;

	DISPATCH(7,5,10,storesp);
	DISPATCH(7,5,11,loadsp);
	DISPATCH(7,4,1,addsp);

	DDISPATCH(7,0,56,neqbranch);
	DDISPATCH(7,0,57,poppcrel);
	DDISPATCH(7,0,61,pushspadd);
	DDISPATCH(7,0,38,ulessthan);
	DDISPATCH(7,0,39,ulessthanorequal);
	DDISPATCH(7,0,36,lessthan);
	DDISPATCH(7,0,37,lessthanorequal);
	DDISPATCH(7,0,46,eq);
	DDISPATCH(7,0,49,sub);
	DDISPATCH(7,0,63,callpcrel);
	DDISPATCH(7,0,41,mult);
	DDISPATCH(7,0,45,call);
	DDISPATCH(7,0,51,loadb);
	DDISPATCH(7,0,52,storeb);


	DDISPATCH(7,0,42,lshiftright);
	DDISPATCH(7,0,44,ashiftright);
	DDISPATCH(7,0,43,ashiftleft);
	/*
	 elsif tOpcode(5 downto 0)=OpCode_Eq then
	 ex_state <=State_Eq;
	 elsif tOpcode(5 downto 0)=OpCode_Lessthan then
	 ex_state <=State_Lessthan;
	 elsif tOpcode(5 downto 0)=OpCode_Lessthanorequal then
	 --ex_state <=State_Lessthanorequal;
	 elsif tOpcode(5 downto 0)=OpCode_Ulessthan then
	 ex_state <=State_Ulessthan;
	 elsif tOpcode(5 downto 0)=OpCode_Ulessthanorequal then
	 --ex_state <=State_Ulessthanorequal;
	 elsif tOpcode(5 downto 0)=OpCode_Loadb then
	 ex_state <=State_Loadb;
	 elsif tOpcode(5 downto 0)=OpCode_Mult then
	 ex_state <=State_Mult;
	 elsif tOpcode(5 downto 0)=OpCode_Storeb then
	 ex_state <=State_Storeb;
	 elsif tOpcode(5 downto 0)=OpCode_Pushspadd then
	 ex_state <=State_Pushspadd;
	 elsif tOpcode(5 downto 0)=OpCode_Callpcrel then
	 ex_state <=State_Callpcrel;
	 elsif tOpcode(5 downto 0)=OpCode_Call then
	 --ex_state <=State_Call;
	 elsif tOpcode(5 downto 0)=OpCode_Sub then
	 ex_state <=State_Sub;
	 */

	DISPATCH(7,5,1,emulate);

	DISPATCH(7,0,100,poppc);
	DISPATCH(7,0,1000,load);
	DISPATCH(7,0,1100,store);
	DISPATCH(7,0,10,pushsp);
	DISPATCH(7,0,1101,popsp);
	DISPATCH(7,0,101,add);
	DISPATCH(7,0,110,and);
	DISPATCH(7,0,111,or);
	DISPATCH(7,0,1001,not);
	DISPATCH(7,0,1010,flip);
	DISPATCH(7,0,1011,nop);
	DISPATCH(3,0,0,ibreak);

	if (execute_nonstd(op))
		return;
	std::cerr<<"Invalid opcode 0x"<<std::hex<<(int)op<<std::endl;
	abort(); // This never happens - so we hope
}

void zpu::incInstruction(insn_t insn)
{
	unsigned i =(unsigned)insn;
	m_stats[i]++;
}

void zpu::dumpStats()
{
	std::tr1::unordered_map<unsigned, unsigned long long>::const_iterator i;
	std::tr1::unordered_map<insn_t, unsigned long long, hashInsn>::const_iterator si;
	std::tr1::unordered_map<unsigned, unsigned>::const_iterator msi;

	unsigned long long allcount =0;
	std::cout << "Statistics: "<<std::endl;


	for (i=m_stats.begin(); i!=m_stats.end(); i++) {
		std::cout<< insnName[i->first] << "  " << i->second<<std::endl;
        allcount += i->second;
	}
	std::cout << "Instruction stall statistics: "<<std::endl<<std::endl;

	for (si=m_stallStats.begin(); si!=m_stallStats.end(); si++) {
		std::cout<< insnName[si->first] << "  " << si->second<<std::endl;
	}

	std::cout << "Total instructions executed: "<<allcount<<std::endl;
	std::cout << "MAX sp: "<<m_uSPmax<<std::endl;
	std::cout << "MIN sp: "<<m_uSPmin<<std::endl;
	std::cout << "Stack used: "<<(m_uSPmax-m_uSPmin)<<std::endl;

	std::cout << "IM statistics: " << std::endl;

	for (msi=m_imsizestats.begin(); msi!=m_imsizestats.end(); msi++) {
		std::cout<< "  Size "<<msi->first << " => " << msi->second<<std::endl;
	}

}

void zpu::run()
{
	m_clock->signalTick().connect( sigc::mem_fun(*this, &zpu::clockTick) );
	m_clock->run();

	if (m_bShowStats)
		dumpStats();

	if (m_ValidatefileStream.is_open()) {
		vwriter writer;
		writer.generate( m_vecValidateEntries, m_ValidatefileStream );
		m_ValidatefileStream.close();
	}
	if (m_TracefileStream.is_open())
		m_TracefileStream.close();
}

void zpu::clockTick(unsigned long long cnt)
{
	unsigned int m = readmem(m_uPC).value();
	m>>=8*(3-(m_uPC&0x3));

	unsigned char op = m&0xff;
	try {
		execute(op);

		if (m_bMaxInstructionsSet && m_clock->getTicks() > m_uMaxInstructions)
			throw ZPUException("Max instructions limit reached");
	} catch (std::exception &e) {

		std::cerr<<"Exception caught, PC "<<std::hex<< m_uPC<<std::dec<<std::endl;
		std::cerr<<e.what()<<std::endl;
		m_clock->stop();
	}
}
