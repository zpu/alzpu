/*            

 Copyright (C) 2008 Alvaro Lopes <alvieboy at alvie dot com>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 */
#include "vwriter.h"

vwriter::vwriter()
{
}

static std::string hex8(unsigned int v)
{
	char buf[9];
	sprintf(buf,"%08x",v);
	return buf;
}
static std::string hex2(unsigned int v)
{
	char buf[3];
	sprintf(buf,"%02x",v);
	return buf;
}

void vwriter::generate( validate_list_t &l, std::ostream &out )
{
	validate_list_t::const_iterator i;
	int n = l.size();

	out << "library IEEE; " << std::endl;
	out << "use IEEE.std_logic_1164.all;" << std::endl;
	out << "use IEEE.std_logic_unsigned.all;" << std::endl;
	out << "use ieee.numeric_std.all;" << std::endl;
	out << "library work;" << std::endl;
	out << "use work.alzpupkg.all;" << std::endl;
	out << "use work.alzpu_config.all;" << std::endl;
    out << "entity zpuvalidatortable is" << std::endl;
	out << "port ( " << std::endl;
	out << "cnt:         in integer;" << std::endl;
    out << "op:          out std_logic_vector(7 downto 0);" << std::endl;
	out << "stackA:      out std_logic_vector(wordSize-1 downto 0);" << std::endl;
    out << "stackB:      out std_logic_vector(wordSize-1 downto 0);" << std::endl;
    out << "pc:          out std_logic_vector(wordSize-1 downto 0);" << std::endl;
	out << "sp:          out std_logic_vector(wordSize-1 downto 0)" << std::endl;
	out << ");" << std::endl;
	out << "end zpuvalidatortable;" << std::endl;
	out << "architecture behave of zpuvalidatortable is" << std::endl;
	out << "subtype ROM_WORD is STD_LOGIC_VECTOR (wordSize-1 downto 0);" << std::endl;
	out << "subtype ROM_PWORD is STD_LOGIC_VECTOR (wordSize-1 downto 0);" << std::endl;
	out << "subtype ROM_BYTE is STD_LOGIC_VECTOR (7 downto 0);" << std::endl;
	out << "type ROM_TABLE is array (0 to "<<(n-1)<<") of ROM_WORD;" << std::endl;
	out << "type PROM_TABLE is array (0 to "<<(n-1)<<") of ROM_PWORD;" << std::endl;
	out << "type OPROM_TABLE is array (0 to "<<(n-1)<<") of ROM_BYTE;" << std::endl;

	out<<"constant PCCHECK: PROM_TABLE := PROM_TABLE'("<<std::endl;
	for (i=l.begin(); i!=l.end(); i++,n--) {
		out<<"ROM_PWORD'(x\""<<std::hex<<hex8(i->pc)<<std::dec<<"\")";
		if (n>1) out<<",";
		out<<std::endl;
	}
	out<<");"<<std::endl;
	n = l.size();
	out<<"constant SPCHECK: ROM_TABLE := ROM_TABLE'("<<std::endl;
	for (i=l.begin(); i!=l.end(); i++,n--) {
		out<<"ROM_WORD'(x\""<<std::hex<<hex8(i->sp)<<std::dec<<"\")";
		if (n>1) out<<",";
		out<<std::endl;
	}
	out<<");"<<std::endl;



	n = l.size();
	out<<"constant STACKACHECK: ROM_TABLE := ROM_TABLE'("<<std::endl;
	for (i=l.begin(); i!=l.end(); i++,n--) {
		if (i->stacka.isValid())
			out<<"ROM_WORD'(x\""<<i->stacka.asString()<<"\")";
		else
            out<<"ROM_WORD'(\""<<i->stacka.asBinaryString()<<"\")";
		if (n>1) out<<",";
		out<<std::endl;
	}
	out<<");"<<std::endl;



	n = l.size();
	out<<"constant STACKBCHECK: ROM_TABLE := ROM_TABLE'("<<std::endl;
	for (i=l.begin(); i!=l.end(); i++,n--) {
		if (i->stackb.isValid())
			out<<"ROM_WORD'(x\""<<i->stackb.asString()<<"\")";
		else
			out<<"ROM_WORD'(\""<<i->stackb.asBinaryString()<<"\")";
		if (n>1) out<<",";
		out<<std::endl;
	}
	out<<");"<<std::endl;

	n = l.size();
	out<<"constant OPCHECK: OPROM_TABLE := OPROM_TABLE'("<<std::endl;
	for (i=l.begin(); i!=l.end(); i++,n--) {
		out<<"ROM_BYTE'(x\""<<std::hex<<hex2((unsigned)i->op)<<std::dec<<"\")";
		if (n>1) out<<",";
		out<<std::endl;
	}
	out<<");"<<std::endl;
	out << "begin" << std::endl;

	out << "process(cnt)" << std::endl;
	out << "begin" << std::endl;
	out << "stackA <= STACKACHECK(cnt);" << std::endl;
	out << "stackB <= STACKBCHECK(cnt);" << std::endl;
	out << "pc <= PCCHECK(cnt);" << std::endl;
	out << "sp <= SPCHECK(cnt);" << std::endl;
	out << "op <= OPCHECK(cnt);" << std::endl;
    out << "end process;" << std::endl;
	out <<"end behave;" << std::endl;
}   

/*
 entity zpuvalidatortable is
 port (
 seq: in unsigned;
 pc: out unsigned;
 sp: out unsigned;
 stacka: out unsigned;
 stackb: out unsigned;
 );
 end zpuvalidator;


 subtype ROM_WORD is STD_LOGIC_VECTOR (31 downto 0); 
 type ROM_TABLE is array (0 to 32767) of ROM_WORD;

 constant PCCHECK: ROM_TABLE := ROM_TABLE'(
 ROM_WORD'(x"01111111010001010100110001000110"),-- hex 7f454c46
 );

 constant SPCHECK: ROM_TABLE := ROM_TABLE'(
 ROM_WORD'("01111111010001010100110001000110"),-- hex 7f454c46
 );

 constant STACKACHECK: ROM_TABLE := ROM_TABLE'(
 ROM_WORD'("01111111010001010100110001000110"),-- hex 7f454c46
 );
 constant STACKBCHECK: ROM_TABLE := ROM_TABLE'(
 ROM_WORD'("01111111010001010100110001000110"),-- hex 7f454c46
 );


 architecture behave of zpuvalidatortable is
 begin

 end behave;
 */
