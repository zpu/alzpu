/*            

 Copyright (C) 2008 Alvaro Lopes <alvieboy at alvie dot com>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 */
#ifndef __ZPU_ZPU4_PIPELINED_H__
#define __ZPU_ZPU4_PIPELINED_H__


#include "zpu_zpu4.h"

class zpu_zpu4_pipelined: public zpu_zpu4
{
public:
	zpu_zpu4_pipelined(class clock *myclock,dev_address_t spstart);
protected:
	virtual dev_value_t stackA() const { return m_stackA; }
	virtual dev_value_t stackB() const { return m_stackB; }
	virtual bool execute_nonstd(unsigned char op);
	virtual void push(const dev_value_t &val){
		throw ZPUException("PUSH not allowed in pure ZPU");
	}
	virtual dev_value_t pop(){
		throw ZPUException("POP not allowed in pure ZPU");
	}
	virtual void process_im(unsigned char op);
	virtual void process_storesp(unsigned char op);
	virtual void process_loadsp(unsigned char op);
	virtual void process_emulate(unsigned char op);
	virtual void process_addsp(unsigned char op);
	virtual void process_ibreak(unsigned char op);
	virtual void process_pushsp(unsigned char op);
	virtual void process_poppc(unsigned char op);
	virtual void process_add(unsigned char op);
	virtual void process_and(unsigned char op);
	virtual void process_sub(unsigned char op);
	virtual void process_or(unsigned char op);
	virtual void process_load(unsigned char op);
	virtual void process_not(unsigned char op);
	virtual void process_flip(unsigned char op);
	virtual void process_nop(unsigned char op);
	virtual void process_store(unsigned char op);
	virtual void process_popsp(unsigned char op);
	virtual void process_neqbranch(unsigned char op);
	virtual void process_ulessthan(unsigned char op);
	virtual void process_ulessthanorequal(unsigned char op);
	virtual void process_lessthan(unsigned char op);
    virtual void process_lessthanorequal(unsigned char op);
	virtual void process_pushspadd(unsigned char op);
	virtual void process_eq(unsigned char op);
	virtual void process_callpcrel(unsigned char op);
	virtual void process_mult(unsigned char op);
	virtual void process_lshiftright(unsigned char op);
	virtual void process_ashiftleft(unsigned char op);
	virtual void process_loadb(unsigned char op);
	virtual void process_storeb(unsigned char op);

	virtual void process_syncb(unsigned char op);
	virtual void process_synca(unsigned char op);

	//void trace(unsigned char op, const char *insn );
	void execute(unsigned char op);

	virtual void resync();
	virtual void saveStackA(const dev_address_t&);
	virtual void saveStackB(const dev_address_t&);

	virtual void setStackA(const dev_value_t &val);
	virtual void setStackB(const dev_value_t &val);
	virtual void popped();

	dev_value_t m_stackA, m_stackB;
	bool m_bInitialized;
	bool m_bInjectStackSync;
};

#endif
