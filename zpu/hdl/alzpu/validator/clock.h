/*            

 Copyright (C) 2008 Alvaro Lopes <alvieboy at alvie dot com>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 */
#ifndef __CLOCK_H__
#define __CLOCK_H__

#include <sigc++/sigc++.h>

class clock {
public:
	clock( unsigned freq );
	sigc::signal<void,unsigned long long> &signalTick() { return m_tickSignal; }
	unsigned getFreq() const { return m_uFreq; }
	void stop();
	void delayTick(unsigned int many);
	void run();
	unsigned long long getTicks() const { return m_uCounter; }
private:
	sigc::signal<void,unsigned long long> m_tickSignal;
	unsigned m_uFreq;
	unsigned long long m_uCounter;
	bool m_bExit;
};

#endif
