/*            

 Copyright (C) 2008 Alvaro Lopes <alvieboy at alvie dot com>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 */
#include "timer.h"
#include "zpu.h"
#include "endianess.h"
#include "zpuexception.h"

timer::timer(class clock *myclock): m_uCnt(0), m_clock(myclock)
{
	myclock->signalTick().connect(
								  sigc::mem_fun(*this, &timer::tick)
								 );

}

timer::~timer()
{

}

void timer::tick(unsigned long long)
{
	m_uCnt++;
}

dev_value_t timer::read( const dev_address_t & address ) const
{
	return dev_value_t(m_uCntSample/m_clock->getFreq());
	//return dev_value_t(0x41C);
}

void timer::write( const dev_address_t &address, const dev_value_t &value, bool setup)
{
	if (setup) {
		throw ZPUException("Trying to load DATA onto TIMER, this is not supported");
	}

	if (value.value()&0x2) {
		m_uCntSample=m_uCnt;
	}

	if (value.value()&0x1) {
		m_uCnt=0;
	}
}
