/*

 Copyright (C) 2008 Alvaro Lopes <alvieboy at alvie dot com>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 */

#ifndef __MEMORY_H__
#define __MEMORY_H__

#include "zpudevice.h"
#include "zputypes.h"
#include <cassert>
#include <vector>

class memory: public zpudevice
{
public:
	memory(unsigned int size): m_memory(size) {
	}
	memory() {};

	void resize(unsigned int size) {
		m_memory.resize(size);
	}


	virtual dev_value_t read( const dev_address_t & address ) const
	{

		assert(address>>2 <= m_memory.size());
		return m_memory[address>>2];
	}

	virtual void write( const dev_address_t &address, const dev_value_t &value, bool setup=false ) {
		assert(address>>2 <= m_memory.size());
		m_memory[address>>2] = value;
	}
	virtual dev_address_t size() const { return m_memory.size(); }

	virtual void zero() {
		std::vector<dev_value_t>::size_type i;
		for (i=0; i<m_memory.size(); i++) {
            m_memory[i] = dev_value_t(0);
		}
	}
private:
	std::vector<dev_value_t> m_memory;
};

#endif
