/*            

 Copyright (C) 2008 Alvaro Lopes <alvieboy at alvie dot com>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 */

#include "rom.h"
#include <fcntl.h>
#include <stdlib.h>
#include <string>
#include <sys/stat.h>
#include <iostream>


rom::rom(const std::string &name)
{
	int infile;
	struct stat st;
	unsigned char v;
	int i = 0;

	stat(name.c_str(), &st);

	resize(st.st_size);
	infile = open(name.c_str(),O_RDONLY);

	while (::read(infile,&v,1)==1) {
		unsigned int m = memory::read(i).value();
		m <<= 8;
		m |= v;
		// Dont allow overload here
		memory::write(i,dev_value_t(m));
		i++;
	}
	close(infile);
}


void rom::write( const dev_address_t &address, const dev_value_t &value, bool setup )
{
	if (!setup) {
		if (address > 0xc) { // Allow GCC registers.
			std::ostringstream ss;
			ss<<"Invalid write to ROM, address 0x"<<std::hex<<address<<", value 0x"<<value.value()<<std::dec<<std::endl;
			throw ZPUException(ss.str());
		}

	}
//	std::cout<<"write to ROM, address 0x"<<std::hex<<address<<", value 0x"<<value<<std::dec<<std::endl;

	memory::write( address,value );
}
