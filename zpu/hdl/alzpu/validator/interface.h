/*            

 Copyright (C) 2008 Alvaro Lopes <alvieboy at alvie dot com>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 */

#ifndef __INTERFACE_H__
#define __INTERFACE_H__

#include "zpudevice.h"

struct zpudeventry
{
	zpudevice *device;
	unsigned address;
	unsigned mask;
};

std::vector<zpudeventry>::iterator zpu::locateDevice(unsigned address)
{
	std::vector<zpudeventry>::iterator i;

	for (i=m_vecDevices.begin(); i!=m_vecDevices.end(); i++) {
//        printf("WScan %08x, this %08x with mask %08x \n", address, i->address, i->mask);
		if ( (i->address & i->mask) == (address & i->mask) ) {
  //  		printf("Matches\n");
			return i;
		}
	}
	return m_vecDevices.end();
}

std::vector<zpudeventry>::const_iterator zpu::locateDevice(unsigned address) const
{
	std::vector<zpudeventry>::const_iterator i;

	for (i=m_vecDevices.begin(); i!=m_vecDevices.end(); i++) {
    //    printf("Scan %08x, this %08x with mask %08x \n", address, i->address, i->mask);
		if ( (i->address & i->mask) == (address & i->mask) ) {
			return i;
		}
	}
	return m_vecDevices.end();
}

void zpu::attachDevice( zpudevice *dev, unsigned base, unsigned mask )
{
	zpudeventry e;
	e.device = dev;
	e.address=base;
	e.mask = mask;
	m_vecDevices.push_back(e);
}


unsigned int zpu::readmem(unsigned int addr) const
{
	std::vector<zpudeventry>::const_iterator dev = locateDevice(addr);
	assert(dev!=m_vecDevices.end());

	//printf("Reading address 0x%08x = 0x%08x\n", addr, memory[addr]);
	return (*dev->device)[addr & ~dev->mask];
}

void zpu::writemem(unsigned int addr, unsigned int value)
{
	std::vector<zpudeventry>::iterator dev = locateDevice(addr);
	assert(dev!=m_vecDevices.end());
   // printf("Write address 0x%08x = 0x%08x\n", addr, value);
	(*dev->device)[addr & ~dev->mask] = value;
	return;
}
