/*            

 Copyright (C) 2008 Alvaro Lopes <alvieboy at alvie dot com>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 */

#ifndef __ZPU_H__
#define __ZPU_H__

#include "zpudevice.h"
#include "zpuinterface.h"
#include "zputypes.h"
#include <vector>
#include <tr1/unordered_map>
#include "vwriter.h"
#include <fstream>
#include "clock.h"
#include <tr1/unordered_map>


typedef enum {
	I_im,
	I_storesp,
	I_loadsp,
	I_emulate,
	I_addsp,
	I_ibreak,
	I_pushsp,
	I_poppc,
	I_add,
	I_and,
	I_or,
	I_load,
	I_not,
	I_flip,
	I_nop,
	I_store,
	I_popsp,
	I_neqbranch,
	I_ulessthan,
	I_pushspadd,
	I_eq,
	I_callpcrel,
	I_mult,
	I_lshiftright,
	I_ashiftleft,
	I_sub,
	I_lessthan,
	I_loadb,
	I_ulessthanorequal,
	I_lessthanorequal,
	I_storeb,
	I_synca,
	I_syncb,
	I_call,
	I_poppcrel,
	I_ashiftright
} insn_t;

class hashInsn
{
public:
	size_t operator()(const insn_t &insn) const {
		return (size_t)insn;
	}
};

class zpu
{
public:


	zpu(class clock *myclock,dev_address_t spstart);
	void reset();
	virtual ~zpu() {};
	void run();
	void attachDevice(zpudevice *dev, dev_address_t addr, dev_address_t size, dev_address_t mask);

	zpuinterface *getInterface() { return &m_Interface ; }

	void dumpStats();
	void setMaxInstructions(unsigned int max);
	void setSkipDump(unsigned int max);
	void setDumpFile( const std::string & file );
	void setValidateFile( const std::string & file );
	void setImplement( const std::string & file );
	void setShowStats(bool);
	void clockTick(unsigned long long);

	dev_value_t readmem_Byte(dev_address_t address);

protected:
	virtual void incInstruction(insn_t insn);
	dev_value_t readmem(const dev_address_t& addr) const;
	void writemem(const dev_address_t &addr, const dev_value_t &value);
	virtual dev_value_t stackA() const;
	virtual dev_value_t stackB() const;
	virtual void push(const dev_value_t &val);
	virtual dev_value_t pop();

	virtual void process_im(unsigned char op);
	virtual void process_storesp(unsigned char op);
	virtual void process_loadsp(unsigned char op);
	virtual void process_emulate(unsigned char op);
	virtual void process_addsp(unsigned char op);
	virtual void process_ibreak(unsigned char op);
	virtual void process_pushsp(unsigned char op);
	virtual void process_poppc(unsigned char op);
	virtual void process_add(unsigned char op);
	virtual void process_and(unsigned char op);
	virtual void process_sub(unsigned char op);
	virtual void process_or(unsigned char op);
	virtual void process_load(unsigned char op);
	virtual void process_not(unsigned char op);
	virtual void process_flip(unsigned char op);
	virtual void process_nop(unsigned char op);
	virtual void process_store(unsigned char op);
	virtual void process_popsp(unsigned char op);
	virtual void process_neqbranch(unsigned char op);
	virtual void process_ulessthan(unsigned char op);
	virtual void process_ulessthanorequal(unsigned char op);
	virtual void process_lessthan(unsigned char op);
	virtual void process_lessthanorequal(unsigned char op);
	virtual void process_pushspadd(unsigned char op);
	virtual void process_eq(unsigned char op);
	virtual void process_callpcrel(unsigned char op);
	virtual void process_mult(unsigned char op);
	virtual void process_lshiftright(unsigned char op);
	virtual void process_ashiftright(unsigned char op);
	virtual void process_ashiftleft(unsigned char op);
	virtual void process_loadb(unsigned char op);
	virtual void process_storeb(unsigned char op);
	virtual void process_call(unsigned char op);
	virtual void process_poppcrel(unsigned char op);

	void trace(unsigned char op, const char *insn );
	virtual void execute(unsigned char op);
	virtual bool execute_nonstd(unsigned char op) { return false; }
	void stall(unsigned int cycles) { m_clock->delayTick(cycles); }

	// For statistics
	void insnStall( insn_t insn, unsigned int cycles ) {
		m_stallStats[insn]+=cycles;
	}

	void incPC() { m_uPC++; }
	void setPC(const dev_address_t &pc) { m_uPC=pc; }
	dev_address_t getPC() const { return m_uPC; }

	virtual dev_address_t getSP() const { return m_uSP; }
	virtual dev_address_t getIncSP() const{ return m_uSP+4; }
	virtual dev_address_t getIncIncSP() const{return m_uSP+8; }
	virtual void incSP() { m_uSP+=4; setSPminmax();}
	virtual void decSP() { m_uSP-=4; setSPminmax();}
	virtual void setSP(const dev_address_t &sp) { m_uSP=sp; setSPminmax(); }
private:
	unsigned m_uSP;
	unsigned m_uPC;
	unsigned m_uSPStart;
	void setSPminmax() {
		if (m_uSPmax<m_uSP) m_uSPmax=m_uSP;
		if (m_uSPmin>m_uSP) m_uSPmin=m_uSP;
	}

protected:
	unsigned m_uSPOffset;
	bool m_bIdimflag;
	unsigned m_uIdimSize;

	unsigned m_uMaxInstructions;
	bool m_bMaxInstructionsSet;
	unsigned m_uSkipDump;
	bool m_bSkipDumpSet;
	class clock *m_clock;

	zpuinterface m_Interface;
	std::tr1::unordered_map<unsigned, unsigned long long> m_stats;
	std::tr1::unordered_map<unsigned, unsigned> m_imsizestats;
	std::tr1::unordered_map<insn_t, unsigned long long, hashInsn> m_stallStats;
	validate_list_t m_vecValidateEntries;
	std::ofstream m_TracefileStream;
	std::ofstream m_ValidatefileStream;

	unsigned m_uSPmax;
	unsigned m_uSPmin;
	bool m_bShowStats;
	std::tr1::unordered_map<std::string,bool> m_mapImplement;
};

// Binary helper :) I love metaprogramming

template<unsigned long b>
struct bin
{
	enum { value = 2 * bin<b/10>::value + b%2 };
};

template<>
struct bin<0>
{
	enum { value = 0 };
};


#endif
