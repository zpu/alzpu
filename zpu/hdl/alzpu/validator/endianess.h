/*            

 Copyright (C) 2008 Alvaro Lopes <alvieboy at alvie dot com>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 */
#ifndef __ENDIANESS_H__
#define __ENDIANESS_H__

#include <endian.h>
#include <byteswap.h>

#if __BYTE_ORDER == __LITTLE_ENDIAN

#  define CPU_TO_BE32(x) (bswap_32(x))
#  define CPU_TO_LE32(x) (x)
#  define CPU_TO_BE16(x) (bswap_16(x))
#  define CPU_TO_LE16(x) (x)

#define BE_TO_CPU16(x) (bswap_16(x))
#define BE_TO_CPU32(x) (bswap_32(x))

#else
# if __BYTE_ORDER == __BIG_ENDIAN
# define CPU_TO_BE32(x) (x)
# define CPU_TO_LE32(x) (bswap_32(x))
# define CPU_TO_BE16(x) (x)
# define CPU_TO_LE16(x) (bswap_16(x))
#define BE_TO_CPU16(x) (x)
#define BE_TO_CPU32(x) (x)

# else
#   error Unsupported endianness type
# endif
#endif

#endif
