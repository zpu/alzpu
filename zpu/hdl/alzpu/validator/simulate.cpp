/*            

 Copyright (C) 2008 Alvaro Lopes <alvieboy at alvie dot com>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 */

#include <string>
#include "zpu.h"
#include "zpu_zpu4.h"
#include "zpu_zpu4wt.h"
#include "zpu_zpu4_pipelined.h"
#include "rom.h"
#include "uart.h"
#include "timer.h"
#include "clock.h"
#include <getopt.h>

static int is_zpu_zpu4=0;
static int is_zpu_zpu4wt=0;
static int is_zpu_pipelined=0;
static int max_instructions=-1;
static int skip_dump=-1;
static int show_stats=-1;
static int zero_mem=0;
static char *bin_file;
static char *trace_file;
static char *validate_file;
static char *implement_list;

static struct option sim_options[] = {
	{ "zpu4", 0, &is_zpu_zpu4, 1 },
	{ "bin", 1, NULL, 0 },
	{ "maxinstructions", 1, NULL, 0 },
	{ "skipdump", 1, NULL, 0 },
	{ "tracefile", 1, NULL, 0 },
	{ "validatefile", 1, NULL, 0 },
	{ "stats", 0, &show_stats, 1 },
	{ "zeromem", 0, &zero_mem, 1 },
	{ "pipelined", 0, &is_zpu_pipelined, 1 },
	{ "zpu4wt", 0, &is_zpu_zpu4wt, 1 },
	{ "implement", 1, NULL, 1 },

	{ NULL, 0, NULL, 0 }
};

static int help()
{
	std::cerr<<"Usage: "<<std::endl;
	std::cerr<<"  validator --bin binaryfile [options]  "<<std::endl;
	std::cerr<<"  valid options:"<<std::endl<<std::endl;
	std::cerr<<"  --zpu4"<<std::endl<<"\tUse ZPU4 simulator. This simulator tries to mimic"<<std::endl;
	std::cerr<<"\toperation of ZPU4 medium core"<<std::endl<<std::endl;
	std::cerr<<"  --zpu4wt"<<std::endl<<"\tUse ZPU4 Write-through simulator. This simulator tries to mimic"<<std::endl;
	std::cerr<<"\toperation of ALZPU with write-through mode."<<std::endl<<std::endl;
	std::cerr<<"  --maxinstructions N"<<std::endl<<"\tStop after processing N instructions"<<std::endl<<std::endl;
	std::cerr<<"  --skipdump N"<<std::endl<<"\tSkip dumping of first N instructions to trace file"<<std::endl<<std::endl;
	std::cerr<<"  --tracefile filename.txt"<<std::endl<<"\tWrite ZPU trace to filename.txt"<<std::endl<<std::endl;
	std::cerr<<"  --validatefile filename.vhd"<<std::endl<<"\tWrite ZPU validate file (VHDL) to filename.vhd"<<std::endl<<std::endl;
	std::cerr<<"  --stats"<<std::endl<<"\tShow some stats at end of run"<<std::endl<<std::endl;
	std::cerr<<"  --zeromem"<<std::endl<<"\tZeroe all memory at startup"<<std::endl<<std::endl;
    std::cerr<<"  --implement op[,op]*"<<std::endl<<"\tOptional instructions to implement"<<std::endl<<std::endl;
	return -1;
}

static int parse_options(int argc, char **argv)
{
	int optindex = 0;
	do {
		switch ( getopt_long_only(argc,argv, "", sim_options, &optindex) )
		{
		case '?':
			return -1;
		default:
			switch (optindex) {
			case 1:
				bin_file = optarg;
				break;
			case 2:
				max_instructions = atoi(optarg);
				break;
			case 3:
				skip_dump = atoi(optarg);
				break;
			case 4:
				trace_file = optarg;
				break;
			case 5:
				validate_file = optarg;
				break;
			case 10:
				implement_list = optarg;
				break;
			}
			continue;
		case -1:
			return 0;
		}
	} while (1);
}

#define ROMSIZE 0x4000
#define RAMSIZE 0x4000

#define RAMTOP (ROMSIZE+RAMSIZE-8)

int main(int argc,char **argv)
{
	zpu *myzpu;

	if (parse_options(argc,argv)<0)
		return help();

	class clock *myclock = new class clock(50);

	if (is_zpu_zpu4) {
		if (is_zpu_pipelined) {
			myzpu = new zpu_zpu4_pipelined(myclock,RAMTOP);
		} else {
			std::cerr<<"Using ZPU4 core"<<std::endl;

			myzpu = new zpu_zpu4(myclock,RAMTOP);
		}
	} else if (is_zpu_zpu4wt) {
		std::cerr<<"Using ZPU4 (Write-Through) core"<<std::endl;
		myzpu = new zpu_zpu4wt(myclock,RAMTOP);
	} else {
		myzpu = new zpu(myclock,RAMTOP);
	}
	if (max_instructions>0)
		myzpu->setMaxInstructions(max_instructions);

	if (skip_dump>0)
		myzpu->setSkipDump(skip_dump);
	if (show_stats>0)
		myzpu->setShowStats(true);

	if (trace_file!=NULL)
		myzpu->setDumpFile(trace_file);

	if (validate_file!=NULL)
		myzpu->setValidateFile(validate_file);

	if (implement_list!=NULL)
		myzpu->setImplement(implement_list);

	if (bin_file==NULL) {
		std::cerr<<"Missing bin file"<<std::endl;
		return help();
	}

    // Load ROM from file
	rom *myrom = new rom(bin_file);

    // Add RAM
	memory *myram = new memory( RAMSIZE );
	if (zero_mem) {
		myram->zero();
	}
    // And devices
	uart *myuart = new uart();
	timer *mytimer = new timer(myclock);

	myzpu->attachDevice(myrom,
						0x00000000, // Start address,
						ROMSIZE, // Size,
						ROMSIZE-1 // Mask
					   );

	myzpu->attachDevice(myram,
						ROMSIZE, // Start address
						RAMSIZE, // Size
						RAMSIZE-1 // Mask
					   );
	myzpu->attachDevice(myuart,
						ROMSIZE+RAMSIZE, // Start address
						0x00000001, // Size
						0x00000000 // Mask
					   );

	myzpu->attachDevice(mytimer,
						0x0000C000, // Start address
						0x00000008, // Size
						0x0000000F // Mask
					   );

	myzpu->run();

}
