/*            

 Copyright (C) 2008 Alvaro Lopes <alvieboy at alvie dot com>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 */

#ifndef __TIMER_H__
#define __TIMER_H__

#include "zpudevice.h"
#include "clock.h"

class zpu;

class timer: public zpudevice
{
public:
	timer(class clock *clk);
	~timer();

	dev_value_t read( const dev_address_t & address ) const;
	void write( const dev_address_t &address, const dev_value_t &value, bool setup=false);

	void tick(unsigned long long);

private:
	unsigned long long m_uCnt;
	unsigned long long m_uCntSample;
	class clock *m_clock;
};


#endif
