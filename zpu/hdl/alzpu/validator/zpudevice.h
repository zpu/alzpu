/*            

 Copyright (C) 2008 Alvaro Lopes <alvieboy at alvie dot com>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 */
#ifndef __ZPUDEVICE_H__
#define __ZPUDEVICE_H__

#include "zputypes.h"

class zpudevice
{
public:
	virtual dev_value_t read( const dev_address_t & address ) const = 0;
	virtual void write( const dev_address_t &address, const dev_value_t &value, bool setup=false ) = 0;

	virtual ~zpudevice() {};
};


#endif
