/*            

 Copyright (C) 2008 Alvaro Lopes <alvieboy at alvie dot com>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 */

#include "zpu_zpu4.h"
#include <cassert>
#include "common.h"
#include <iostream>
#include "zpuexception.h"
#include <fstream>

zpu_zpu4::zpu_zpu4(class clock *myclock,dev_address_t spstart):
	zpu(myclock,spstart),
	m_bInitialized(false)
{
}

void zpu_zpu4::saveStackA(const dev_address_t &where)
{
	if (m_TracefileStream.is_open()) {
		m_TracefileStream<<"Writing STACKA to 0x"<<std::hex<<where<<std::dec<<std::endl;
	}
    writemem(where, stackA());
}

void zpu_zpu4::saveStackB(const dev_address_t &where)
{
	if (m_TracefileStream.is_open()) {
		m_TracefileStream<<"Writing STACKB to 0x"<<std::hex<<where<<std::dec<<std::endl;
	}
	writemem(where, stackB());
}

void zpu_zpu4::setStackA(const dev_value_t &v)
{
    m_stackA=v;
}

void zpu_zpu4::setStackB(const dev_value_t &v)
{
    m_stackB=v;
}

void zpu_zpu4::process_im(unsigned char op)
{
	unsigned int v = 0;
	if (m_bIdimflag) {
		v = stackA().value();
		v <<= 7;
		v |= op &0x7f;
		setStackA( dev_value_t(v) );
	} else {
		m_bIdimflag=true;
		saveStackB(getIncSP());
		setStackB( stackA() );
		decSP();

		if ( op&0x40) {
			v=0xFFFFFF80;
		}
		v |= op &0x7f;
		setStackA(dev_value_t(v));
	}
	incPC();
}

void zpu_zpu4::process_storesp(unsigned char op)
{

	switch (m_uSPOffset) {
	case 0:
		// POP
		insnStall( I_storesp, 2);
		setStackA(stackB());
		incSP();
		popped();
		break;
	case 1:
		// POPDOWN
		insnStall( I_storesp, 2);
		incSP();
		popped();
		break;
	default:
		insnStall( I_storesp, 3 );
		saveStackA( getSP() + (m_uSPOffset<<2) );
		setStackA( stackB() );
		incSP();
		popped();
	}
	incPC();
}

void zpu_zpu4::process_loadsp(unsigned char op)
{
	saveStackB( getIncSP() );
	decSP();

	dev_value_t saveA;
	switch (m_uSPOffset) {
	case 0:
		// DUP
		setStackB( stackA() );
		break;
	case 1:
		// DUPSTACKB
		saveA = stackA();
		setStackA( stackB() );
		setStackB( saveA );
		break;
	default:
		insnStall( I_loadsp, 3);
		setStackB( stackA() );
		if (m_TracefileStream.is_open()) {
			m_TracefileStream<<"Reading from 0x"<<std::hex<<
                 getSP() + 4 + (m_uSPOffset<<2)
				<<std::dec<<std::endl;
		}

		setStackA( readmem( getSP() + 4 + (m_uSPOffset<<2) ) );
		break;
	}
	incPC();
}

void zpu_zpu4::process_emulate(unsigned char op)
{
	saveStackB( getIncSP() );
	setStackB( stackA() );

	setStackA(dev_value_t(getPC()+1));
	setPC(get_bits(op,4,0)<<5);

	decSP();
}

void zpu_zpu4::process_addsp(unsigned char op)
{
	switch (m_uSPOffset) {
	case 0:
		// SHIFT
		setStackA( stackA()<<1 );
		break;
	case 1:
		// ADDTOP
		setStackA( stackA() + stackB() );
		break;
	default:
		dev_address_t address = getSP() + m_uSPOffset*4;
		setStackA( stackA() + readmem(address) );
		break;
	}
	incPC();
}

void zpu_zpu4::process_ibreak(unsigned char op)
{
    throw ZPUException("BREAK instrucion\n");
}

void zpu_zpu4::process_pushsp(unsigned char op)
{
	saveStackB( getIncSP() );
	setStackB(stackA());
	setStackA(dev_value_t(getSP()));
	decSP();
	incPC();
}

void zpu_zpu4::process_poppc(unsigned char op)
{
	insnStall( I_poppc, 5);
	saveStackB( getIncSP() );
	setPC( stackA().value() );
	incSP();
	resync();
}

void zpu_zpu4::process_poppcrel(unsigned char op)
{
	saveStackB( getIncSP() );
	setPC( getPC() + stackA().value() );
	incSP();
	resync();
}

void zpu_zpu4::process_add(unsigned char op)
{
    insnStall( I_add, 2);
	setStackA( stackA() + stackB() );
	incSP();
	popped();
	incPC();
}

void zpu_zpu4::process_sub(unsigned char op)
{
    insnStall( I_sub, 2);
	setStackA( stackB() - stackA() );
	incSP();
	popped();
	incPC();
}

void zpu_zpu4::process_and(unsigned char op)
{
	setStackA( stackA() & stackB() );
	incSP();
	popped();
	incPC();
}

void zpu_zpu4::process_or(unsigned char op)
{
	insnStall( I_or, 2 );
	setStackA( stackA() | stackB() );
	incSP();
	popped();
	incPC();
}

void zpu_zpu4::process_load(unsigned char op)
{
	insnStall( I_load, 3 );
	if (m_TracefileStream.is_open()) {
		m_TracefileStream<<"Reading from 0x"<<std::hex<<stackA().asString()<<std::dec<<std::endl;
	}

	setStackA( readmem( stackA().value() ) );
	incPC();
}

void zpu_zpu4::process_not(unsigned char op)
{
	setStackA( stackA() ^ dev_value_t(0xffffffff));
	incPC();
}

void zpu_zpu4::process_nop(unsigned char op)
{
    incPC();
}

void zpu_zpu4::process_store(unsigned char op)
{
	if (m_TracefileStream.is_open()) {
		m_TracefileStream<<"Writing "<<std::hex<<stackB().asString()<<" to "<<stackA().asString()<<std::dec<<std::endl;
	}

	writemem(stackA().value(),stackB());
	incSP();
	incSP();
	resync();
	incPC();
	insnStall( I_store, 5 );
}

void zpu_zpu4::process_popsp(unsigned char op)
{
	insnStall( I_poppc, 5 );
	saveStackB( getIncSP() );
	setSP( stackA().value() );
	resync();
	incPC();
}

void zpu_zpu4::process_neqbranch(unsigned char op)
{
    insnStall( I_neqbranch, 5);
	if (stackB() != dev_value_t(0) )
		setPC( getPC() + stackA().value() );
	else
		incPC();
	incSP();
	incSP();
	resync();
}

void zpu_zpu4::process_ulessthan(unsigned char op)
{
	dev_value_t res = stackA() < stackB() ? dev_value_t(1): dev_value_t(0);
	setStackA(res);
	incSP();
	popped();
	incPC();
	insnStall( I_ulessthan, 4 );
}

void zpu_zpu4::process_ulessthanorequal(unsigned char op)
{
	dev_value_t res = stackA() <= stackB() ? dev_value_t(1): dev_value_t(0);
	setStackA(res);
	incSP();
	popped();
	incPC();
}

void zpu_zpu4::process_lessthan(unsigned char op)
{
	int a = (int)(stackA().value());
	int b = (int)(stackB().value());

	dev_value_t res = a < b ? dev_value_t(1): dev_value_t(0);
	setStackA(res);
	incSP();
	popped();
	incPC();
}

void zpu_zpu4::process_lessthanorequal(unsigned char op)
{
	int a = (int)(stackA().value());
	int b = (int)(stackB().value());

	dev_value_t res = a <= b ? dev_value_t(1): dev_value_t(0);
	setStackA(res);
	incSP();
	popped();
	incPC();
}

void zpu_zpu4::process_pushspadd(unsigned char op)
{
	// stackA(maxAddrBitIncIO downto minAddrBit) <= stackA(maxAddrBitIncIO-minAddrBit downto 0)+sp;

#if 0
	setStackA(dev_value_t(stackA().value()*4 + getSP()));
#else
    setStackA(dev_value_t(stackA().value() + getSP()));
#endif
   // decSP();
	incPC();
}

void zpu_zpu4::process_eq(unsigned char op)
{
	dev_value_t res = stackA() == stackB() ? dev_value_t(1): dev_value_t(0);
	setStackA(res);
	incSP();
	popped();
	incPC();
	insnStall( I_eq, 4);
}

void zpu_zpu4::process_callpcrel(unsigned char op)
{
	dev_value_t spc= dev_value_t(getPC()+1);
	setPC( getPC() +(int)stackA().value() );
	setStackA(spc);
}

void zpu_zpu4::process_call(unsigned char op)
{
	dev_value_t spc= dev_value_t(getPC()+1);
	setPC( stackA().value() );
	setStackA(spc);
}

void zpu_zpu4::process_flip(unsigned char op)
{
	int i;
	dev_value_t a = stackA();
	unsigned int mask = a.mask();

	unsigned int b = 0;
    unsigned int bmask = 0;
	for (i=0;i<32; i++) {
		b<<=1;
		bmask<<=1;
		b |= ( a.raw_value() &0x1 );
		bmask |= ( mask &0x1 );
		a>>=1;
		mask>>=1;
	}
	setStackA(dev_value_t(b, bmask));
	incPC();
}
void zpu_zpu4::process_mult(unsigned char op)
{
//	insnStall( I_mult, 5 );
	setStackA( dev_value_t( ( stackA().value() * stackB().value() ) &0xffffffff) );
	incSP();
	popped();
	incPC();
}
void zpu_zpu4::process_lshiftright(unsigned char op)
{
	dev_value_t shift;
	dev_value_t valX;
	dev_value_t t;

	shift = stackA() & dev_value_t(0x3f);
	valX = stackB();
	t = dev_value_t( (unsigned)valX.value() >> shift.value());
	setStackA( t );
	//printf("LSHIFTRIGHT: %08x by %08x, gives %08x\n", valX.value(), shift.value(), t.value());
	//push(t);
	incSP();
	popped();
	incPC();
}

void zpu_zpu4::process_ashiftleft(unsigned char op)
{
	dev_value_t shift;
	int valX;
	int t;
	shift = stackA() & dev_value_t(0x3f);
	valX = stackB().value();
	t = valX << shift.value();

	setStackA(dev_value_t(t));


	incSP();
	popped();
	incPC();
}
void zpu_zpu4::process_ashiftright(unsigned char op)
{
	dev_value_t shift;
	int valX;
	int t;
	shift = stackA() & dev_value_t(0x3f);
	valX = stackB().value();
	t = valX >> shift.value();

	setStackA(dev_value_t(t));


	incSP();
	popped();
	incPC();
}

void zpu_zpu4::process_loadb(unsigned char op)
{
	dev_value_t addr = stackA();
	dev_value_t val;

	dev_value_t rmem = readmem( addr.value() & 0xFFFFFFFC );
//	std::cerr << "READ: address "<< addr.asString()<< std::endl;
//	std::cerr << "MEM VALUE: "<< rmem.asString()<< std::endl;
	val = dev_value_t( (rmem.value() >> (8*(3-(addr.value() & 0x3)))) & 0xFF );
//	std::cerr << "VAL VALUE: "<< val.asString()<< std::endl;

	setStackA( val );
	incPC();
}

void zpu_zpu4::process_storeb(unsigned char op)
{
	dev_value_t addr = stackA();
	dev_value_t val = stackB() & dev_value_t(0xFF);

    unsigned int toshift = 3-(addr.value() & 0x3);
	unsigned int mask = 0xFF;
	mask<<=8*toshift;

	dev_value_t rmem = readmem( addr.value() & 0xFFFFFFFC );

	/*std::cerr << "READ: address "<< addr.asString()<< std::endl;
	std::cerr << "SHIFT: "<< toshift << std::endl;
	std::cerr << "READ VALUE: "<< rmem.asString()<< std::endl;
	std::cerr << "VAL VALUE: "<< val.asString()<< std::endl;
	std::cerr << "MASK: "<< std::hex<<mask<<std::dec<< std::endl;
    */
	rmem = rmem & dev_value_t(~mask);

	val = rmem | dev_value_t( val.value() << 8*toshift);

	//std::cerr << "STORE VALUE: "<< val.asString()<< std::endl<<std::endl;

	writemem(addr.value() & 0xFFFFFFFC, val);

	incSP();
	incSP();
	resync();
	incPC();
}

void zpu_zpu4::resync()
{
	setStackA( readmem(getSP()) );
	setStackB( readmem(getIncSP()) );
}

void zpu_zpu4::popped()
{
	if (m_TracefileStream.is_open()) {
		m_TracefileStream<<"Reading STACKB from 0x"<<std::hex<<
			getIncSP() 
			<<std::dec<<std::endl;
	}
	setStackB( readmem(getIncSP()) );
}

void zpu_zpu4::execute(unsigned char op)
{
	if (!m_bInitialized) {
		resync();
		m_bInitialized=true;
	}
	zpu::execute(op);
}
