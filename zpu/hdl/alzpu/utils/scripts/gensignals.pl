#!/usr/bin/perl

#/test_fpga_top_v/uut/onchip_ram_top/wb_ack_o 


my $curmodule;

open(my $infile, $ARGV[0]) or die;

while (<$infile>) {
    next if /^\s*$/;
    next if /^#/;
    if (/^module (.*)$/) {
        $curmodule = $1;
        next;
    }
    if (/^allinmodule (.*)$/) {
        print "ntrace select -o on -m ${1} -l this\n";
        next;
    }
    die unless $curmodule;
    print "ntrace select -o on -n ${curmodule}/$_\n";
}
close($infile);
