#!/usr/bin/perl      
use Math::Complex; 

use POSIX qw(ceil);

my $infile = $ARGV[0];
my $force = $ARGV[1];

my @st = stat($infile) or die;

my $size = $st[7];

my $blocks = $size/32;
if ($size%32 != 0) {
    print STDERR "Infile is not aligned, fixing....\n";
    open(my $f, ">>",$infile) or die;
    my $rest = 32 - ($size%32);
    print $f "\0"x$rest;
    close($f);
    print STDERR "Padded with $rest zeroes.\n";
    $size+=$rest;
}

open(my $in, $infile) or die;

my $words = $size;

if (defined $force) {
    $words=$force;
}

print STDERR "Need to map $words words\n";

my $bits = ceil(logn($words,2));

print STDERR "Need $bits bits for address\n";

my $total_words = 2 ** $bits;
my $total_words_m1 = ($total_words/4)-1;

my $mybuf;

my $out = "";
my $addr = 0;

print<<EOM;
library IEEE; 
use IEEE.std_logic_1164.all; 
use IEEE.std_logic_unsigned.all; 

entity dp_rom_8_32 is
port (ADDRA: in std_logic_vector(${bits}-1 downto 0); 
      CLK: in std_logic;
      ENA:   in std_logic;
      WEA: in std_logic; -- to avoid a bug in Xilinx ISE
      DIA: in STD_LOGIC_VECTOR (7 downto 0); -- to avoid a bug in Xilinx ISE
      DOA: out STD_LOGIC_VECTOR (7 downto 0);
      ADDRB: in std_logic_vector(${bits}-1 downto 2); 
      DIB: in STD_LOGIC_VECTOR (31 downto 0); -- to avoid a bug in Xilinx ISE
      WEB: in std_logic;
      ENB:   in std_logic;
      DOB: out STD_LOGIC_VECTOR (31 downto 0));
end dp_rom_8_32; 
 
architecture behave of dp_rom_8_32 is
 
subtype ROM_WORD is STD_LOGIC_VECTOR (7 downto 0); 
type ROM_TABLE is array (0 to ${total_words_m1}) of ROM_WORD; 
 
EOM


my @ROMS=([],[],[],[]);

my $addr = 0;
while ( $total_words ) {
    #print STDERR "E $val ";
    $mybuf="\0\0\0\0";
    sysread($in,$mybuf,4);
    my @v = unpack("CCCC",$mybuf);
    my $val;
    if (@v) {
        #$val = ;
        for my $i (0..3) {
            #print STDERR "ROM $i: $v[$i]\n";
            push(@{$ROMS[$i]}, sprintf("%02x", $v[$i]));
        }
    }  else {
        $val = 0;
        for my $i (0..3) {
            #print STDERR "ROM $i: $v[$i]\n";
            push(@{$ROMS[$i]}, sprintf("%02x", $val));
        }
    }
    $addr++;
    $total_words-=4;
}
 
# Write ROMs
for my $i (0..3) {
    $total_words = 2 ** $bits;
    print "signal ROM${i}: ROM_TABLE := ROM_TABLE'(";
    my $wcount = $total_words/4;
    
    foreach $val (@{$ROMS[$i]}) {
        printf ("ROM_WORD'(\"%08b\")", hex($val));
        print "," if ($wcount>1) ;
        print "-- hex $val ($wcount)";
        print "\n";
        $wcount--;
    }
    print ");\n\n"
}
print <<EOM;

signal read_a_0: std_logic_vector(${bits}-1 downto 2);
signal read_a_1: std_logic_vector(${bits}-1 downto 2);
signal read_a_2: std_logic_vector(${bits}-1 downto 2);
signal read_a_3: std_logic_vector(${bits}-1 downto 2);

signal read_b: std_logic_vector(${bits}-3 downto 0);

signal a_out_0: std_logic_vector(7 downto 0);
signal a_out_1: std_logic_vector(7 downto 0);
signal a_out_2: std_logic_vector(7 downto 0);
signal a_out_3: std_logic_vector(7 downto 0);

signal req_rom: std_logic_vector(1 downto 0);
begin 

  process (clk)
  begin
    if rising_edge(clk) then
      if ena='1' then req_rom <= addra(1 downto 0); end if;
    end if;
  end process;

  -- ROM0
  process (clk)
    variable r_addr: std_logic_vector(${bits}-1 downto 2);
  begin
    if (clk'event and clk = '1') then
      r_addr := addra(${bits}-1 downto 2);
      if ENA='1' then
          read_a_0 <= r_addr;
      end if;
    end if;  
  end process;  

  a_out_0 <= ROM0(conv_integer(read_a_0));
  a_out_1 <= ROM1(conv_integer(read_a_0));
  a_out_2 <= ROM2(conv_integer(read_a_0));
  a_out_3 <= ROM3(conv_integer(read_a_0));

  process(req_rom, a_out_0, a_out_1, a_out_2, a_out_3)
  begin
    case req_rom is
      when "00" => 
        doa <= a_out_0;
      when "01" => 
        doa <= a_out_1;
      when "10" => 
        doa <= a_out_2;
      when "11" => 
        doa <= a_out_3;
      when others =>
        doa <= (others => 'X');
    end case;
  end process;

  process (clk)
    variable r_addr: std_logic_vector(${bits}-3 downto 0);
  begin  
    if (clk'event and clk = '1') then
      r_addr:= addrb(${bits}-1 downto 2);
      if ENB='1' then
      read_b <= r_addr;
      if web='1' then
        ROM0( conv_integer(r_addr) ) <= dib(31 downto 24);
        ROM1( conv_integer(r_addr) ) <= dib(23 downto 16);
        ROM2( conv_integer(r_addr) ) <= dib(15 downto 8);
        ROM3( conv_integer(r_addr) ) <= dib(7 downto 0);
      end if;
      end if;
    end if;  
  end process;  

  dob(7 downto 0) <= ROM3(conv_integer(read_b));
  dob(15 downto 8) <= ROM2(conv_integer(read_b));
  dob(23 downto 16) <= ROM1(conv_integer(read_b));
  dob(31 downto 24) <= ROM0(conv_integer(read_b));
 
end behave; 
EOM

